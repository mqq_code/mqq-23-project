// ECMA 语法 var let const for if while switch function
// BOM browers object model ==> window location history document navigator screen
// DOM document object model
// node 只可以运行 js 基础语法

// const fs = require('fs') // node 内置包
const path = require('path')
const express = require('express') // 第三方包
const citys = require('./data/city.json') // 第三方包

// 创建一个应用
const app = express()

// 处理 post 请求 urlencoded 格式参数
app.use(express.urlencoded())
// 处理 post 请求 json 格式参数
app.use(express.json())

// 设置允许跨域访问的响应头
// app.all('*', function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*"); // 允许跨域访问的地址
//   res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type"); // 允许跨域访问的请求头
//   res.header("Access-Control-Allow-Methods", "POST,GET"); // 允许跨域访问的请求类型
//   next();
// })


// 接口
app.get('/api/list', (request, response) => {
  response.send(citys)
})

// get 传参数
app.get('/api/list/search', (request, response) => {
  // 接收 get 请求参数
  const { keywords } = request.query
  const data = citys.filter(v => {
    return v.name.includes(keywords) || v.pinyin.includes(keywords)
  })
  response.send(data)
})

// post 传参数
app.post('/api/city/search', (request, response) => {
  // 从请求体中获取 post 请求参数
  console.log(request.query)
  console.log(request.body)
  const { keywords } = request.body
  const data = citys.filter(v => {
    return v.name.includes(keywords) || v.pinyin.includes(keywords)
  })
  response.send(data)
})

// 处理 jsonp
app.get('/api/jsonp/list', (request, response) => {
  const { callbackName } = request.query
  response.send(`${callbackName}(${JSON.stringify(citys)})`)
})



app.listen(3000, () => {
  console.log('服务启动成功 http://localhost:3000')
  console.log('服务启动成功 http://192.168.1.113:3000')
})


// 面向对象 ajax scroll插件 视频音频 


