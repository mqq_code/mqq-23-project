
const path = require('path')
const express = require('express') // 第三方包
const axios = require('axios') // 调接口第三方包

// 创建一个应用
const app = express()

// 处理静态资源
app.use(express.static(path.join(__dirname, 'fe')))

app.get('/api/list', (req, res) => {
  // 请求第三方接口
  axios.get('http://192.168.1.113:3000/api/list').then(response => {
    // 把第三方数据传给前端
    res.send(response.data)
  })
})

app.listen(3001, () => {
  console.log('服务启动成功 http://localhost:3001')
  console.log('服务启动成功 http://192.168.1.113:3001')
})



