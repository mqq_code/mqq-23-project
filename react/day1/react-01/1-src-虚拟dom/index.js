import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';


// 创建react元素
// const div = React.createElement('div', { id: 'app' }, [
//   React.createElement('h1', { className: 'title' }, [
//     '开始学习',
//     React.createElement('b', {}, ['react!'])
//   ])
// ])
// const div = React.createElement('div', {}, ['app'])
// console.log(div)

// jsx：js + xml，本质是 React.createElement 方法的语法糖
// 使用jsx创建react元素
const div = <div>app</div>


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div>app</div>
);


