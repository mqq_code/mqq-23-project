import React, { Component } from 'react'
import './App.scss'
import Button from './components/Button'

class App extends Component {

  state = {
    num: 0
  }

  add = n => {
    this.setState({
      num: this.state.num + n
    })
  }
  
  render() {
    return (
      <div className="app">
        <Button title={this.state.num} text="按钮" handleClick={this.add}></Button>
      </div>
    )
  }
}

export default App
