import React, { Component } from 'react'

export default class Button extends Component {
  render() {
    console.log('接收父组件传过来的参数', this.props)
    const { title, text, handleClick } = this.props
    return (
      <div>
        <h3>{title}</h3>
        <button onClick={() => handleClick(2)}>{text}</button>
      </div>
    )
  }
}
