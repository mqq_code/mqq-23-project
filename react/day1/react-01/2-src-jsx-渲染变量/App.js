// react组件
// 1. 类组件
// 2. 函数组件 （react v16.8 之后可以使用状态，v16.8之前只能纯渲染）

import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  // 存变量（类似vue中的data）
  state = {
    title: '标题',
    num: 0
  }

  changeCount = () => {
    console.log('changeCount', this.state)
    // 必须通过setState通知页面更新
    // setState 把参数和state进行合并
    this.setState({
      num: this.state.num + 1
    })
  }

  text = () => {
    return '测试函数'
  }

  render() {
    // return React.createElement('div', {}, ['app'])
    // 实例化组件时自动执行此函数，渲染虚拟dom
    // this => 组件实例对象
    return (
      <div className='app'>
        <h1>{this.state.title}</h1>
        <p>
          <button>-</button>
          {this.state.num}
          <button onClick={this.changeCount}>+</button>
        </p>
        <h3>jsx中可直接渲染的变量类型</h3>
        <div>数字: {100}</div>
        <div>字符串: {'ABCDEFG'}</div>
        <div>true: {true}</div>
        <div>false: {false}</div>
        <div>undefined: {undefined}</div>
        <div>null: {null}</div>
        <div>数组:{[1,2,3,4,5]}</div>
        {/* <div>对象:{ { a: 100} }</div> jsx中不能直接渲染对象 */}
        <div>调用函数: {this.text()}</div>
      </div>
    )
  }
}

export default App




// class Person {
//   constructor (name) {
//     this.name = name
//   }
//   say () {
//     console.log(this)
//   }
// }

// let xm = new Person('小明')
// // xm.say()


// const fn = xm.say
// fn() // => undefined 严格模式下 全局的this指向 undefined

