// react组件
// 1. 类组件
// 2. 函数组件 （react v16.8 之后可以使用状态，v16.8之前只能纯渲染）

import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  // 存变量（类似vue中的data）
  state = {
    title: '标题',
    num: 0,
    arr: ['王小明', '李小刚', '张晓红'],
    arr1: [
      {
        name: '王小明',
        age: 20,
        sex: 1,
        hobby: ['吃早饭', '吃午饭', '吃完饭', '吃夜宵']
      },
      {
        name: '李小刚',
        age: 22,
        sex: 0,
        hobby: ['睡觉', '睡午觉']
      },
      {
        name: '张晓红',
        age: 21,
        sex: 0,
        hobby: ['唱歌', '跳舞', 'rap', '篮球']
      }
    ]
    // list: [
    //   <li>1</li>,
    //   <li>2</li>,
    //   <li>3</li>
    // ]
  }

  changeCount = () => {
    console.log('changeCount', this.state)
    // 必须通过setState通知页面更新
    // setState 把参数和state进行合并
    this.setState({
      num: this.state.num + 1
    })
  }

  text = () => {
    return '测试函数'
  }

  render() {
    // return React.createElement('div', {}, ['app'])
    // 实例化组件时自动执行此函数，渲染虚拟dom
    // this => 组件实例对象
    return (
      <div className='app'>
        <h1>{this.state.title}</h1>
        <p>
          <button>-</button>
          {this.state.num}
          <button onClick={this.changeCount}>+</button>
        </p>
        <ul>
          {/* 遍历数组 */}
          {this.state.arr.map(item => <li key={item}>{item}</li>)}
        </ul>
        <hr />
        <ul>
          {this.state.arr1.map(item => 
            <li key={item.name}>
              <h3>{item.name}</h3>
              <p>年龄: {item.age}</p>
              <p>性别: {item.sex === 1 ? '男' : '女'}</p>
              <p>
                爱好：{item.hobby.map(v => <span key={v}>{v}</span>)}
              </p>
            </li>
          )}
        </ul>
      </div>
    )
  }
}

export default App




// class Person {
//   constructor (name) {
//     this.name = name
//   }
//   say () {
//     console.log(this)
//   }
// }

// let xm = new Person('小明')
// // xm.say()


// const fn = xm.say
// fn() // => undefined 严格模式下 全局的this指向 undefined

