// react组件
// 1. 类组件
// 2. 函数组件 （react v16.8 之后可以使用状态，v16.8之前只能纯渲染）

import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  // 存变量（类似vue中的data）
  state = {
    title: '标题',
    num: 0,
    arr: ['王小明', '李小刚', '张晓红'],
    arr1: [
      {
        name: '王小明',
        age: 20,
        sex: 1,
        hobby: ['吃早饭', '吃午饭', '吃完饭', '吃夜宵']
      },
      {
        name: '李小刚',
        age: 22,
        sex: 0,
        hobby: ['睡觉', '睡午觉']
      },
      {
        name: '张晓红',
        age: 21,
        sex: 0,
        hobby: ['唱歌', '跳舞', 'rap', '篮球']
      }
    ]
  }

  changeCount = (n) => {
    console.log('changeCount', this.state)
    // 必须通过setState通知页面更新
    // setState 把参数和state进行合并
    this.setState({
      num: this.state.num + n
    })
  }

  renderList = () => {
    if (this.state.num > 0) {
      return <ul>
        {/* 遍历数组 */}
        {this.state.arr.map(item => <li key={item}>{item}</li>)}
      </ul>
    }
    return <ul>
      {this.state.arr1.map(item => 
        <li key={item.name}>
          <h3>{item.name}</h3>
          <p>年龄: {item.age}</p>
          <p>性别: {item.sex === 1 ? '男' : '女'}</p>
          <p>
            爱好：{item.hobby.map(v => <span key={v}>{v}</span>)}
          </p>
        </li>
      )}
    </ul>
  }

  render() {
    return (
      <div className="app">
        <h1>{this.state.num > 0 ? this.state.title : '默认标题'}</h1>
        <p>
          <button onClick={() => this.changeCount(-1)}>-</button>
          {this.state.num}
          <button onClick={() => this.changeCount(1)}>+</button>
        </p>
        {/* jsx 条件判断 */}
        {this.renderList()}
        {this.state.num > 0 ? <div>ABCedfekljalkdf</div> : null}
        {this.state.num > 0 && <div>ABCedfekljalkdf</div>}
      </div>
    )
  }
}

export default App












// class Person {
//   constructor (name) {
//     this.name = name
//   }
//   say () {
//     console.log(this)
//   }
// }

// let xm = new Person('小明')
// // xm.say()


// const fn = xm.say
// fn() // => undefined 严格模式下 全局的this指向 undefined

