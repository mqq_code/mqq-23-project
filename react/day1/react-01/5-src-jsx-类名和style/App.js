// react组件
// 1. 类组件
// 2. 函数组件 （react v16.8 之后可以使用状态，v16.8之前只能纯渲染）

import React, { Component } from 'react'
import classNames from 'classnames'
import './App.scss'

class App extends Component {

  state = {
    title: '标题',
    num: 0,
    arr: ['王小明', '李小刚', '张晓红'],
    arr1: [
      {
        name: '王小明',
        age: 20,
        sex: 1,
        hobby: ['吃早饭', '吃午饭', '吃完饭', '吃夜宵']
      },
      {
        name: '李小刚',
        age: 22,
        sex: 0,
        hobby: ['睡觉', '睡午觉']
      },
      {
        name: '张晓红',
        age: 21,
        sex: 0,
        hobby: ['唱歌', '跳舞', 'rap', '篮球']
      }
    ]
  }

  changeCount = (n) => {
    console.log('changeCount', this.state)
    // 必须通过setState通知页面更新
    // setState 把参数和state进行合并
    this.setState({
      num: this.state.num + n
    })
  }


  render() {
    const { num, title } = this.state
    // const classNames = ['app', num > 0 ? 'yellow' : 'green'].join(' ')

    const styles = {
      background: '#' + Math.random().toString(16).slice(2, 8)
    }
    console.log('render 渲染组件')
    return (
      // <div className={`app ${num > 0 ? 'yellow' : 'green'}`}>
      // <div className={classNames}>
      <div className={classNames('app', { yellow: num > 0, green: num < 0 })} style={styles}>
        <h1>{num > 0 ? title : '默认标题'}</h1>
        <p>
          <button onClick={() => this.changeCount(-1)}>-</button>
          {num}
          <button onClick={() => this.changeCount(1)}>+</button>
        </p>
      </div>
    )
  }
}

export default App












// class Person {
//   constructor (name) {
//     this.name = name
//   }
//   say () {
//     console.log(this)
//   }
// }

// let xm = new Person('小明')
// // xm.say()


// const fn = xm.say
// fn() // => undefined 严格模式下 全局的this指向 undefined

