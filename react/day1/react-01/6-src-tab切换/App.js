import React, { Component } from 'react'
import classNames from 'classnames'
import './App.scss'

class App extends Component {

  state = {
    tablist: [
      {
        title: '许嵩',
        songs: ['断桥残雪', '你若成风', '素颜', '有何不可']
      },
      {
        title: '周杰伦',
        songs: ['晴天', '夜曲', '双节棍', '霍元甲']
      },
      {
        title: '毛不易',
        songs: ['消愁', '像我这样的人', '东北民谣', '呓语']
      }
    ],
    curIndex: 0
  }

  changeTab = index => {
    this.setState({
      curIndex: index
    })
  }

  render() {
    const { tablist, curIndex } = this.state
    return (
      <div className="app">
        <nav>
          {tablist.map((item, index) =>
            <span
              className={classNames({ active: curIndex === index })}
              key={item.title}
              onClick={() => this.changeTab(index)}
            >{item.title}</span>
          )}
        </nav>
        <ul>
          {tablist[curIndex].songs.map(item =>
            <li key={item}>{item}</li>
          )}
        </ul>
      </div>
    )
  }

  // render() {
  //   const { tablist, curIndex } = this.state
  //   return React.createElement('div', { className: 'app' }, [
  //     React.createElement('nav', null, tablist.map((item, index) => {
  //       return React.createElement('span', {
  //         className: classNames({ active: curIndex === index }),
  //         key: item.title,
  //         onClick: () => this.changeTab(index)
  //       }, [item.title])
  //     })),
  //     React.createElement('ul', null, tablist[curIndex].songs.map(item => {
  //       return React.createElement('li', { key: item }, item)
  //     }))
  //   ])
  // }
}

export default App
