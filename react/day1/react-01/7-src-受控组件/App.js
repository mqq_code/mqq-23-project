import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  state = {
    title: '标题',
    form: {
      name: '小明',
      checked: true,
      address: 'shanghai',
      sex: 1
    }
  }

  changeTitle = e => {
    this.setState({
      title: e.target.value
    })
  }

  changeForm = (key, val) => {
    this.setState({
      form: { ...this.state.form, [key]: val }
    })
  }

  submit = () => {
    console.log(this.state.form)
  }
  render() {
    const { title, form } = this.state
    return (
      <div className="app">
        <h2>{title}</h2>
        {/* 受控组件：input的value受组件的状态控制，必须通过onChange事件修改状态 */}
        <input type="text" value={title} onChange={this.changeTitle} />
        <hr />
        <p>姓名: <input type="text" value={form.name} onChange={(e) => this.changeForm('name', e.target.value)} /></p>
        <p>已婚: <input type="checkbox" checked={form.checked}  onChange={(e) => this.changeForm('checked', e.target.checked)} /></p>
        <p>性别:
          <input type="radio" name="sex" value={1} checked={form.sex === 1} onChange={(e) => this.changeForm('sex', e.target.value * 1)} /> 男
          <input type="radio" name="sex" value={0} checked={form.sex === 0} onChange={(e) => this.changeForm('sex', e.target.value * 1)} /> 女
        </p>
        <p>城市:
          <select value={form.address} onChange={(e) => this.changeForm('address', e.target.value)} >
            <option value="">请选择</option>
            <option value="beijing">北京</option>
            <option value="shanghai">上海</option>
            <option value="guangzhou">广州</option>
          </select>
        </p>
        <button onClick={this.submit}>提交</button>
      </div>
    )
  }
}

export default App
