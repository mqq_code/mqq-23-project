import React, { Component, createRef } from 'react'
import './App.scss'

class App extends Component {

  state = {
    title: '标题'
  }

  inp = null
  titleRef = createRef() // 推荐

  submit = () => {
    console.log(this.inp)
    console.log(this.inp.value)
    this.setState({
      title: this.inp.value
    })
  }

  getH2 = () => {
    console.log(this.titleRef.current)
  }

  render() {
    const { title } = this.state
    return (
      <div className="app">
        {/* 获取dom元素方式一： ref属性传入 createRef 创建的对象 */}
        <h2 ref={this.titleRef}>{title}</h2>
        {/* 非受控组件：input的value不受组件的状态控制，直接通过dom元素获取 */}
        {/* 获取dom元素方式二： ref属性传入一个函数，函数的参数就是dom元素 */}
        {/* defaultValue: 默认值只渲染一次， defaultChecked: 默认选中渲染一次 */}
        <input type="text" defaultValue={title} ref={el => this.inp = el} />
        <input type="checkbox" defaultChecked={true} />
        <button onClick={this.submit}>提交</button>
        <button onClick={this.getH2}>获取h2标签</button>
      </div>
    )
  }
}

export default App
