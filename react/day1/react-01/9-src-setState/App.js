import React, { Component } from 'react'
import './App.scss'

class App extends Component {

  state = {
    num: 0,
    title: '标题'
  }

  changeNum = () => {
    // 1. setState 修改数据是异步执行
    // 2. setState 会把传入的参数根原本的state合并，同时调用多次会合并更新，只更新一次
    this.setState({ num: this.state.num + 1 })
    this.setState({ num: this.state.num + 1 })
    this.setState({ num: this.state.num + 1, title: Math.random() })

    // 3. 传入函数会把函数的返回值跟原本的state进行合并，函数的参数就是最新的state
    // this.setState(prevState => {
    //   // prevState: 最新的state
    //   console.log(prevState)
    //   return { num: prevState.num + 1 }
    // })
    // this.setState(prevState => {
    //   return { num: prevState.num + 1 }
    // })
    // this.setState(prevState => {
    //   return { num: prevState.num + 1 }
    // })

    // 4. setState可以传入第二个函数，是页面更新后的回调函数，可以获取最新的dom
    // this.setState({
    //   num: this.state.num + 1
    // }, () => {
    //   // 等待页面更新后执行，可以获取最新的dom
    //   console.log(this.domp.current.innerHTML)
    // })


    // 5. react v18.0之前 this.setState 在setTimeout和原生事件中是同步执行的，在react合成事件和生命周期中是异步的
  }

  domp = React.createRef()

  render() {
    const { num, title } = this.state
    console.log('render函数', num)
    return (
      <div className="app">
        <p ref={this.domp}>{num}</p>
        <button onClick={this.changeNum}>+</button>
        <h4>{title}</h4>
      </div>
    )
  }
}

export default App
