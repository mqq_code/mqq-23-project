import React from 'react'
import './App.scss'
import { Switch, Route, NavLink } from 'react-router-dom'
import Home from './pages/Home'
import Create from './pages/Create'

const App = () => {
  return (
    <div className='app'>
      <main>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/create" component={Create} />
        </Switch>
      </main>
      <footer>
        <NavLink exact to="/">首页</NavLink>
        <NavLink exact to="/create">新增</NavLink>
      </footer>
    </div>
  )
}

export default App