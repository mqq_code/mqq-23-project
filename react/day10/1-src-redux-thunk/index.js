import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App'
import {
  // 路由根组件，跟路由相关的所有内容必须包含在根组件内
  BrowserRouter, // history模式，地址栏没#
  HashRouter // hash模式，地址栏中有#
} from 'react-router-dom'
import store from './store'
import { Provider } from 'react-redux'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <HashRouter>
    {/* 把store传给所有后代组件 */}
    <Provider store={store}>
      <App />
    </Provider>
  </HashRouter>
);
