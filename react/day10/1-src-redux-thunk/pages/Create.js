import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addNumAction, setNumAction, setBannersAction } from '../store/action'

class Create extends Component {

  componentDidMount() {
    this.props.dispatch(setBannersAction)
  }

  setNum = () => {
    this.props.dispatch(setNumAction(2))
  }

  render() {
    return (
      <div>
        <h1>新增页面</h1>
        <p>
          <button onClick={() => this.props.dispatch(addNumAction(-1))}>-1</button>
          <b style={{ margin: '0 20px' }}>{this.props.num}</b>
          <button onClick={() => this.props.dispatch(addNumAction(2))}>+2</button>
        </p>
        <button onClick={this.setNum}>修改num</button>
        {JSON.stringify(this.props.banners)}
      </div>
    )
  }
}

const mapState = state => {
  return {
    num: state.create.num,
    banners: state.create.banners,
    a: 100
  }
}
export default connect(mapState)(Create)

