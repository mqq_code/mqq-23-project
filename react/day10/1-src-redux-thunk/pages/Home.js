import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import classNames from 'classnames'
import { setTitleAction, addListItemAction, changeMarkAction, setBannersAction } from '../store/action'

const Home = () => {
  const dispatch = useDispatch() // 获取dispatch函数
  const title = useSelector(state => state.home.title) // 获取store中的数据
  const list = useSelector(state => state.home.list)
  const banners = useSelector(state => state.create.banners)
  
  const changeTitle = e => {
    dispatch(setTitleAction(e.target.value))
  }
  const keydown = e => {
    if (e.keyCode === 13) {
      dispatch(addListItemAction(e.target.value))
    }
  }
  const changeChecked = (index) => {
    dispatch(changeMarkAction(index))
  }

  useEffect(() => {
    dispatch(setBannersAction)
  }, [])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={changeTitle} onKeyDown={keydown} />
      <h2>历史标题</h2>
      <ul>
        {list.map((item, index) =>
          <li
            key={item.id}
            className={classNames({ red: item.checked })}
          >
            {item.title}
            <button onClick={() => changeChecked(index)}>{item.checked ? '取消' : '标记'}</button>
          </li>
        )}
      </ul>
      <ol>
        {banners.map(item =>
          <li key={item.targetId}><img src={item.imageUrl} width={150} alt="" /></li>
        )}
      </ol>
    </div>
  )
}

export default Home
