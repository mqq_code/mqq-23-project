import axios from 'axios'

// 统一管理store中所有的 action type
export const SET_TITLE = 'SET_TITLE'
export const LIST_ADD_ITEM = 'LIST_ADD_ITEM'
export const CHANGE_MARK = 'CHANGE_MARK'
export const ADD_NUM = 'ADD_NUM'
export const SET_NUM = 'SET_NUM'
export const SET_BANNER = 'SET_BANNER'

// 添加num
export const addNumAction = payload => {
  return {
    type: ADD_NUM,
    payload
  }
}
// 修改num
export const setNumAction = payload => {
  return {
    type: SET_NUM,
    payload
  }
}

// 修改标题
export const setTitleAction = payload => {
  return {
    type: SET_TITLE,
    payload
  }
}
// 添加历史标题
export const addListItemAction = payload => {
  return {
    type: LIST_ADD_ITEM,
    payload
  }
}
// 添加修改list高亮
export const changeMarkAction = (index, a = 'aaaaaaa') => {
  return {
    type: CHANGE_MARK,
    payload: {
      index,
      a
    }
  }
}
// 获取轮播图
export const setBannersAction = dispatch => {
  axios.get('https://zyxcl.xyz/banner').then(res => {
    dispatch({
      type: SET_BANNER,
      payload: res.data.banners
    })
  })
}