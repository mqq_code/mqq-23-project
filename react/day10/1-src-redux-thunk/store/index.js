import { legacy_createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger' // dispatch时自动打印log
import thunk from 'redux-thunk' // 让dispatch可以接受函数作为参数
import reducers from './reducers' // 引入合并后的reducer



// applyMiddleware: 添加中间件，增强dispatch，给dispatch添加额外的功能
const store = legacy_createStore(reducers, applyMiddleware(thunk, logger))

export default store

