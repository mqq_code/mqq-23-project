import { ADD_NUM, SET_NUM, SET_BANNER } from '../action'

const initState = {
  num: 0,
  banners: []
}

// 返回state
const reducer = (state = initState, action) => {
  if (action.type === ADD_NUM) {
    return { ...state, num: state.num + action.payload }
  } else if (action.type === SET_NUM) {
    return { ...state, num: state.num * action.payload }
  } else if (action.type === SET_BANNER) {
    return { ...state, banners: action.payload }
  }
  return state
}

export default reducer