import {
  SET_TITLE,
  LIST_ADD_ITEM,
  CHANGE_MARK
} from '../action'

// 初始值
const initState = {
  title: 'redux 标题',
  list: []
}

// 返回state
const reducer = (state = initState, action) => {
  const newState = JSON.parse(JSON.stringify(state))

  if (action.type === SET_TITLE) {
    newState.title = action.payload
    return newState // { ...state, title: action.payload }
  } else if (action.type === LIST_ADD_ITEM) {
    const list = JSON.parse(JSON.stringify(state.list))
    const index = list.findIndex(v => v.title === action.payload)
    if (index > -1) {
      list.splice(index, 1)
    }
    list.unshift({ id: Date.now(), title: action.payload, checked: false })
    return {
      ...state,
      list
    }
  } else if (action.type === CHANGE_MARK) {
    const { index } = action.payload
    newState.list[index].checked = !newState.list[index].checked
    return newState
  }
  return newState
}

export default reducer