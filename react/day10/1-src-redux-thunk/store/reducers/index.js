import { combineReducers } from 'redux'
import home from './home'
import create from './create'

// 合并多个reducer
const reducers = combineReducers({
  home,
  create
})

export default reducers









// // 初始值
// const initState = {
//   title: 'redux 标题',
//   list: [],
//   num: 0,
// }

// // 返回state
// const reducer = (state = initState, action) => {
//   if (action.type === 'SET_TITLE') {
//     return { ...state, title: action.payload }
//   } else if (action.type === 'ADD_NUM') {
//     return { ...state, num: state.num + action.payload }
//   }
//   return state
// }

// export default reducer





// const reducer = (state = initState, action) => {
  // switch(action.type) {
  //   case 'SET_TITLE':
  //     return { ...state, title: action.payload }
  //   case 'ADD_NUM':
  //     return { ...state, title: action.payload }
  //   default :
  //     return state
  // }
// }