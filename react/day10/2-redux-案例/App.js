import React, { useEffect, useState } from 'react'
import { Switch, Route, withRouter, useHistory, useLocation, useRouteMatch } from 'react-router-dom'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import Edit from './pages/edit/Edit'
import { NavBar } from 'antd-mobile'

const routes = [
  {
    title: '首页',
    exact: true,
    path: '/',
    component: Home
  },
  {
    title: '详情页面',
    exact: true,
    path: '/detail/:id',
    component: Detail
  },
  {
    title: '登陆',
    exact: true,
    path: '/login',
    component: Login
  },
  {
    title: '编辑',
    exact: true,
    path: '/edit/:id?',
    component: Edit
  }
]

const App = (props) => {
  const [title, setTitle] = useState('app')
  const history = useHistory()

  const back = () => {
    history.goBack()
  }

  return (
    <div className="app">
      <NavBar onBack={back}>{title}</NavBar>
      <main>
        <Switch>
          {routes.map(item =>
            <Route key={item.path} exact={item.exact} path={item.path} render={routeInfo => {
              setTimeout(() => {
                setTitle(item.title)
              })
              return <item.component {...routeInfo} />
            }} />
          )}
        </Switch>
      </main>
    </div>
  )
}

export default App