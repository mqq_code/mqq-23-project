import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App'
import './App.scss'
import {
  // 路由根组件，跟路由相关的所有内容必须包含在根组件内
  BrowserRouter, // history模式，地址栏没#
  HashRouter // hash模式，地址栏中有#
} from 'react-router-dom'
import { Provider } from 'react-redux'
import store from './store'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <HashRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </HashRouter>
);
