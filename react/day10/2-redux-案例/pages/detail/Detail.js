import React from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'
import ReactMarkdown from 'react-markdown'
import rehypeHighlight from 'rehype-highlight'
import { Avatar } from 'antd-mobile'
import moment from 'moment'

const Detail = () => {
  const params = useParams()
  const info = useSelector(state => {
    return state.list.find(v => v.id === params.id * 1) || {}
  })
  console.log(info)
  return (
    <div className='page' style={{ overflow: 'auto' }}>
      <Avatar src={info.avatar} />
      <p>{info.user}</p>
      <p>{moment(info.id).format('YYYY-MM-DD kk:mm:ss')}</p>
      <ReactMarkdown children={info.content} rehypePlugins={[rehypeHighlight]}></ReactMarkdown>
    </div>
  )
}

export default Detail