import React, { useEffect, useState } from 'react'
import style from './edit.module.scss'
import { Input, Button, TextArea, Toast } from 'antd-mobile'
import classNames from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'

const Edit = (props) => {
  const [title, setTitle] = useState('')
  const [content, setContent] = useState('')
  const dispatch = useDispatch()
  const history = useHistory()
  const params = useParams()
  const editInfo = useSelector(state => state.list.find(v => v.id === params.id * 1))

  console.log(props)

  useEffect(() => {
    if (editInfo) {
      setTitle(editInfo.title)
      setContent(editInfo.content)
    }
  }, [])

  const submit = () => {
    if (!title.trim()) {
      Toast.show({
        content: '标题不能为空'
      })
      return
    }
    if (!content.trim()) {
      Toast.show({
        content: '内容不能为空'
      })
      return
    }
    dispatch({
      type: 'add_list',
      payload: {
        title,
        content
      }
    })
    Toast.show({
      content: '发布成功'
    })
    history.push('/')
  }

  const submitEdit = () => {
    if (!title.trim()) {
      Toast.show({
        content: '标题不能为空'
      })
      return
    }
    if (!content.trim()) {
      Toast.show({
        content: '内容不能为空'
      })
      return
    }
    dispatch({
      type: 'edit_list',
      payload: {
        title,
        content,
        id: editInfo.id
      }
    })
    Toast.show({
      content: '编辑成功'
    })
    history.push('/')
  }
  return (
    <div className={classNames('page', style.edit)}>
      <Input placeholder='请输入标题' value={title} onChange={val => setTitle(val)}/>
      <TextArea
        className={style.content}
        placeholder='请输入markdown'
        value={content}
        onChange={val => setContent(val)}
      />
      {editInfo ?
        <Button color="primary" block onClick={submitEdit}>编辑</Button>
      :
        <Button color="primary" block onClick={submit}>发表</Button>
      }
    </div>
  )
}

export default Edit