import React from 'react'
import { Empty, Button, Card, Avatar, Space, Dialog } from 'antd-mobile'
import moment from 'moment'
import style from './home.module.scss'
import classNames from 'classnames'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'


const Home = (props) => {
  const list = useSelector(s => s.list)
  const history = useHistory()
  const dispatch = useDispatch()
  const goDetail = id => {
    console.log(id)
    history.push(`/detail/${id}`)
  }
  const remove = id => {
    Dialog.show({
      content: '确定要删除此博客吗',
      closeOnAction: true,
      actions: [
        [
          {
            key: 'cancel',
            text: '取消',
          },
          {
            key: 'delete',
            text: '删除',
            bold: true,
            danger: true,
            onClick: () => {
              dispatch({
                type: 'remove_item',
                payload: id
              })
            }
          },
        ],
      ],
    })
  }
  return (
    <div className={classNames('page', style.home)}>
      <main>
        {list.length === 0 ?
          <Empty description='暂无数据' />
        :
          <div className={style.list}>
            {list.map(item =>
              <Card
                key={item.id}
                headerStyle={{ color: '#1677ff'}}
                bodyClassName={style.card}
                title={item.title}
                onBodyClick={() => goDetail(item.id)}
                extra={<Space>
                  <Button size="mini" color="primary" onClick={() => {
                    history.push(`/edit/${item.id}`)
                  }}>编辑</Button>
                  <Button size="mini" color="danger" onClick={() => remove(item.id)}>删除</Button>
                </Space>}
              >
                <Space>
                  <Avatar src={item.avatar} />
                  <p>{item.user}</p>
                  <p>{moment(item.id).format('YYYY-MM-DD kk:mm:ss')}</p>
                </Space>
              </Card>
            )}
          </div>
        }
      </main>
      <footer>
        <Button block color="primary" onClick={() => history.push('/edit')}>发表博客</Button>
      </footer>
    </div>
  )
}

export default Home