import React, { useState } from 'react'
import style from './login.module.scss'
import classNames from 'classnames'
import { Form, Input, Button, Toast } from 'antd-mobile'
import { EyeInvisibleOutline, EyeOutline } from 'antd-mobile-icons'
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'

const Login = () => {
  const [visible, setVisible] = useState(false)
  const [form] = Form.useForm() // 表单实例对象
  const history = useHistory()
  const dispatch = useDispatch()

  const submit = () => {
    // 触发校验
    form.validateFields().then(res => {
      const token = res.user + res.pwd
      localStorage.setItem('token', token)
      localStorage.setItem('user', res.user)
      dispatch({
        type: 'set_user',
        payload: res.user
      })
      Toast.show({
        content: '登陆成功',
        afterClose: () => {
          console.log('after')
        },
      })
      history.replace('/')
    }).catch(err => {
      console.log(err)
    })
  }

  return (
    <div className={classNames('page', style.login)}>

      <Form
        form={form}
        footer={
          <Button block color='primary' onClick={submit}>登陆</Button>
        }
      >
        <Form.Item name='user' rules={[{ required: true, message: '用户名不能为空' }]}>
          <Input placeholder='请输入用户名' clearable />
        </Form.Item>
        <Form.Item
          name='pwd'
          rules={[{ required: true, message: '密码不能为空' }]}
          extra={
            <div onClick={() => setVisible(!visible)} >
              {!visible ? <EyeInvisibleOutline /> : <EyeOutline />}
            </div>
          }
        >
          <Input
            placeholder='请输入密码'
            clearable
            type={visible ? 'text' : 'password'}
          />
        </Form.Item>
      </Form>

    </div>
  )
}

export default Login