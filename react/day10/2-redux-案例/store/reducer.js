
const initState = {
  list: [],
  user: localStorage.getItem('user') || '',
  title: 'app'
}


const reducer = (state = initState, action) => {
  if (action.type === 'set_user') {
    return { ...state, user: action.payload }
  } else if (action.type === 'add_list') {
    return {
      ...state,
      list: [
        {
          title: action.payload.title,
          content: action.payload.content,
          id: Date.now(),
          user: state.user,
          avatar: 'https://images.unsplash.com/photo-1493666438817-866a91353ca9?ixlib=rb-0.3.5&q=80&fm=jpg&crop=faces&fit=crop&h=200&w=200&s=b616b2c5b373a80ffc9636ba24f7a4a9'
        },
        ...state.list
      ]
    }
  } else if (action.type === 'remove_item') {
    return { ...state, list: state.list.filter(v => v.id !== action.payload) }
  } else if (action.type === 'edit_list') {
    const list = JSON.parse(JSON.stringify(state.list))
    const { title, content, id } = action.payload
    const index = list.findIndex(v => v.id === id)
    list.splice(index, 1, { ...list[index], title, content })
    return {
      ...state,
      list
    }
  } else if (action.type === 'set_title') {
    return {...state, title: action.payload}
  }
  return state
}

export default reducer