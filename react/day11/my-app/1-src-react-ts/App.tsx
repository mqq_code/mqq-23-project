import React, { useEffect, useRef } from 'react'
import Child1, { Item } from './components/Child1'
import Child2 from './components/Child2'
import Child3, { IChild3Ref } from './components/Child3'

const arr = [] as Item[]

// 文件中使用了 jsx 语法，文件后缀名必须叫 tsx
const App = () => {

  // 获取类组件实例对象
  // const child1Ref = useRef<Child1>(null)
  const child3Ref = useRef<IChild3Ref>(null)

  useEffect(() => {
    // console.log(child1Ref.current?.keydown)
    console.log(child3Ref.current?.test)
  }, [])

  return (
    <div>
      <h1>App</h1>
      {/* <Child1 ref={child1Ref} a="aaaa" b={100} c={arr} /> */}
      {/* <Child2 a="aaa" b={100} /> */}

      <Child3 ref={child3Ref} a="AAAAA" />
    </div>
  )
}

export default App