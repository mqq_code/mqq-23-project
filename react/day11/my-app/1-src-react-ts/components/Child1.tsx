import React, { Component } from 'react'

export interface Item {
  id: number;
  title: string
}

interface IState {
  num: number;
  title: string;
  arr: Item[];
}

interface IProps {
  a: string;
  b: number;
  c: Item[];
}

class Child1 extends Component<IProps, IState> {

  state = {
    num: 0,
    title: '标题',
    arr: [] as Item[]
  }

  changeNum = (n: number) => {
    this.setState({
      num: this.state.num + n,
      title: Math.random().toString()
    })
  }
  setTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      title: e.target.value
    })
  }
  clickh1 = (e: React.MouseEvent<HTMLHeadingElement>) => {
    console.log((e.target as HTMLHeadingElement).innerHTML)
  }
  keydown = (e: React.KeyboardEvent) => {
    console.log(e.keyCode)
  }
  render() {
    const { title, num, arr } = this.state
    return (
      <div className="box">
        <h1 onClick={this.clickh1}>Child1 - {title}</h1>
        <input type="text" value={title} onChange={this.setTitle} onKeyDown={this.keydown} />
        <button onClick={() => this.changeNum(-1)}>-</button>
        {num}
        <button onClick={() => this.changeNum(1)}>+</button>
        <ul>
          {arr.map(item =>
            <li key={item.id}>{item.title}</li>
          )}
        </ul>
      </div>
    )
  }
}

export default Child1