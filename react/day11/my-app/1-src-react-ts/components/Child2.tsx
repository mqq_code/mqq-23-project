import React, { useEffect, useMemo, useRef, useState, forwardRef } from 'react'

interface IProps {
  a: string;
  b: number;
}
interface Item {
  id: number;
  text: string;
}

const Child2: React.FC<IProps> = (props) => {

  const [num, setNum] = useState(10)
  const [arr, setArr] = useState<Item[]>([])
  const h2Ref = useRef<HTMLHeadingElement>(null)

  const clickfn = () => {
    console.log(h2Ref.current?.innerHTML)
  }
  
  const listLen = useMemo<number>(() => {
    return arr.length
  }, [arr])

  const fn = (a: number): number => {
    return a * 2
  }
  const add = () => {
     setArr([
      ...arr,
      {
        id: Date.now(),
        text: arr.length + ''
      }
    ])
  }

  return (
    <div className="box">
      <h2 ref={h2Ref}>Child2</h2>
      <button onClick={clickfn}>获取h2</button>
      <button onClick={() => setNum(num + 1)}>{num}</button>
      <button onClick={add}>add</button>
      {listLen}
      <ul>
        {arr.map(item =>
          <li key={item.id}>{item.text}</li>
        )}
      </ul>
      {fn(100)}
    </div>
  )
}

export default Child2