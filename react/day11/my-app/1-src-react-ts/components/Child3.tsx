import React, { forwardRef, useState, useImperativeHandle } from 'react'

interface IProps {
  a: string;
}
export interface IChild3Ref {
  add: (n: number) => void;
  test: string;
}
const Child2: React.ForwardRefRenderFunction<IChild3Ref, IProps> = (props, ref) => {

  const [num, setNum] = useState(0)
  
  const add = (n: number) => {
    setNum(num + n * 2)
  }

  useImperativeHandle(ref, () => {
    return {
      add,
      test: 'test'
    }
  }, [num, add])


  return (
    <div className="box">
      <h2>Child3</h2>
      <button onClick={() => add(2)}>{num}</button>
      <div>{props.a}</div>
    </div>
  )
}
// forwardRef: 把父组件传过来的ref对象传给Child2的第二个参数
export default forwardRef(Child2)