import React from 'react'
import Home from './pages/Home'
import Detail from './pages/Detail'
import { Switch, Route, RouteComponentProps } from 'react-router-dom'


const App: React.FC = () => {
  return (
    <Switch>
      <Route exact path="/" render={(routeInfo: RouteComponentProps) => {
        return <Home {...routeInfo} a="100" ></Home>
      }} />
      <Route exact path="/detail/:id" component={Detail} />
    </Switch>
  )
}

export default App