import React from 'react'
import { RouteComponentProps } from 'react-router-dom'

// type IProps = RouteComponentProps & {
//   a: string;
// }
interface IProps extends RouteComponentProps {
  a: string;
}

const Home: React.FC<IProps> = (props) => {

  const goDetail = () => {
    props.history.push('/detail/123')
  }


  return (
    <div>
      <h1>Home</h1>
      <button onClick={goDetail}>跳转详情页面</button>
    </div>
  )
}

export default Home