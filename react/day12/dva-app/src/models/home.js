import { getBanner } from '../services'


export default {

  // 命名空间，可以定义仓库名称
  namespace: 'home',
  // 存数据
  state: {
    banners: [],
    historylist: [] // 存历史记录
  },

  // 处理异步操作
  effects: {
    *setBanner({ payload }, { call, put }) {
      // 通过call调用接口
      const res = yield call(getBanner, payload)

      // 通过 put 发送 action 调用 reducers 中的函数修改state数据
      yield put({
        type: 'changeBanners',
        payload: res.data.banners
      })
    }
  },

  // 修改仓库数据
  reducers: {
    changeBanners(state, action) {
      console.log('home => changeBanners', state, action)
      return { ...state, banners: action.payload };
    },
    addHistory(state, action) {
      return {...state, historylist: [...state.historylist, action.payload]}
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
      // 可以发起监听
      history.listen(() => {
        console.log('%c 页面跳转了', 'color: red; font-size: 30px', history.location.pathname)
        if (history.location.pathname === '/') {
          document.title = '首页'
        } else {
          document.title = '详情页面'
        }
        dispatch({
          type: 'addHistory',
          payload: history.location.pathname
        })
      })
    },
  },

};
