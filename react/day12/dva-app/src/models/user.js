
export default {
  // 命名空间，可以定义仓库名称
  namespace: 'user',

  // 存数据
  state: {
    age: 20,
    username: '小明'
  },

  // 处理异步
  effects: {
  },

  // 修改state
  reducers: {
    changeUsername(state, action) {
      // console.log(state)
      // console.log(action)
      return {...state, username: action.payload}
    },
    changeAge(state, action) {
      console.log(state)
      console.log(action)
      return {...state, age: action.payload}
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
    },
  },

};
