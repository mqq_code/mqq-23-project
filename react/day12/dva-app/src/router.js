import React from 'react';
import { Router, Route, Switch } from 'dva/router';
import Home from './routes/home/Home';
import Detail from './routes/detail/Detail';
import Mine from './routes/mine/Mine';

function RouterConfig({ history }) {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/detail" exact component={Detail} />
        <Route path="/mine" exact component={Mine} />
      </Switch>
    </Router>
  );
}

export default RouterConfig;
