import React from 'react'
import { connect } from 'dva'

const Detail = (props) => {
  console.log(props)

  const changeName = e => {
    // 调用redux中的方法修改数据
    props.dispatch({
      type: 'user/changeUsername',
      payload: e.target.value
    })
  }
  const add = () => {
    props.dispatch({
      type: 'user/changeAge',
      payload: props.age + 1
    })
  }
  const sub = () => {
    props.dispatch({
      type: 'user/changeAge',
      payload: props.age - 1
    })
  }
  return (
    <div>
      <h1>{props.username}</h1>
      <input type="text" value={props.username} onChange={changeName} />
      <div>
        年龄: 
        <button onClick={sub}>-</button>
        {props.age}
        <button onClick={add}>+</button>
      </div>
    </div>
  )
}

const mapState = state => {
  console.log(state)
  return {
    age: state.user.age,
    username: state.user.username,
  }
}
export default connect(mapState)(Detail)