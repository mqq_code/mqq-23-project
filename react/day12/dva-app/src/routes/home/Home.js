import React, { useEffect } from 'react'
import { connect } from 'dva'
import { Link } from 'dva/router'

const IndexPage = (props) => {

  useEffect(() => {
    props.dispatch({
      type: 'home/setBanner',
      payload: '测试参数'
    })
  }, [])

  return (
    <div>
      <h1>IndexPage</h1>
      <div>
        <Link to="/detail">跳转详情</Link>
      </div>
      {JSON.stringify(props.banners)}
    </div>
  )
}

const mapState = state => {
  console.log(state)
  return {
    banners: state.home.banners,
    username: state.user.username,
    age: state.user.age
  }
}
export default connect(mapState)(IndexPage)