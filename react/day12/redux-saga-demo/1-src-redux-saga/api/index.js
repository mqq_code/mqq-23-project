import axios from 'axios'


export const getBanners = () => {
  return axios.get('https://zyxcl.xyz/banner')
}

export const getToplist = (params) => {
  return axios.get('https://zyxcl.xyz/toplist?a=' + params)
}