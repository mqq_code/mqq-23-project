import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'


const Edit = () => {
  const diapatch = useDispatch()
  const state = useSelector(s => s)

  useEffect(() => {
    diapatch({
      type: 'set_toplist_api'
    })
  }, [])

  return (
    <div>
      <h1>Edit</h1>
      {JSON.stringify(state.toplist)}
    </div>
  )
}

export default Edit