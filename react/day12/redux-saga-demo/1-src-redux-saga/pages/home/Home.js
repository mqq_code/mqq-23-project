import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'

const Home = () => {
  const diapatch = useDispatch()
  const state = useSelector(s => s)

  useEffect(() => {
    diapatch({
      type: 'set_banners_api',
      payload: 123
    })
  }, [])

  return (
    <div>
      <h1>home</h1>
      <Link to="/edit">编辑页面</Link>
      {JSON.stringify(state.banners)}
    </div>
  )
}

export default Home