import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import reducer from './reducer'
import createSagaMiddleware from 'redux-saga'
import mySaga from './saga'

// 创建saga中间件
const sagaMiddleware = createSagaMiddleware()

const store = createStore(reducer, applyMiddleware(sagaMiddleware, logger))

// 运行saga
sagaMiddleware.run(mySaga)

export default store