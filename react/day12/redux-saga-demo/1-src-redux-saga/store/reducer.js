
const initState = {
  banners: [],
  toplist: []
}


const reducer = (state = initState, action) => {
  if (action.type === 'set_banners') {
    return { ...state, banners: action.payload }
  } else if (action.type === 'set_toplist') {
    return { ...state, toplist: action.payload }
  }
  return state
}

export default reducer