import { getBanners, getToplist } from '../api'
import { takeEvery, call, put } from 'redux-saga/effects'


function *setBannersApi (action) {
  console.log(action)
  // saga中通过 call 调用接口
  const res = yield call(getBanners)
  console.log(res.data.banners)

  // 通过put发送action到reducer函数
  yield put({
    type: 'set_banners',
    payload: res.data.banners
  })
}

function *setToplistApi (action) {
  const res = yield call(getToplist, 'bbbbbbbbb')

  console.log(res.data.list)
  yield put({
    type: 'set_toplist',
    payload: res.data.list
  })
}

// 处理异步逻辑
function *saga() {
  // 监听组件中通过 dispatch 调用 set_banners_api
  yield takeEvery('set_banners_api', setBannersApi)
  yield takeEvery('set_toplist_api', setToplistApi)
}

export default saga

