import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import Edit from './pages/edit/Edit'

const routes = [
  {
    title: '首页',
    exact: true,
    path: '/',
    component: Home
  },
  {
    title: '详情页面',
    exact: true,
    path: '/detail/:id',
    component: Detail
  },
  {
    title: '登陆',
    exact: true,
    path: '/login',
    component: Login
  },
  {
    title: '编辑',
    exact: true,
    path: '/edit/:id?',
    component: Edit
  }
]

const App = (props) => {


  return (
    <div className="app">
      <Switch>
        {routes.map(item =>
          <Route key={item.path} exact={item.exact} path={item.path} render={routeInfo => {
            return <item.component {...routeInfo} />
          }} />
        )}
      </Switch>
    </div>
  )
}

export default App