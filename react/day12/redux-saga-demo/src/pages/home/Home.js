import React, { useState } from 'react'
import { Col, Row, Dropdown, Space, Table, Tag, Button, message, Popconfirm } from 'antd';
import { DownOutlined, SmileOutlined } from '@ant-design/icons';


const items = [
  {
    key: '1',
    label: (
      <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
        1st menu item
      </a>
    ),
  },
  {
    key: '2',
    label: (
      <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
        2nd menu item (disabled)
      </a>
    ),
    icon: <SmileOutlined />,
  },
  {
    key: '3',
    label: (
      <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
        3rd menu item (disabled)
      </a>
    ),
  },
  {
    key: '4',
    danger: true,
    label: 'a danger item',
  },
];


const Home = () => {
const [data, setData] = useState([
  {
    id: 1,
    name: '立刻',
    age: 32,
    address: '北京',
    tags: ['男', '唱跳'],
  },
  {
    id: 2,
    name: '王刚',
    age: 42,
    address: '广州',
    tags: ['loser'],
  },
  {
    id: 3,
    name: '张三',
    age: 32,
    address: '河南',
    tags: ['cool', 'teacher'],
  },
])

// 表头
const columns = [
  {
    title: '姓名',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: '年龄',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: '地址',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: '标签',
    key: 'tags',
    dataIndex: 'tags',
    render: (tags, record) => {
      // console.log(tags, record)
      // console.log(tags)
      return <div>
        {tags.map(v => <b key={v} style={{ marginRight: 20 }}>{v}</b>)}
      </div>
    }
  },
  {
    title: '操作',
    key: 'action',
    render: (_, record) => {
      return <Space>
          <Button type="primary">编辑 {record.name}</Button>
          <Popconfirm
            title={<b>警告</b>}
            description={<p>确定要删除 <b style={{color: 'red'}}>{record.name}</b> 吗？</p>}
            onConfirm={(e) => {
              setData(data.filter(v => v.name !== record.name))
              message.success('确定')
            }}
            onCancel={(e) => {
              message.error('取消')
            }}
            okText="确定"
            cancelText="取消"
          >
            <Button danger>删除 {record.name}</Button>
          </Popconfirm>
        </Space>
    },
  },
];

  return (
    <div>
      <h1>Home</h1>
      <Row gutter={16}>
        <Col span={6}>
          <div className="box">6</div>
        </Col>
        <Col span={12}>
          <div className="box">12</div>
        </Col>
        <Col span={6}>
          <div className="box">12</div>
        </Col>
      </Row>
      <Dropdown
        menu={{
          items,
        }}
      >
        <span>下拉带单</span>
      </Dropdown>
      <Table bordered rowKey="id" columns={columns} dataSource={data} />
    </div>
  )
}

export default Home