import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  history: {
    type: 'hash'
  },
  antd: {},
  dva: {
    hmr: true
  },
  routes: [
    { title: '登陆', path: '/login', component: '@/pages/login/Login' },
    {
      title: '首页',
      path: '/',
      component: '@/layouts/Home',
      wrappers: ['@/wrappers/auth'], // 包裹在component外的高阶组件
      routes: [
        { title: '监控页面', path: '/', component: '@/pages/dashboard/Dashboard' },
        { title: '列表页面', path: '/list', component: '@/pages/list/List' },
        { title: '详情', path: '/detail/:id', component: '@/pages/detail' }
      ]
    },
    { path: '*', component: '@/pages/404/404' }
  ],
});
