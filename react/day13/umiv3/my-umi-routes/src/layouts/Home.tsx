import React from 'react'

const Home: React.FC<{}> = (props) => {
  return (
    <div>
      <h1>Home</h1>
      {props.children}
    </div>
  )
}

export default Home