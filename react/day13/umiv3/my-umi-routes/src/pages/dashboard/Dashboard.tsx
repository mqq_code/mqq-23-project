import React from 'react'
import { useHistory, useLocation, Link, NavLink } from 'umi'
import { Button, message, Popconfirm } from 'antd'

const dashboard = () => {
  const history = useHistory()
  const location = useLocation()

  const goLogin = () => {
    console.log(history)
    console.log(location)
  }

  const confirm = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    message.success('Click on Yes');
  };
  
  const cancel = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    message.error('Click on No');
  };
  return (
    <div>
      <h2>监控页面</h2>
      <Popconfirm
        title="Are you sure to delete this task?"
        onConfirm={confirm}
        onCancel={cancel}
        okText="Yes"
        cancelText="No"
      >
        <Button type="primary">Primary Button</Button>
      </Popconfirm>
      <Link to="/list">跳转list</Link>
      <NavLink to="/list">跳转list</NavLink>
    </div>
  )
}

export default dashboard