import React from 'react'
import { useDispatch, useSelector, UserModelState } from 'umi'
import { Input, InputNumber } from 'antd'

const index = () => {
  const dispatch = useDispatch()
  const user = useSelector((state: { user: UserModelState }) => state.user)


  return (
    <div>
      <h1>test页面</h1>
      <h3>{user.name}</h3>
      <Input type="text" value={user.name} onChange={e => {
        dispatch({
          type: 'user/changeName',
          payload: e.target.value
        })
      }} />

      <InputNumber value={user.age} onChange={e => {
        dispatch({
          type: 'user/changeAge',
          payload: e
        })
      }}/>
    </div>
  )
}

export default index