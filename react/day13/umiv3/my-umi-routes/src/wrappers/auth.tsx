import { Redirect } from 'umi'
 
const Auth: React.FC<{}> =  (props) => {
  const token = localStorage.getItem('token')
  if (token) {
    return <div className="auth" style={{ border: '1px solid red' }}>
      {props.children}
    </div>
  } else{
    return <Redirect to="/login" />
  }
}

export default Auth