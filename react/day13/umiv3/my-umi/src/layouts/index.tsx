import React from 'react'
import { useLocation } from 'umi'

const Home: React.FC<any> = (props) => {
  const location = useLocation()


  if (location.pathname === '/login') return props.children

  const detailReg = /^\/detail\/\w+/

  if (detailReg.test(location.pathname)) {
    return <div>
      <h2>详情页面 layout</h2>
      {props.children}
    </div>
  }

  return (
    <div className="wrap" style={{ border: '2px solid red' }}>
      <header>首页导航</header>
      {props.children}
    </div>
  )
}

export default Home