import React from 'react'
import { history, useLocation, useHistory, Link, NavLink } from 'umi'

const dashboard = () => {

  const location = useLocation()

  const goLogin = () => {
    console.log(history)
    console.log(location)
    // console.log(link)
    // history.push('/login')
  }
  return (
    <div>
      <h2>监控页面</h2>
      <button onClick={goLogin}>跳转登陆</button>
      <Link to="/list">跳转list</Link>
      <NavLink to="/list">跳转list</NavLink>
    </div>
  )
}

export default dashboard