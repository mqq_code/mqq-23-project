import { defineConfig } from "umi";

export default defineConfig({
  routes: [
    { path: '/login', component: '@/pages/login/Login' },
    {
      path: '/',
      component: '@/layouts/Home',
      wrappers: ['@/wrappers/auth'], // 包裹在component外的高阶组件
      routes: [
        { path: '/', redirect: '/dashboard' },
        { path: '/dashboard', component: '@/pages/dashboard/Dashboard' },
        { path: '/list', component: '@/pages/list/List' }
      ]
    },
    { path: '*', component: '@/pages/404/404' }
  ],
  npmClient: 'npm',
  plugins: ['@umijs/plugins/dist/antd', '@umijs/plugins/dist/dva'],
  antd: {},
  dva: {}
});
