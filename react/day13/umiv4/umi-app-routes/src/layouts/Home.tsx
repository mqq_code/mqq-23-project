import React from 'react'
import { Outlet } from 'umi'

const Home = () => {
  return (
    <div>
      <h1>Home</h1>
      <Outlet />
    </div>
  )
}

export default Home