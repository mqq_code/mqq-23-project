import { Effect, Reducer, Subscription } from 'umi';

// state数据类型
export interface UserModelState {
  name: string;
  age: number;
}

// 仓库数据类型
export interface UserModelType {
  namespace: 'user';
  state: UserModelState;
  effects: {
    query: Effect;
  };
  reducers: {
    changeName: Reducer<UserModelState>;
    changeAge: Reducer<UserModelState>;
    // 启用 immer 之后
    // save: ImmerReducer<UserModelState>;
  };
  subscriptions: { setup: Subscription };
}

const UserModel: UserModelType = {
  namespace: 'user',

  state: {
    name: '王小明',
    age: 20
  },

  effects: {
    *query({ payload }, { call, put }) {},
  },
  
  reducers: {
    changeName(state, action) {
      return {
        ...state,
        name: action.payload,
      };
    },
    changeAge(state, action) {
      return {
        ...state,
        age: action.payload
      }
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {

    },
  },
};

export default UserModel;