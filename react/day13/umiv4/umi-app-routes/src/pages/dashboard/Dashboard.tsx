import React from 'react'
import { history, useLocation, useNavigate, Link, NavLink } from 'umi'
import { Button } from 'antd'

const dashboard = () => {

  const link = useNavigate()
  const location = useLocation()

  const goLogin = () => {
    console.log(history)
    console.log(location)
    // console.log(link)
    // history.push('/login')
    // link('/login')
  }
  return (
    <div>
      <h2>监控页面</h2>
      <button onClick={goLogin}>跳转登陆</button>
      <Link to="/list">跳转list</Link>
      <NavLink to="/list">跳转list</NavLink>
      <Button type="primary">按钮</Button>
    </div>
  )
}

export default dashboard