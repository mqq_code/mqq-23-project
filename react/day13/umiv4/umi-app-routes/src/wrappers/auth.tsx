import { Navigate, Outlet } from 'umi'
 
const Auth: React.FC<{}> =  (props) => {
  const token = localStorage.getItem('token')
  if (token) {
    return <div className="auth" style={{ border: '1px solid red' }}><Outlet /></div>
  } else{
    return <Navigate to="/login" />
  }
}

export default Auth