import React from 'react'
import { useLocation, Outlet } from 'umi'

const Home = () => {
  const location = useLocation()


  if (location.pathname === '/login') return <Outlet />

  const detailReg = /^\/detail\/\w+/

  if (detailReg.test(location.pathname)) {
    return <div>
      <h2>详情页面 layout</h2>
      <Outlet />
    </div>
  }

  return (
    <div className="wrap" style={{ border: '2px solid red' }}>
      <header>首页导航</header>
      <Outlet />
    </div>
  )
}

export default Home