import React from 'react'
import { useSearchParams } from 'umi'

const List = () => {


  const [searchParams, setSearchParams] = useSearchParams();

  // console.log(searchParams)
  // console.log(searchParams.get('b'))
  // console.log(searchParams.toString())

  return (
    <div>
      <h2>list</h2>
      <button onClick={() => {
        setSearchParams({a:'c',d:'e'}) // location 变成 /comp?a=c&d=e
      }}>修改地址栏参数</button>
    </div>
  )
}

export default List