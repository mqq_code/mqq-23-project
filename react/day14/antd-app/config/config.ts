import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { title: '登陆', path: '/login', component: '@/pages/login/Login' },
    { title: '404', path: '/404', component: '@/pages/404' },
    {
      title: '首页', path: '/', component: '@/layouts/Home',
      wrappers: ['@/wrappers/auth'],
      routes: [
        { title: '数据统计', path: '/', component: '@/pages/dashboard/Dashboard' },
        { title: '学生列表', path: '/list', component: '@/pages/list/List' },
        { redirect: '/404' }
      ]
    }
  ],
  fastRefresh: {},
  dva: {
    immer: true,
    hmr: true,
  }
});
