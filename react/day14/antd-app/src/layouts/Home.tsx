import React, { useEffect } from 'react'
import style from './home.less'
import zhCN from 'antd/es/locale/zh_CN'
import { Avatar, Layout, Menu, Dropdown, Space, ConfigProvider } from 'antd'
import { DownOutlined, SettingOutlined, UserOutlined, PoweroffOutlined } from '@ant-design/icons'
import type { MenuProps } from 'antd'
import { Link, useLocation, history, useSelector, UserModelState, useDispatch } from 'umi'

const { Header, Content, Footer } = Layout

const menus = [
  {
    key: '/',
    label: <Link to="/">监控页面</Link>
  },
  {
    key: '/list',
    label: <Link to="/list">列表管理</Link>
  }
]

const defaultAvatar = 'https://img0.baidu.com/it/u=1821253856,3774998416&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500'

const Home: React.FC<{ children: any }> = (props) => {
  const location = useLocation()
  const dispatch = useDispatch()

  const user = useSelector((state: { user: UserModelState }) => state.user)

  const logout = () => {
    history.replace('/login')
    localStorage.removeItem('token')
  }

  const items: MenuProps['items'] = [
    {
      key: '1',
      label: '个人设置',
      icon: <SettingOutlined />
    },
    {
      key: '2',
      danger: true,
      label: <div onClick={logout}>退出</div>,
      icon: <PoweroffOutlined />
    },
  ]

  useEffect(() => {
    dispatch({
      type: 'user/getuserinfo',
      payload: localStorage.getItem('token')
    })
  }, [])

  return (
    <ConfigProvider locale={zhCN}>
      <Layout className={style.layout}>
        <Header className={style.header}>
          <div className={style.logo}>logo</div>
          <Menu
            className={style.menu}
            mode="horizontal"
            defaultSelectedKeys={[location.pathname]}
            items={menus}
          />
          <div className={style.info}>
            <Space>
              <Avatar src={user.avatar || defaultAvatar} style={{ backgroundColor: '#f56a00' }} shape="square" size="large">{user.username}</Avatar>
              <Dropdown menu={{ items }}>
                <div className={style.name}>
                  <span>{user.username}</span>
                  <DownOutlined />
                </div>
              </Dropdown>
            </Space>
          </div>
        </Header>
        <Content style={{ padding: '20px' }}>
          {props.children}
        </Content>
        <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
      </Layout>
    </ConfigProvider>
  )
}

export default Home