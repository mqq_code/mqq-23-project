import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getuserinfo } from '@/services/api'

export interface UserModelState {
  username: string;
  avatar: string;
}

export interface UserModelType {
  namespace: 'user';
  state: UserModelState;
  effects: {
    getuserinfo: Effect;
  };
  reducers: {
    // setuserinfo: Reducer<UserModelState>;
    // 启用 immer 之后
    setuserinfo: ImmerReducer<UserModelState>;
  };
  subscriptions: { setup: Subscription };
}

const UserModel: UserModelType = {
  namespace: 'user',

  state: {
    username: '',
    avatar: ''
  },

  effects: {
    *getuserinfo({ payload }, { call, put }) {
      const res = yield call(getuserinfo, payload)
      console.log(res.data.data)
      yield put({
        type: 'setuserinfo',
        payload: res.data.data
      })
    }
  },
  reducers: {
    setuserinfo(state, { payload }) {
      state.username = payload.username
      state.avatar = payload.avatar
    }
  },
  subscriptions: {
    setup({ dispatch, history }) {
    },
  },
};

export default UserModel;