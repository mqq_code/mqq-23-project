import React from 'react'
import { Button, Result } from 'antd';
import { history } from 'umi'

const index = () => {
  const goHome = () => {
    history.replace('/')
  }
  return (
    <Result
      status="404"
      title="404"
      subTitle="访问的页面不存在"
      extra={<Button type="primary" onClick={goHome}>去首页</Button>}
    />
  )
}

export default index