import React from 'react'
import Filter from '../list/components/Filter'

const Dashboard = () => {

  const search = (params: any) => {
    console.log(params)
  }

  return (
    <div>
      <h1>监控页面</h1>
      <Filter onSearch={search} />
    </div>
  )
}

export default Dashboard