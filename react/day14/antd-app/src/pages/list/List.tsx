import React, { useEffect, useState } from 'react'
import style from './list.less'
import { getlist, listDel } from '@/services/api'
import { ListItem } from '@/services/type'
import { Space, Table, Tag, Button, Modal, message } from 'antd'
import type { ColumnsType } from 'antd/es/table'
import { ExclamationCircleOutlined, PlusOutlined } from '@ant-design/icons'
import AddUserModel from './components/AddUserModel'
import Filter, { SearchParams } from './components/Filter'

const List = () => {

  const [data, setData] = useState<ListItem[]>([])
  const [total, setTotal] = useState(0)
  const [loading, setLoading] = useState(false)
  const [pageQuery, setPageQuery] = useState({ pagenum: 1, pagesize: 5 })
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [editRow, setEditRow] = useState<ListItem | null>(null)
  const [otherParams, setOtherParams] = useState<SearchParams>({})

  const confirmDel = (row: ListItem) => {
    Modal.confirm({
      title: '提示',
      icon: <ExclamationCircleOutlined />,
      content: <div>确定要删除 <b style={{ color: 'red' }}>{row.username}</b> 吗?</div>,
      okText: '确认',
      cancelText: '取消',
      onOk: async () => {
        const res = await listDel(row.id)
        if (res.data.code === 0) {
          message.success('删除成功')
          const totalPage = Math.ceil(res.data.data.total / pageQuery.pagesize)
          if (totalPage < pageQuery.pagenum) {
            setPageQuery({
              ...pageQuery,
              pagenum: totalPage
            })
            setTotal(res.data.data.total)
          } else {
            getlistApi()
          }
        } else {
          message.error(res.data.msg)
        }
      },
      onCancel: () => {
        message.info('取消删除')
      }
    })
  }

  const columns: ColumnsType<ListItem> = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '序号',
      dataIndex: 'no',
      key: 'no',
    },
    {
      title: '姓名',
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: '年龄',
      dataIndex: 'age',
      key: 'age'
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      render: (sex: ListItem['sex'], record) => {
        switch(sex) {
          case 0:
            return <Tag color="volcano">女</Tag>
          case 1:
            return <Tag color="green">男</Tag>
          default:
            return <Tag color="blue">其他</Tag>
        }
      }
    },
    {
      title: '邮箱',
      key: 'email',
      dataIndex: 'email'
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <Button type="link" onClick={() => {
            setIsModalOpen(true)
            setEditRow(record)
          }}>编辑</Button>
          <Button danger type="link" onClick={() => confirmDel(record)}>删除</Button>
        </Space>
      ),
    },
  ];

  const getlistApi = async () => {
    setLoading(true)
    const res = await getlist({...pageQuery, ...otherParams})
    setLoading(false)
    setData(res.data.data.list)
    setTotal(res.data.data.total)
  }

  useEffect(() => {
    getlistApi()
  }, [pageQuery, otherParams])

  useEffect(() => {
    if (!isModalOpen) {
      setEditRow(null)
    }
  }, [isModalOpen])

  const onSearch = (params: SearchParams) => {
    console.log('搜索', params)
    setOtherParams(params)
  }

  return (
    <div>
      <Filter onSearch={onSearch} />
      <div className={style.bg}>
        <div style={{ marginBottom: '20px', display: 'flex', justifyContent: 'flex-end' }}>
          <Button type="primary" icon={<PlusOutlined />} onClick={() => setIsModalOpen(true)}>新增</Button>
        </div>
        <Table
          rowKey="id"
          columns={columns}
          dataSource={data}
          loading={loading}
          pagination={{
            current: pageQuery.pagenum,
            pageSize: pageQuery.pagesize,
            total,
            pageSizeOptions: [5, 10, 20],
            onChange: (pagenum: number, pagesize: number) => {
              setPageQuery({
                pagenum,
                pagesize
              })
            }
          }}
        />
      </div>
      <AddUserModel
        editRow={editRow}
        visible={isModalOpen}
        onClose={() => setIsModalOpen(false)}
        refresh={getlistApi}
      />
    </div>
  )
}

export default List