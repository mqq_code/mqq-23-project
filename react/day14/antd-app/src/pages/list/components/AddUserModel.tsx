import React, { useEffect } from 'react'
import { Modal, message, InputNumber, Form, Input, Radio } from 'antd'
import { ListItem } from '@/services/type'
import { createUser, updateUser } from '@/services/api'
interface IProps {
  visible: boolean;
  editRow: ListItem | null;
  refresh: () => void;
  onClose: () => void;
}

const AddUserModel: React.FC<IProps> = (props) => {

  const [form] = Form.useForm()

  const create = async (values: ListItem ) => {
    const res = await createUser(values)
    if (res.data.code === 0) {
      message.success('新增成功')
      props.refresh()
      handleCancel()
    } else {
      message.error(res.data.msg)
    }
  }
  const update = async (values: ListItem ) => {
    const res = await updateUser({
      ...values,
      id: props.editRow!.id,
      no: props.editRow!.no
    })
    if (res.data.code === 0) {
      message.success('修改成功')
      props.refresh()
      handleCancel()
    } else {
      message.error(res.data.msg)
    }
  }

  const handleOk = () => {
    // 校验表单
    form.validateFields()
    .then((values: ListItem) => {
      if (props.editRow) {
        update(values)
      } else {
        create(values)
      }
    }).catch(e => {
      console.log(e)
    })
  }
  const handleCancel = () => {
    props.onClose()
    form.resetFields()
  }

  useEffect(() => {
    if (props.editRow) {
      form.setFieldsValue({ ...props.editRow })
    }
  }, [props.editRow])


  return (
    <Modal
      title={props.editRow ? '编辑数据' : '新增数据'}
      open={props.visible}
      onOk={handleOk}
      onCancel={handleCancel}
    >
      <Form form={form} labelCol={{ span: 4 }} wrapperCol={{ span: 20 }} autoComplete="off">
        <Form.Item label="姓名" name="username" rules={[{ required: true, message: '请输入姓名!' }]}>
          <Input />
        </Form.Item>
        <Form.Item label="年龄" name="age" rules={[{ required: true, message: '请输入年龄!' }]}>
          <InputNumber min={18} max={40} />
        </Form.Item>
        <Form.Item label="性别" name="sex" rules={[{ required: true, message: '请选择性别!' }]}>
          <Radio.Group>
            <Radio value={0}>女</Radio>
            <Radio value={1}>男</Radio>
            <Radio value={-1}>其他</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item
          label="邮箱"
          name="email"
          rules={[
            { required: true, message: '请输入邮箱!' },
            { type: 'email', message: '邮箱格式错误!' }
          ]}>
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  )
}

export default AddUserModel