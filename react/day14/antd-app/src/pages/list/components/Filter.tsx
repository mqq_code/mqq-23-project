import React from 'react'
import style from '../list.less'
import { Row, Col, Form, Input, Button, Space, Select } from 'antd'
import { ListItem } from '@/services/type'

export type SearchParams = Partial<Omit<ListItem, 'no'>>
type ParamsKeys = keyof SearchParams
interface IProps {
  onSearch: (params: SearchParams) => void;
}


const Filter: React.FC<IProps> = (props) => {
  const [form] = Form.useForm()

  const submit = () => {
    form.validateFields().then((values: SearchParams) => {
      const obj: SearchParams = {}
      const keys = Object.keys(values) as ParamsKeys[]
      keys.forEach(key => {
        if (values[key] || typeof values[key] === 'number') {
          obj[key] = values[key]
        }
      })
      props.onSearch(obj)
    })
  }
  const reset = () => {
    form.resetFields()
    props.onSearch({})
  }
  return (
    <Form className={style.bg} form={form} labelCol={{ span: 4 }} wrapperCol={{ span: 20 }}>
      <Row>
        <Col span={8}>
          <Form.Item label="id" name="id">
            <Input />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="姓名" name="username">
            <Input />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="年龄" name="age">
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={8}>
          <Form.Item label="性别" name="sex">
            <Select placeholder="请选择性别" options={[
              {
                value: '',
                label: '全部',
              },
              {
                value: 0,
                label: '女',
              },
              {
                value: 1,
                label: '男',
              },
              {
                value: -1,
                label: '其他',
              }
            ]}
          />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item label="邮箱" name="email">
            <Input />
          </Form.Item>
        </Col>
        <Col span={8}>
          <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
            <Space>
              <Button type="primary" onClick={submit}>查询</Button>
              <Button type="primary" ghost onClick={reset}>重置</Button>
            </Space>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}

export default Filter