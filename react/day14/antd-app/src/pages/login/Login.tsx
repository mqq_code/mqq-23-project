import React, { useState } from 'react'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Form, Input, message } from 'antd'
import style from './login.less'
import { history, useLocation, useDispatch } from 'umi'
import { login } from '@/services/api'
import { LoginParams } from '@/services/type'


const Login = () => {
  const location = useLocation()
  const redirectTo = decodeURIComponent(location.query.redirectTo || '/')
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch()

  const onFinish = async (values: LoginParams) => {
    setLoading(true)
    const res = await login(values)
    setLoading(false)
    if (res.data.code === 0) {
      message.success('登陆成功')
      localStorage.setItem('token', res.data.data.token)
      // 获取用户信息
      dispatch({
        type: 'user/getuserinfo',
        payload: res.data.data.token
      })
      history.push(redirectTo)
    } else {
      message.error(res.data.msg)
    }
  };

  return (
    <div className={style.page}>
      <div className={style.wrap}>
        <h2>登陆</h2>
        <Form
          name="normal_login"
          className={style.form}
          onFinish={onFinish}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: '请输入用户名!' }]}
          >
            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="用户名" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              { required: true, message: '请输入密码!' },
              { min: 3, message: '密码最少三位!' }
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="密码"
            />
          </Form.Item>
          <Form.Item>
            <Button loading={loading} block type="primary" htmlType="submit">登陆</Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default Login