import axios from "axios"
import {
  BaseResponse,
  LoginParams,
  LoginResponse,
  ListParams,
  ListResponse,
  UserInfoResponse,
  CreateParams,
  ListItem
} from './type'

export const login = (params: LoginParams) => {
  return axios.post<BaseResponse<LoginResponse>>('/api/login', params)
}

export const getlist = (params: ListParams) => {
  return axios.get<BaseResponse<ListResponse>>('/api/list', { params })
}

export const listDel = (id: string) => {
  return axios.delete<BaseResponse<{ total: number }>>('/api/del', { params: { id } })
}

export const getuserinfo = (token: string) => {
  return axios.post<BaseResponse<UserInfoResponse>>('/api/userinfo', { token })
}

export const createUser = (params: CreateParams) => {
  return axios.post<BaseResponse>('/api/create', params)
}
export const updateUser = (params: ListItem) => {
  return axios.post<BaseResponse>('/api/update', params)
}