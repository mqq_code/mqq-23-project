
export interface BaseResponse<T = {}> {
  code: number;
  msg: string;
  data: T
}

// 登陆接口参数类型
export interface LoginParams {
  username: string;
  password: string;
}
// 登陆接口返回值类型
export type LoginResponse = { token: string; }


// 列表请求参数类型
export interface ListParams {
  pagenum: number;
  pagesize: number;
}
// 列表接口返回值类型
export interface ListItem {
  id: string;
  no: number;
  username: string;
  age: number;
  email: string;
  sex: 0 | 1 | -1;
}

export interface ListResponse {
  list: ListItem[];
  total: number;
}

// 个人信息接口
export interface UserInfoResponse {
  username: string;
  avatar: string;
}

// 创建接口参数
export type CreateParams = Omit<ListItem, 'id' | 'no'>