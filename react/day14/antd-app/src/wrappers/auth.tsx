import React from 'react'
import { Redirect, useLocation } from 'umi'

const auth: React.FC<any> = (props) => {
  const token = localStorage.getItem('token')
  const location = useLocation()
  if (token) {
    return props.children
  }
  const fullPath = location.pathname + location.search
  const params = {
    pathname: '/login',
    query: {
      redirectTo: encodeURIComponent(fullPath)
    }
  }
  return <Redirect to={params} />
}

export default auth