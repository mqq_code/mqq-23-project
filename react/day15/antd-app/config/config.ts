import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    {
      path: '/',
      component: '@/layouts/Home',
      routes: [
        { title: '数据统计', path: '/', component: '@/pages/dashboard/Dashboard' },
        { title: '学生列表', path: '/list', component: '@/pages/list/List' },
        { title: '登陆', path: '/login', component: '@/pages/login/Login' },
        { title: '404', path: '/404', component: '@/pages/404' },
        { redirect: '/404' }
      ]
    }
  ],
  fastRefresh: {},
  dva: {
    immer: true,
    hmr: true,
  }
});
