import Mock from 'mockjs'

const data = Mock.mock({
  "list|1000": [
    {
      "id": "@id",
      "no|+1": 1,
      "username": "@cname",
      "age|18-30": 18,
      "sex|0-1": 0,
      "email": "@email"
    }
  ]
})
let max = 1000

const users = [
  {
    username: '小明',
    password: '123',
    avatar: 'https://img2.baidu.com/it/u=460095101,2160896173&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500',
    token: '12343232432321343213213213123'
  },
  {
    username: '小红',
    password: '234',
    avatar: '',
    token: 'dlskjflajflajsdfljaljdflajdfladlsflkadsf'
  }
]

export default {
  'POST /api/login': (req: any, res: any) => {
    const { username, password } = req.body
    setTimeout(() => {
      const user = users.find(v => v.username === username && v.password === password)
      if (user) {
        res.send({
          code: 0,
          msg: '登陆成功',
          data: {
            token: user.token
          }
        })
      } else {
        res.send({
          code: 1000,
          msg: '用户名或密码错误',
          data: {}
        })
      }
    }, 1000)
  },
  'POST /api/userinfo': (req: any, res: any) => {
    const { token } = req.body
    const user = users.find(v => v.token === token)
    if (user) {
      res.send({
        code: 0,
        msg: '成功',
        data: {
          username: user.username,
          avatar: user.avatar
        }
      })
    } else {
      res.send({
        code: 10001,
        msg: '参数错误',
        data: {}
      })
    }
  },
  'GET /api/list': (req: any, res: any) => {
    const { pagesize, pagenum, ...other } = req.query
    setTimeout(() => {
      if (isNaN(pagenum) || isNaN(pagesize)) {
        res.send({
          code: 10001,
          msg: '参数错误',
          data: {}
        })
        return
      }
      let copylist = [...data.list]
      Object.keys(other).forEach((key: any) => {
        copylist = copylist.filter(v => {
          if (typeof v[key] === 'number') {
            return v[key] === Number(other[key])
          }
          return v[key].includes(other[key])
        })
      })
      const list = copylist.slice(pagenum * pagesize - pagesize, pagenum * pagesize)
      res.send({
        code: 0,
        msg: '成功',
        data: {
          list,
          total: copylist.length
        }
      })
    }, 1000)
  },
  'DELETE /api/del': (req: any, res: any) => {
    const { id } = req.query
    setTimeout(() => {
      const index = data.list.findIndex((v: any) => v.id === id)
      if (index > -1) {
        data.list.splice(index, 1)
        res.send({
          code: 0,
          msg: '删除成功',
          data: {
            total: data.list.length
          }
        })
      } else {
        res.send({
          code: 1004,
          msg: '参数错误，id不存在',
          data: {}
        })
      }
    }, 1000)
  },
  'POST /api/create': (req: any, res: any) => {
    const { username, age, sex, email } = req.body
    max++
    data.list.unshift({
      id: Date.now() + '',
      no: max,
      username,
      age,
      sex,
      email
    })
    res.send({
      code: 0,
      msg: '成功',
      data: {}
    })
  },
  'POST /api/update': (req: any, res: any) => {
    const { id } = req.body
    const index = data.list.findIndex((v: any) => v.id === id)
    if (index > -1) {
      data.list.splice(index, 1, req.body)
      res.send({
        code: 0,
        msg: '成功',
        data: {}
      })
    } else {
      res.send({
        code: 1001,
        msg: '参数错误，id不存在',
        data: {}
      })
    }
  },
}