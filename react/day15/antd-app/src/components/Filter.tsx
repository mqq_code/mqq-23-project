import { Button, Form, Input, Space, Select, Radio, Checkbox } from 'antd'
import type { Rule } from 'antd/es/form'
import React from 'react'
import style from './filter.less'

export interface FilterItem {
  label: string;
  name: string;
  type: 'input' | 'select' | 'radio' | 'checkbox';
  rules?: Rule[];
  placeholder?: string;
  options?: {
    label: string;
    value: string | number;
  }[]
}

interface IProps {
  onSearch: (values: any) => void;
  onReset: () => void;
  filterList: FilterItem[];
  direction: 'vertical' | 'horizontal';
}

const Filter: React.FC<IProps> = (props) => {

  const [form] = Form.useForm()

  const onSearch = () => {
    form.validateFields().then(values => {
      props.onSearch(values)
    }).catch(e => {
      console.log(e)
    })
  }

  const onReset = () => {
    form.resetFields()
    props.onReset()
  }

  const renderInput = (item: FilterItem) => {
    if (item.type === 'select') {
      return <Select options={item.options} placeholder={item.placeholder} />
    } else if (item.type === 'radio') {
      return <Radio.Group options={item.options} />
    } else if (item.type === 'checkbox') {
      return <Checkbox.Group options={item.options} />
    } else {
      return <Input placeholder={item.placeholder} />
    }
  }

  return (
    <Form
      form={form}
      className={style.filter}
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
      autoComplete="off"
      style={{ flexDirection: props.direction === 'vertical' ? 'column' : 'row'}}
    >
      {props.filterList.map(item =>
        <Form.Item
          key={item.name}
          className={style.col}
          label={item.label}
          name={item.name}
          rules={item.rules}
        >
          {renderInput(item)}
        </Form.Item>
      )}
      <Form.Item className={style.col} wrapperCol={{ offset: 6, span: 18 }}>
        <Space>
          <Button type="primary" onClick={onSearch}>搜索</Button>
          <Button type="primary" ghost onClick={onReset}>重置</Button>
        </Space>
      </Form.Item>
    </Form>
  );
};

export default Filter;