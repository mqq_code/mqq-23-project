import React from 'react'
import { NavLink } from 'umi'
import './home.less'

const Home:React.FC = (props) => {
  return (
    <div className="home">
      <nav>
        <NavLink exact to="/">监控页面</NavLink>
        <NavLink exact to="/list">列表页面</NavLink>
        <NavLink exact to="/login">登陆页面</NavLink>
      </nav>
      <main>
        {props.children}
      </main>
    </div>
  )
}

export default Home