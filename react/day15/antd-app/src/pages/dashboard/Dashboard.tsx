import React, { useState } from 'react'
import Filter, { FilterItem } from '@/components/Filter'

const Dashboard = () => {

  const onSearch = (values: any) => {
    console.log('开始搜索', values)
  }
  const onReset = () => {
    console.log('重置数据')
  }
  const filterList: FilterItem[] = [
    {
      label: '姓名',
      name: 'username',
      type: 'input',
      placeholder: '请输入姓名',
      rules: [{ required: true, message: '请输入姓名' }]
    },
    {
      label: '年龄',
      name: 'age',
      type: 'input',
      placeholder: '请输入年龄'
    }
  ]

  return (
    <div>
      <h1>监控页面</h1>
      <Filter
        filterList={filterList}
        onSearch={onSearch}
        onReset={onReset}
      />
    </div>
  )
}

export default Dashboard
