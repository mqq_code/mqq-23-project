import React, { useState } from 'react'
import Filter, { FilterItem } from '@/components/Filter'

const List = () => {

  const onSearch = (values: any) => {
    console.log('开始搜索', values)
  }
  const onReset = () => {
    console.log('重置数据')
  }
  const filterList: FilterItem[] = [
    {
      label: '身份证号',
      name: 'idcard',
      type: 'input',
      placeholder: '请输入身份证号',
      rules: [{ required: true, message: '请输入身份证号' }]
    },
    {
      label: '性别',
      name: 'sex',
      type: 'radio',
      rules: [{ required: true, message: '请选择性别' }],
      options: [
        { label: '男', value: 1 },
        { label: '女', value: 0 },
        { label: '其他', value: -1 }
      ]
    },
    {
      label: '爱好',
      name: 'hobby',
      type: 'checkbox',
      rules: [{ required: true, message: '请选择爱好' }],
      options: [
        { label: '唱歌', value: '唱歌' },
        { label: '跳舞', value: '跳舞' },
        { label: '篮球', value: '篮球' }
      ]
    }
  ]

  return (
    <div>
      <h1>列表页面</h1>
      <Filter
        direction="horizontal"
        filterList={filterList}
        onSearch={onSearch}
        onReset={onReset}
      />
    </div>
  )
}

export default List
