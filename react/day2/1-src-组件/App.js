import React, { Component } from 'react'
import './App.scss'
import CountDown from './components/CountDown'

class App extends Component {

  state = {
  }

  render() {
    return (
      <div className="app">
        <CountDown endTime={new Date('2023-8-1 12:00:00').getTime()} />
        <CountDown endTime={new Date('2023-8-1 14:00:00').getTime()} />
        <CountDown endTime={new Date('2023-8-1 18:00:00').getTime()} />
      </div>
    )
  }
}

export default App
