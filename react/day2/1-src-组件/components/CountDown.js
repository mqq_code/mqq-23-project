import React, { Component } from 'react'

export default class CountDown extends Component {

  state = {
    timeStr: ''
  }

  getTimeStr = () => {
    const { endTime } = this.props
    const d = endTime - Date.now()
    const h = Math.floor(d / 1000 / 60 / 60)
    const m = Math.floor(d / 1000 / 60 % 60)
    const s = Math.floor(d / 1000 % 60)
    this.setState({
      timeStr: `${h}小时${m}分${s}秒`
    })
  }
  // 组件挂载完成
  componentDidMount() {
    this.getTimeStr()
    setInterval(() => {
      this.getTimeStr()
    }, 1000)
  }

  render() {
    return (
      <div>倒计时: {this.state.timeStr}</div>
    )
  }
}
