import React, { Component } from 'react'
import './App.scss'
import Dialog from './components/Dialog'

class App extends Component {

  state = {
    visible: false,
    num: 0
  }

  close = () => {
    console.log('关闭弹窗')
    this.setState({ visible: false })
  }

  changeCount = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  render() {
    const { visible, num } = this.state
    return (
      <div className="app">
        <button onClick={() => this.setState({ visible: true })}>显示弹窗</button>
        <p>
          <button onClick={() => this.changeCount(-1)}>-</button>
          {num}
          <button onClick={() => this.changeCount(1)}>+</button>
        </p>
        {visible &&
          <Dialog
            header={<div>header</div>}
            footer={<div>footer</div>}
            title="警告"
            num={num}
            onClose={this.close}
            onChange={this.changeCount}
          >
            {/* 组件双标签内的元素，组件内通过 props.children 获取 */}
            <div>12345</div>
            <p>pppppppp1111111</p>
            <p>pppppppp22222222</p>
          </Dialog>
        }
      </div>
    )
  }
}

export default App
