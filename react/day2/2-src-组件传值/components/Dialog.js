import React, { Component, Children } from 'react'

console.log(Children)

export default class Dialog extends Component {
  render() {
    // console.log(this.props)

    const { title, num, onClose, onChange, children } = this.props

    // console.log(Children.count(children)) // 返回children的数量
    // console.log(Children.toArray(children)) // 把children转成数组格式
    // console.log(Children.only(children)) // children只能一个元素否则报错
    // 遍历children
    // Children.forEach(children, (item, index) => {
    //   console.log(item, index)
    // })
    // const list = Children.map(children, (item, index) => {
    //   return <div key={index} style={{background: 'red'}}>{item}</div>
    // })
    // console.log(list)

    return (
      <div className="dialog" onClick={onClose}>
        <div className="content" onClick={e => e.stopPropagation()}>
          <h3>{title}</h3>
          <div className="close" onClick={onClose}>x</div>
          {/* <div>
            <button onClick={() => onChange(-1)}>-</button>
            {num}
            <button onClick={() => onChange(1)}>+</button>
          </div> */}
          <div className="header">{this.props.header}</div>
          <div className="center">{this.props.children}</div>
          <div className="footer">{this.props.footer}</div>
        </div>
      </div>
    )
  }
}
