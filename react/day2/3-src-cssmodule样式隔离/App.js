import React, { Component } from 'react'
import './App.scss'
import Child from './components/child/Child'

class App extends Component {

  state = {
  }

  render() {
    return (
      <div className="app">
        <h2 className="title">样式隔离</h2>
        <button className="test">按钮</button>
        <Child />
      </div>
    )
  }
}

export default App
