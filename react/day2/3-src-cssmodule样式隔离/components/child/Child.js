import React, { Component } from 'react'
import style from './child.module.scss'
import classNames from 'classnames'


// 使用 css module 样式隔离
export default class Child extends Component {
  render() {
    return (
      <div className={classNames(style.box, style.box1)}>
        <h2 className={style.title}>我是组件</h2>
        <div className="test">test</div>
      </div>
    )
  }
}
