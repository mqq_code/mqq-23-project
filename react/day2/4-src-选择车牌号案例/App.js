import React, { Component } from 'react'
import './App.scss'
import Keyboard from './components/keyboard/Keyboard'

class App extends Component {

  state = {
    visible: false,
    plateNum: ''
  }
  close = () => {
    this.setState({ visible: false })
  }
  changePlateNum = text => {
    this.setState({
      plateNum: text,
      visible: false
    })
  }
  render() {
    const { visible, plateNum } = this.state
    const text = plateNum || '查看效果请点击这里，请输入车牌号'
    return (
      <div className="app">
        <div className="inp" onClick={() => this.setState({ visible: true })}>{text}</div>
        <Keyboard visible={visible} onClose={this.close} onConfirm={this.changePlateNum} />
      </div>
    )
  }
}

export default App
