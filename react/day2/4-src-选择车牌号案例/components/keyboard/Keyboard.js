import React, { Component } from 'react'
import style from './keyboard.module.scss'
import classNames from 'classnames'


export default class Keyboard extends Component {
  state = {
    plateNum: new Array(8).fill(null),
    keyboard: {
      cn: ['京','津','渝','沪','冀','晋','辽','吉','黑','苏','浙','皖','闽','赣','鲁','豫','鄂','湘','粤','琼','川','贵','云','陕','甘','青','蒙','桂','宁','新','藏','使','领','警','学','港','澳'],
      en: ['1','2','3','4','5','6','7','8','9','0','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M']
    },
    lang: 'cn',
    curIndex: 0
  }

  changeLang = () => {
    this.setState({
      lang: this.state.lang === 'cn' ? 'en' : 'cn'
    })
  }
  changeCurIndex = index => {
    this.setState({
      curIndex: index,
      lang: index === 0 ? 'cn' : 'en'
    })
  }
  // 点击键盘添加车牌文字
  changePlateNum = val => {
    const { curIndex } = this.state
    const plateNum = [...this.state.plateNum]
    // 改变当前高亮位置的文字
    plateNum[curIndex] = val
    // 判断是否到最后一项
    const nextIndex = curIndex + 1 > plateNum.length - 1 ? plateNum.length - 1 : curIndex + 1
    this.setState({
      plateNum,
      curIndex: nextIndex,
      lang: 'en'
    })
  }
  // 删除
  del = () => {
    const { curIndex, plateNum } = this.state
    const newPlateNum = [...plateNum]
    if (plateNum[curIndex]) {
      // 当前高亮有数据
      newPlateNum[curIndex] = null
      this.setState({
        plateNum: newPlateNum
      })
    } else {
      if (curIndex > 0) {
        // 当前高亮没数据，删除前一项
        newPlateNum[curIndex - 1] = null
        this.setState({
          plateNum: newPlateNum,
          curIndex: curIndex - 1,
          lang: curIndex - 1 === 0 ? 'cn' : 'en'
        })
      }
    }
  }
  // 提交
  submit = () => {
    const { plateNum } = this.state
    // 截取车牌前7位
    const plateNumStr = plateNum.slice(0, plateNum.length - 1).join('')
    // 判断前7位是否都有数据
    if (plateNumStr.length === plateNum.length - 1) {
      // 拼接最后一位可选数据
      const last = plateNum[plateNum.length - 1] || ''
      // 调用父组件函数传给父组件
      this.props.onConfirm(plateNumStr + last)
    } else {
      alert('格式错误')
    }
    
  }
  render() {
    const { plateNum, keyboard, lang, curIndex } = this.state
    const { visible, onClose } = this.props
    if (!visible) {
      return null
    }
    return (
      <div className={style.wrap} onClick={onClose}>
        <div className={style.keyboard} onClick={e => e.stopPropagation()}>
          <div className={style.plateNum}>
            <ul>
              {plateNum.map((item, index) => {
                return <li
                  className={classNames({ [style.active]: index === curIndex })}
                  key={index}
                  onClick={() => this.changeCurIndex(index)}
                >{item}</li>
              })}
            </ul>
            <button onClick={this.submit}>确定</button>
          </div>
          <div className={style.keyboardBox}>
            <ul>
              {keyboard[lang].map(item => {
                return <li key={item} onClick={() => this.changePlateNum(item)}>{item}</li>
              })}
              <li onClick={this.changeLang}>{lang === 'cn' ? 'ABC' : '中'}</li>
              <li className={style.del} onClick={this.del}>删除</li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}
