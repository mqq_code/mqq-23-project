import React, { Component } from 'react'
import Child from './Child'
import Child1 from './Child1'
import './App.scss'

class App extends Component {
  // 挂载阶段： 初始化state和props
  // constructor(props) {
  //   super(props)
  //   console.log('%c constructor 初始化', 'color: red; font-size: 20px')
  // }

  state = {
    num: 0,
    visible: false
  }

  h1 = React.createRef(null)
  
  // // 组件挂载之前（新版本已废弃）
  // componentWillMount() {
  //   console.log('%c componentWillMount 挂载之前', 'color: red; font-size: 20px')
  //   console.log(this.h1)
  // }

  // // 组件挂载成功（可以调接口、获取dom、定时器）
  // componentDidMount() {
  //   console.log('%c componentDidMount 挂载完成', 'color: red; font-size: 20px')
  //   console.log(this.h1)
  // }

  // // 注意：不要在更新阶段调用 setState
  // // 更新阶段（性能优化，判断组件是否需要更新）
  // shouldComponentUpdate() {
  //   console.log('%c shouldComponentUpdate 性能优化', 'color: red; font-size: 20px')
  //   return true
  // }

  // // 组件更新之前（新版本已废弃）
  // componentWillUpdate() {
  //   console.log('%c componentWillUpdate 组件更新之前', 'color: red; font-size: 20px')
  // }

  // // 组件更新完成（可以获取到最新的dom元素）
  // componentDidUpdate () {
  //   console.log('%c componentDidUpdate 组件更新完成', 'color: red; font-size: 20px')
  //   console.log(document.querySelector('p').outerHTML)
  // }

  // 开始渲染
  render() {
    // console.log('%c render 开始渲染', 'color: red; font-size: 20px')
    return (
      <div>
        <h1 ref={this.h1}>App</h1>
        <p>{this.state.num}</p>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>+</button>
        {/* <button onClick={() => this.setState({ visible: !this.state.visible })}>toggle</button>
        {this.state.visible && <Child />} */}
        <Child1 num={this.state.num} />
      </div>
    )
  }
}


export default App

