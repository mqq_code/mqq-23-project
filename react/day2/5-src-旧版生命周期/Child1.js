import React, { Component } from 'react'

export default class Child1 extends Component {

  state = {
    count: this.props.num
  }

  // props改变时执行此函数，可以同步最新的props到state中 （新版本已废弃）
  componentWillReceiveProps(nextProps) {
    console.log('最新的props', nextProps)
    // console.log('当前的props', this.props)
    if (nextProps.num > this.state.count) {
      this.setState({
        count: nextProps.num
      })
    }
  }

  render() {
    return (
      <div className="child">
        <h1>Child1</h1>
        <p>{this.state.count}</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>+</button>
      </div>
    )
  }
}
 