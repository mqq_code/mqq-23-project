import React, { Component } from 'react'
// import Child from './Child'
// import Child1 from './Child1'
import './App.scss'


class App extends Component {
  // 页面渲染相关的数据
  state = {
    num: 0,
    visible: false
  }

  h1 = React.createRef(null)
  // 实例属性存数据，改变页面不会自动更新，可以用来存跟页面渲染无关的数据
  n = 100

  shouldComponentUpdate() {
    return true
  }
  
  update = () => {
    this.n++
    console.log(this.n)

    // 强制更新
    this.forceUpdate()
  }

  render() {
    return (
      <div>
        <h1 ref={this.h1}>App</h1>
        <p>{this.state.num}</p>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>+</button>
        <button onClick={this.update}>强制更新{this.n}</button>
        {/* <button onClick={() => this.setState({ visible: !this.state.visible })}>toggle</button>
        {this.state.visible && <Child />} */}
        {/* <Child1 num={this.state.num} /> */}
      </div>
    )
  }
}


export default App

