import React, { Component } from 'react'

export default class Child extends Component {

  state = {
    count: 0
  }
  timer = null
  componentDidMount() {
    this.timer = setInterval(() => {
      console.log(this.state.count + 1)
      this.setState({
        count: this.state.count + 1
      })
    }, 1000)

    document.addEventListener('click', this.clickFn)
  }

  clickFn = () => {
    console.log('点击页面', this.state.count)
    this.setState({
      count: this.state.count + 1
    })
  }

  // 组件销毁之前（可以清除异步任务，例如定时器、原生事件等）
  componentWillUnmount() {
    clearInterval(this.timer)
    document.removeEventListener('click', this.clickFn)
  }


  render() {
    return (
      <div className="child">
        <h2>子组件</h2>
        <p>{this.state.count}</p>
      </div>
    )
  }
}
