import React, { Component } from 'react'
import Child1 from './Child1'
import './App.scss'


class App extends Component {
  // 页面渲染相关的数据
  state = {
    num: 10,
    list: [],
  }

  keydown = e => {
    if (e.keyCode === 13) {
      this.setState({
        list: [...this.state.list, {
          id: Date.now(),
          text: e.target.value
        }]
      })
    }
  }
  listRef = React.createRef(null)

  getSnapshotBeforeUpdate() {
    // 把更新前的信息传给 componentDidUpdate 第三个参数
    const { scrollTop, clientHeight, scrollHeight } = this.listRef.current
    return {
      isBottom: scrollTop + clientHeight >= scrollHeight
    }
  }

  componentDidUpdate(prevprops, prevstate, info) {
    if (info.isBottom) {
      this.listRef.current.scrollTop += 41
    }
  }
  

  render() {
    const { list } = this.state
    return (
      <div>
        <p>{this.state.num}</p>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>+</button>
        <Child1 num={this.state.num} />
        <hr />
        <input type="text" onKeyDown={this.keydown} />
        <ul ref={this.listRef}>
          {list.map((item,index) => <li key={item.id}>{index} - {item.text}</li>)}
        </ul>
      </div>
    )
  }
}


export default App

