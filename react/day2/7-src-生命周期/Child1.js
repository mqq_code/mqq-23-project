import React, { Component } from 'react'

export default class Child1 extends Component {

  state = {
    count: this.props.num
  }

  // 当state依赖于props更新时可以在此函数中更新state数据
  static getDerivedStateFromProps(props, state) {
    // 此函数返回的对象会合并到state中
    if (state.count < props.num) {
      return {
        count: props.num
      }
    }
    return null
  }

  render() {
    return (
      <div className="child">
        <h1>Child1</h1>
        <p>{this.state.count}</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>+</button>
      </div>
    )
  }
}
 