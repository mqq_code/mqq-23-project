import React, { Component, PureComponent } from 'react'

// PureComponent: 性能优化，会内部实现 shouldComponentUpdate 对 state 和 props 的所有属性进行浅比较
export default class Child1 extends PureComponent {

  state = {
    count: this.props.num,
    text: 'text',
    obj: {
      age: 10
    }
  }

  // 当state依赖于props更新时可以在此函数中更新state数据
  static getDerivedStateFromProps(props, state) {
    // 此函数返回的对象会合并到state中
    if (state.count < props.num) {
      return {
        count: props.num
      }
    }
    return null
  }

  // 优化性能，减少不必要的更新
  // shouldComponentUpdate(nextProps, nextState) {
  //   // console.log(nextState) // 最新的props和state
  //   // console.log(this.state)
  //   if (nextState.count !== this.state.count || nextProps.num !== this.props.num || nextState.text !== this.state.text) {
  //     return true
  //   }
  //   return false
  // }

  render() {
    console.log('Child render函数')
    return (
      <div className="child">
        <h1>Child1</h1>
        <p>{this.state.count}</p>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>+</button>
        <p>
          <input type="text" value={this.state.text} onChange={e => this.setState({ text: e.target.value})} />
          {this.state.text}
        </p>
        <p>state.obj.age: {this.state.obj.age}</p>
        <button onClick={() => {
          const obj = {...this.state.obj}
          obj.age++
          this.setState({
            obj: obj
          })
        }}>agge+</button>
      </div>
    )
  }
}
 