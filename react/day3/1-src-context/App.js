import React, { Component } from 'react'
import './App.scss'
import Child1 from './components/Child1'
import { Provider } from './context/ctx'


class App extends Component {

  state = {
    num: 0
  }
  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }
  render() {

    const obj = {
      num: this.state.num,
      changeNum: this.changeNum
    }

    return (
      // 把 value 中的数据传给所有后代组件
      <Provider value={obj}>
        <div className="box">
          <h1>App: {this.state.num}</h1>
          <button onClick={() => this.changeNum(1)}>+</button>
          <Child1 />
        </div>
      </Provider>
    )
  }
}

export default App
