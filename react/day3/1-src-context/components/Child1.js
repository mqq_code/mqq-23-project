import React, { Component } from 'react'
import Child2 from './Child2'

class Child1 extends Component {
  render() {
    return (
      <div className="box">
        <h2>Child1</h2>
        <Child2 />
      </div>
    )
  }
}

export default Child1
