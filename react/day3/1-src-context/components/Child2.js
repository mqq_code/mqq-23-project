import React, { Component } from 'react'
import Child3 from './Child3'
import { Consumer } from '../context/ctx'


class Child2 extends Component {

  renderDom = value => {
    return (
      <div className="box">
        <h3>Child2</h3>
        <Child3 />
      </div>
    )
  }

  render() {
    return (
      <Consumer>
        {this.renderDom}
      </Consumer>
    )
  }
}

export default Child2
