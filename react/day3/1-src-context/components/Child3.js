import React, { Component } from 'react'
import { Consumer } from '../context/ctx'

class Child3 extends Component {

  renderDom = value => {
    // 获取 Provider 组件传过来的参数
    console.log(value)
    return (
      <div className="box">
        <h4>Child3</h4>
        <p>app组件中的num: {value.num}</p>
        <button onClick={() => value.changeNum(-1)}>-1</button>
      </div>
    )
  }

  render() {
    return (
      <Consumer>
        {this.renderDom}
      </Consumer>
    )
  }
}

export default Child3
