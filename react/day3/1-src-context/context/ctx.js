import { createContext } from 'react'


// context对象处理跨级传值
const ctx = createContext()

export const Provider = ctx.Provider
export const Consumer = ctx.Consumer

export default ctx
