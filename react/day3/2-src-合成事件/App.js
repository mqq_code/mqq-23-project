import React, { Component } from 'react'
import './App.scss'


class App extends Component {
  constructor(props) {
    super(props)
    this.sub = this.sub.bind(this)
  }

  state = {
    num: 0
  }

  changeNum = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  sub() {
    console.log(this)
  }

  sub1 = e => {
    console.log(e)
    console.log(this)
  }

  render() {
    return (
      <div className="box">
        <h1>App: {this.state.num}</h1>
        {/*
          合成事件：
            react中的事件没有绑定到真实dom元素上
            利用事件冒泡的原理把事件绑定到了document上（v18之后是root元素）
            触发事件时去事件池中查找需要执行的函数调用
        */}
        {/* {React.createElement('button', {
          onClick: this.sub
        }, ['-'])} */}
        {/* <button onClick={() => this.sub()}>-</button> */}
        {/* <button onClick={this.sub.bind(this)}>-</button> */}
        {/* <button onClick={this.sub}>-</button> */}
        <button onClick={this.sub1}>-</button>

        <button onClick={e => {
          // e: 合成事件对象，想要获取原生事件对象 e.nativeEvent
          console.log(e)
          console.log(e.nativeEvent)
          this.changeNum(1)
        }}>-</button>
        <button onClick={(e) => this.changeNum(1)}>+</button>
      </div>
    )
  }
}

export default App
