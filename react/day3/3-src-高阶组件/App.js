import React, { Component } from 'react'
import './App.scss'
// import Child1 from '../src/components/Child1'
import Child2 from '../src/components/Child2'
// import Child3 from '../src/components/Child3'
// import withPos from './hoc/withPos'


 class App extends Component {
  state = {
    show: true,
  }
  child2Ref = React.createRef(null)
  child3Ref = React.createRef(null)
  render() {
    // const { show } = this.state
    // const { x, y } = this.props
    return (
      <div className="app box">
        <h1>App</h1>
        {/* <p>鼠标近距离左侧: {x}</p>
        <p>鼠标距离顶部: {y}</p>
        <button onClick={() => this.setState({ show: !show })}>显示隐藏child1</button> */}
        {/* {show && <Child1 />} */}

        <button onClick={() => {

          console.log(this.child2Ref.current)
          
        }}>获取child2实例对象</button>

        <Child2 ref={this.child2Ref} title="第二个组件" a="100" />

        {/* <button onClick={() => {
          // 通过 ref 获取类组件的实例对象，可以调用子组件的方法和数据
          console.log(this.child3Ref.current)
          this.child3Ref.current.add()
        }}>调用child3中的add函数</button>

        <Child3 ref={this.child3Ref} /> */}
      </div>
    )
  }
}


export default App // withPos(App)