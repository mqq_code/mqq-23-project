import React, { Component } from 'react'
import withPos from '../hoc/withPos'

class Child2 extends Component {
  boxRef = React.createRef(null)

  getPos = () => {
    const { x, y } = this.props
    let l = x
    let t = y
    if (this.boxRef.current) {
      // 获取父元素的位置
      const { left, top, width, height } = this.boxRef.current.getBoundingClientRect()
      if (x < left) l = left
      if (x > left + width - 100) l = left + width - 100
      if (y < top) t = top
      if (y > top + height - 100) t = top + height - 100
    }
    return {
      top: t,
      left: l
    }
  }


  render() {
    return (
      <div className="box" ref={this.boxRef}>
        <h2>{this.props.title}</h2>
        <input type="text" />
        <button>提交</button>
        <div className="rect" style={this.getPos()}></div>
      </div>
    )
  }
}

export default withPos(Child2)
