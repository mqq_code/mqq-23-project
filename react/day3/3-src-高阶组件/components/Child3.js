import React, { Component } from 'react'

class Child3 extends Component {
  state = {
    num: 0
  }
  add = () => {
    this.setState({
      num: this.state.num + 1
    })
  }
  render() {
    return (
      <div className="box">
        <h3>Child3</h3>
        <p>{this.state.num}</p>
        <button onClick={this.add}>+</button>
      </div>
    )
  }
}


export default Child3