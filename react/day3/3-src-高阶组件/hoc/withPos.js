import React, { forwardRef } from 'react'

// 高阶组件：参数为组件，返回值为新组件的函数。（处理公共逻辑，给组件添加额外的功能）

function withPos(Com) {
  class WithPosCom extends React.Component {

    state = {
      x: null,
      y: null
    }
  
    mousemove = e => {
      this.setState({
        x: e.pageX,
        y: e.pageY
      })
    }
  
    componentDidMount() {
      document.addEventListener('mousemove', this.mousemove)
    }
  
    componentWillUnmount() {
      document.removeEventListener('mousemove', this.mousemove)
    }


    render() {
      const { x, y } = this.state
      const { postRef, ...restProps } = this.props
      // postRef: 获取父组件传过来的ref
      const newProps = {
        ...restProps,
        x,
        y
      }
      return <Com {...newProps} ref={postRef} ></Com>
    }

  }

  // 通过 forwardRef 获取父组件传过来的ref对象，把ref转发给原本的组件
  return forwardRef((props, ref) => {
    return <WithPosCom {...props} postRef={ref}></WithPosCom>
  })
}

export default withPos






