import React, { Component } from 'react'
import axios from 'axios'

class App extends Component {

  async componentDidMount() {
    const res = await axios.get('/api/list')
    console.log(res.data)
  }


  render() {
    return (
      <div>App</div>
    )
  }
}

export default App
