import React, { useState } from 'react'

// react v16.8 之前不可以定义状态，16.8之后新增 hooks 让函数组件可以代替类组件
// hook: 名称以 use 开头的函数
// hook 必须在函数组件的最顶层使用，不能再if for 子函数中使用（react通过hook的调用顺序管理状态，放在if或者循环中可能会导致顺序不正确）
// hook 只能在函数组件和自定义 hook 中使用

const App = (props) => {

  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('默认标题')
  const [list, setList] = useState([])
  const [xm, setXm] = useState({ name: '小明', age: 20 })

  const keydown = e => {
    if (e.keyCode === 13) {
      setList([
        {
          id: Date.now(),
          text: e.target.value
        },
        ...list
      ])
    }
  }
  const remove = id => {
    setList(list.filter(v => v.id !== id))
  }
  const add = () => {
    // 更新数据是异步执行
    setNum(prev => {
      // prev 最新值
      return prev + 1
    })
    setNum(prev => prev + 1)
    setNum(prev => prev + 1)
  }
  const addAge = () => {
    setXm({ ...xm, age: xm.age + 1 })
  }
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} onKeyDown={keydown} />
      <ul>
        {list.map(item =>
          <li key={item.id}>
            {item.text}
            <button onClick={() => remove(item.id)}>删除</button>
          </li>
        )}
      </ul>
      <hr />
      {num > 0 &&
        <>
          <button onClick={() => setNum(num - 1)}>-</button>
          <span>{num}</span>
        </>
      }
      <button onClick={add}>+</button>
      <hr />
      <p>姓名: {xm.name} <input type="text" value={xm.name} onChange={e => setXm({...xm, name: e.target.value})} /></p>
      <p>年龄: {xm.age} <button onClick={addAge}>+</button></p>
    </div>
  )
}


export default App





