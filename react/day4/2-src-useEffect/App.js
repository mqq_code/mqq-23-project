import React, { useState, useEffect } from 'react'
import Child from './components/Child'
import './App.scss'
import axios from 'axios'

// react v16.8 之前不可以定义状态，16.8之后新增 hooks 让函数组件可以代替类组件
// hook: 名称以 use 开头的函数
// hook 使用规则
// hook 必须在函数组件的最顶层使用，不能再if、循环、子函数中使用（react通过hook的调用顺序管理状态，放在if或者循环中可能会导致顺序不正确）
// hook 只能在函数组件和自定义 hook 中使用

// useEffect: 处理组件中的副作用（可以实现类似于类组件中的生命周期的功能）
// useEffect(callback, [依赖项])

/*
  注意: 回调函数是在页面更新后执行，所以可以获取最新的dom元素
  注意: useEffet第一个参数不能是 async 函数，因为该回调函数需要 return 一个函数，async 返回的时 Promise 对象
  1. 依赖项传入 [], 回调函数函数只执行一次，类似 componentDidMount
  2. 依赖项传入具体变量，该变量更新会执行回调函数
  3. 依赖项不传，任何state变量改变都会执行回调函数，类似 componentDidUpdate
  4. 回调函数中 return 的函数会在组件销毁时执行，类似 componentWillUnmount
*/

const App = (props) => {

  const [num, setNum] = useState(0)
  const [list, setList] = useState([])
  const [title, setTitle] = useState('标题')
  const [obj, setObj] = useState({ name: '默认', age: 10 })

  useEffect(() => {
    console.log('num改变了', num)
    console.log(document.querySelector('h1').outerHTML)
  }, [])

  useEffect(() => {
    console.log('title变了', title)
  }, [title])

  // 注意useEffet第一个参数不能是 async 函数，因为该回调函数需要 return 一个函数，async 返回的时 Promise 对象
  // useEffect(async () => {
  //   const res = await axios.get('/api/list')
  //   console.log(res.data)
  // }, [])

  const getList = async () => {
    const res = await axios.get('/api/list')
    console.log(res.data)
  }

  useEffect(() => {
    getList()
  }, [])

  useEffect(() => {
    console.log('obj改变了', obj)
  }, [obj.age])


  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <div>
        <button onClick={() => setNum(num - 1)}>-</button>
        {num}
        <button onClick={() => setNum(num + 1)}>+</button>
      </div>
      {num > 0 && <Child />}
      <p>姓名: {obj.name} <button onClick={() => setObj({ ...obj, name: Math.random() })}>改名字</button></p>
      <p>年龄: {obj.age} <button onClick={() => setObj({ ...obj, age: obj.age + 1 })}>+</button></p>
    </div>
  )
}


export default App

