import React, { useEffect, useState } from 'react'

const Child = () => {
  const [num, setNum] = useState(0)

  useEffect(() => {
    const timer = setInterval(() => {
      // useEffect依赖项传入空数组，回调函数内部无法获取到最新数据，改变数据时需要通过传入回调函数的方法修改数据
      setNum(prev => { // prev 最新的数据
        if (prev + 1 >= 10) {
          clearInterval(timer)
        }
        return prev + 1
      })
      // setNum(num + 1)
    }, 1000)

    return () => {
      // 组件销毁时执行
      clearInterval(timer)
    }
  }, [])


  return (
    <div className="box">
      定时器：{num}
    </div>
  )
}

export default Child