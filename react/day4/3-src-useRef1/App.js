import React, { useState, useEffect, useRef } from 'react'
import './App.scss'

class Child extends React.Component {
  state = {
    title: 'child'
  }
  changeTitle = title => {
    this.setState({ title })
  }
  render() {
    return <div>{this.state.title}</div>
  }
}


const App = (props) => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('标题')
  const inp = useRef() // 获取dom元素
  const n = useRef(0) // 存和组件渲染无关的数据，ref的数据改变不会引起组件重新渲染，组件重新渲染时也不会清空ref的数据
  const timer = useRef(null)
  const childRef = useRef(null) // 获取类组件实例对象

  const submit = () => {
    n.current ++
    console.log(inp.current)
    console.log('按钮点击次数', n)
  }

  const start = () => {
    timer.current = setInterval(() => {
      setNum(n => n + 1)
    }, 1000)
  }
  const stop = () => {
    clearInterval(timer.current)
  }
 
  return (
    <div>
      <h1>{title} <input type="text" value={title} onChange={e => setTitle(e.target.value)} /></h1>
      <input type="text" ref={inp} />
      <button onClick={submit}>提交</button>
      <div>定时器： {num}</div>
      <button onClick={start}>开始</button>
      <button onClick={stop}>暂停</button>
      <hr />
      <button onClick={() => {
        console.log(childRef.current)
        // 使用ref获取子组件实例对象
        childRef.current.changeTitle(Math.random())
      }}>获取child组件实例</button>
      <Child ref={childRef} />
    </div>
  )
}


export default App

