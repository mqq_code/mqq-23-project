import React, { useState, useEffect, useRef } from 'react'
import './App.scss'
import Child from './components/Child'

const App = (props) => {
  const [title, setTitle] = useState('标题')
  // ref无法获取函数子组件实例对象，因为函数组件没有实例对象
  // useRef 可以配合  forwardRef 和 useImperativeHandle 获取到子组件提供的数据和方法
  const childRef = useRef()

  return (
    <div>
      <h1>{title} <input type="text" value={title} onChange={e => setTitle(e.target.value)} /></h1>

      <button onClick={() => {
        console.log(childRef.current)
        // 获取子组件的数据和方法
        childRef.current.setNum(childRef.current.num + 2)
      }}>获取子组件实例对象</button>

      <hr />
      <Child ref={childRef} test="abcdefg" a="100" />
    </div>
  )
}


export default App

