import React, { useState, forwardRef, useImperativeHandle } from 'react'

const Child = (props, ref) => {
  // ref: 父组件传过来的ref对象

  const [num, setNum] = useState(0)

  // 使用此hook给父组件闯过来的ref赋值
  useImperativeHandle(ref, () => {
    // 把此函数的返回值赋值给父组件传过来的ref对象的current属性
    return {
      num,
      setNum
    }
  }, [num]) // 依赖项，依赖项改变执行函数更新给ref的数据


  const add = n => {
    setNum(num + n)
  }

  return (
    <div>
      <h2>Child</h2>
      <button onClick={() => add(1)}>+</button> {num}
    </div>
  )
}

// forwardRef: 把父组件传过来的 ref 对象转发到 Child 组件的第二个参数
export default forwardRef(Child)