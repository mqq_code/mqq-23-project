import React, { useEffect, useRef, useState } from 'react'
import axios from 'axios'
import classNames from 'classnames'
import './App.scss'

const App = () => {
  const [list, setList] = useState([])
  const [curIndex, setCurIndex] = useState(0)
  const [showMask, setShowMask] = useState(false)
  const [maskPos, setMaskPos] = useState({ top: 0, left: 0 })
  const imgBoxRef = useRef(null)

  const getlist = async () => {
    const res = await axios.get('/api/list')
    setList(res.data)
  }

  useEffect(() => {
    getlist()
  }, [])

  const mouseMove = (e) => {
    // console.log(e.clientX, e.clientY)
    const { left, top, height, width } = imgBoxRef.current.getBoundingClientRect()
    let t = e.clientY - top - 75
    let l = e.clientX - left - 75
    if (t <= 0) t = 0
    if (l <= 0) l = 0
    if (t >= height - 150) t = height - 150
    if (l >= width - 150) l = width - 150
    setMaskPos({
      top: t,
      left: l
    })
  }

  return (
    <div className="app">
      <div className="left" >
        <div className="img-box" ref={imgBoxRef} onMouseMove={mouseMove} onMouseEnter={() => setShowMask(true)} onMouseLeave={() => setShowMask(false)}>
          <img src={list[curIndex]} alt="" />
          <div className={classNames('mask', { show: showMask })} style={maskPos}></div>
        </div>
        <ul>
          {list.map((item, index) =>
            <li
              key={item}
              className={classNames({ active: curIndex === index })}
              onClick={() => setCurIndex(index)}
            >
              <img src={item} alt="" />
            </li>
          )}
        </ul>
        <div className={classNames('right', { show: showMask })}>
          <img src={list[curIndex]} alt="" style={{ top: maskPos.top * -3, left: maskPos.left * -3 }} />
        </div>
      </div>
    </div>
  )
}

export default App