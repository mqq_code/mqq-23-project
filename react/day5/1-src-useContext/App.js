import React, { useState } from 'react'
import './App.scss'
import Child1 from './components/Child1'
import { Provider } from './context/ctx'


const App = () => {

  const [title, setTitle] = useState('App')

  return (
    <Provider value={{ title, a: 100 }}>
      <div className="box">
        <h1>{title}</h1>
        <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
        <Child1 />
      </div>
    </Provider>
  )
}

export default App