import React from 'react'
import Child2 from './Child2'

const Child1 = (props) => {
  return (
    <div className="box">
      <h2>Child1</h2>
      <Child2  />
    </div>
  )
}

export default Child1