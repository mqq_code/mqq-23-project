import React from 'react'
import Child3 from './Child3'

const Child2 = (props) => {
  return (
    <div className="box">
      <h3>Child2</h3>
      <Child3 />
    </div>
  )
}

export default Child2