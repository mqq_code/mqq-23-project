import React, { useContext } from 'react'
import ctx from '../context/ctx'

const Child3 = () => {
  const obj = useContext(ctx) // 接收context对象提供的value

  return (
    <div className="box">
      <h4>Child3</h4>
      <p>App数据: {obj.title}</p>
    </div>
  )
}

export default Child3