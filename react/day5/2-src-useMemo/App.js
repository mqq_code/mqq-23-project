import React, { useState, useMemo } from 'react'
import './App.scss'


const App = () => {

  const [title, setTitle] = useState('App')
  const [xm, setXm] = useState({ name: '小明', age: 20 })
  const [list, setList] = useState([])

  const getInfo = () => {
    // console.log('计算info函数')
    return `当前标题是:${title}, 我的名字是:${xm.name}`
  }
  // 返回一个缓存的值，依赖项更新时执行函数计算结果，依赖项没有更新从缓存中读取数据
  const info = useMemo(() => {
    // console.log('useMemo 计算info')
    return `当前标题是:${title}, 我的名字是:${xm.name}`
  }, [title, xm.name])




  const add = e => {
    if (e.keyCode === 13) {
      setList([...list, {
        id: Date.now(),
        name: e.target.value,
        price: Math.floor(Math.random() * 100)
      }])
      e.target.value = ''
    }
  }

  // const getTotal = () => {
  //   return list.reduce((prev, val) => {
  //     console.log(val.price)
  //     return prev + val.price
  //   }, 0)
  // }
  
  const total = useMemo(() => {
    return list.reduce((prev, val) => {
      return prev + val.price
    }, 0)
  }, [list])

  return (
    <div className="box">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      <p>姓名: {xm.name} <input type="text" value={xm.name} onChange={e => setXm({ ...xm, name: e.target.value })}  /></p>
      <p>年龄: {xm.age} <button onClick={() => setXm({ ...xm, age: xm.age + 1 })}>+</button></p>
      <hr />
      {getInfo()}
      <p>useMemo: {info}</p>
      <hr />
      <input type="text" onKeyDown={add} />
      <ul>
        {list.map(item => <li key={item.id}>
          <h3>{item.name}</h3>
          <p>价格: ¥{item.price}</p>
        </li>)}
      </ul>
      <footer>总价: {total}</footer>
    </div>
  )
}

export default App