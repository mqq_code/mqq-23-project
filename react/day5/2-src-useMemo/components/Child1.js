import React from 'react'

const Child1 = (props) => {
  return (
    <div className="box">
      <h2>Child1</h2>
    </div>
  )
}

export default Child1