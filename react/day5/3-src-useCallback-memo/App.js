import React, { useState, useMemo, useCallback } from 'react'
import './App.scss'
import Child1 from './components/Child1'

window.arr = []

const App = () => {

  const [title, setTitle] = useState('App')
  const [num, setNum] = useState(0)
  const [obj, setObj] = useState({ age: 20 })
  const [arr, setArr] = useState([])

  // const add = n => {
  //   setNum(num + n)
  // }
  // 返回一个缓存的函数，依赖项更新时重新创建，依赖项没有更新从缓存中读取
  const add = useCallback(n => {
    setNum(num + n)
  }, [num])

  // 可以使用 useMemo 实现 useCallback 的功能
  // const add = useMemo(() => {
  //   return n => {
  //     setNum(num + n)
  //   }
  // }, [num])

  return (
    <div className="box">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <div>
        <button onClick={() => add(1)}>+</button>{num}
      </div>
      <div>
        年龄: {obj.age}
        <button onClick={() => {
          setObj({ age: obj.age + 1 })
        }}>+</button>
      </div>
      <div>
        arr: {arr}
        <button onClick={() => {
          const arr1 = [...arr]
          arr1.push(arr.length)
          setArr(arr1)
        }}>push</button>
      </div>
      <Child1 num={num} add={add} obj={obj} arr={arr} />
    </div>
  )
}

export default App