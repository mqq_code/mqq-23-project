import React, { useState, memo } from 'react'

const Child1 = (props) => {
  const [show, setShow] = useState(true)

  console.log('渲染Child1', props)

  return (
    <div className="box">
      <h2>Child1</h2>
      <div>父组件的num: {props.num} <button onClick={() => props.add(-1)}>-</button></div>
      <hr />
      <button onClick={() => setShow(!show)}>toggle</button>
      {show &&
        <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
        </ul>
      }
    </div>
  )
}

// memo: 高阶组件，优化性能
// 浅比较当前 props 和最新的 props 是否一致，不一样就更新，一样就阻止更新
export default memo(Child1)

// 如果需要深层比较props内的数据，可以传入第二个参数进行比较
// export default memo(Child1, (prevProps, props) => {
//   console.log('之前的props', prevProps)
//   console.log('最新的props', props)
//   // return true: 阻止更新， false: 不阻止更新
//   return prevProps.num === props.num
// })
