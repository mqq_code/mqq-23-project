import React, { useState, useEffect, useLayoutEffect } from 'react'
import './App.scss'

const App = () => {

  const [title, setTitle] = useState('App')

  // 页面渲染完成执行回调函数
  // useEffect(() => {
  //   console.log(document.querySelector('h1').outerHTML, title)
  //   if (title === '') {
  //     setTitle('ABC')
  //   }
  // }, [title])

  
  // dom更新完成，浏览器绘制之前完成
  useLayoutEffect(() => {
    console.log(document.querySelector('h1').outerHTML, title)
    if (title === '') {
      setTitle('ABC')
    }
  }, [title])

  return (
    <div className="box">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <button onClick={() => {
        setTitle('')
      }}>改变title</button>
    </div>
  )
}

export default App