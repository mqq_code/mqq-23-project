import React, { useState, useReducer } from 'react'
import './App.scss'


const reducer = (state, action) => {
  if (action.type === 'CHANGE_NAME') {
    return { ...state, name: action.payload }
  } else if (action.type === 'CHANGE_AGE_SEX') {
    return { ...state, age: action.payload.age, sex: action.payload.sex }
  }
  return state
}

const App = () => {

  const [title, setTitle] = useState('App')
  // const [user, setUser] = useState({ name: '小明', age: 20, sex: '男' })

  const [state, dispatch] = useReducer(reducer, { name: '小明', age: 20, sex: '男' }) // 存储操作复杂的数据，数据改变页面更新

  return (
    <div className="box">
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <hr />
      <p>姓名: {state.name}</p>
      <p>年龄: {state.age}</p>
      <p>性别: {state.sex}</p>
      <button onClick={() => {
        // 发送action通知reducer修改数据
        dispatch({
          type: 'CHANGE_NAME', // 每次修改内容的描述
          payload: '王小明' + Math.random()
        })
      }}>修改姓名</button>
      <button onClick={() => {
        dispatch({
          type: 'CHANGE_AGE_SEX',
          payload: {
            age: 100 + Math.random(),
            sex: Math.random() > 0.5 ? '男' : '女'
          }
        })
      }}>修改年龄和性别</button>
      {/* <p>姓名: {user.name}</p>
      <p>年龄: {user.age}</p>
      <p>性别: {user.sex}</p>
      <button onClick={() => {
        setUser({ ...user, name: '王小明' + Math.random() })
      }}>修改姓名</button>
      <button onClick={() => {
        setUser({ ...user, age: user.age + Math.random(), sex: Math.random() })
      }}>修改年龄和性别</button> */}
    </div>
  )
}

export default App