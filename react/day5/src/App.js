import React, { useCallback, useEffect, useRef, useState } from 'react'
import axios from 'axios'
import './App.scss'
import Keyboard from './components/keyboard/Keyboard'
import Control from './components/control/Control'

const App = () => {
  const [list, setList] = useState(new Array(9).fill({}))
  const [curMusic, setMusic] = useState({}) // 当前播放的音乐
  const [power, setPower] = useState(true) // 开关
  const [bank, setBank] = useState(false) // bank开关
  const audio = useRef(new Audio()) // 音乐对象
  const [curVolume, setVolume] = useState(0.5) // 当前音量



  const preLoad = (url) => {
    const aud = new Audio()
    aud.src = url
    aud.load()
  }

  useEffect(() => {
    axios.get('/api/list').then(res => {
      const list = res.data.map(item => ({ ...item, active: false }))
      setList(list)
      list.forEach(item => {
        // 预加载音频
        preLoad(item.bankUrl)
        preLoad(item.url)
      })
    })
  }, [])

  const changeActive = (item, active) => {
    const newlist = [...list] // 拷贝数据
    const index = newlist.findIndex(v => v === item) // 查找当前点击的对象
    newlist[index].active = active // 改变高亮
    if (power && active) { // 按钮开启并且按下，存音乐
      setMusic(item)
      // 播放音乐
      const url = bank ? item.bankUrl : item.url
      audio.current = new Audio()
      audio.current.src = url
      audio.current.volume = curVolume
      audio.current.load()
      audio.current.play()
    }
    setList(newlist)
  }

  useEffect(() => {
    // 关闭按钮清空当前音乐
    if (!power) {
      setMusic({})
    }
  }, [power])


  return (
    <div className="app">
      <Keyboard list={list} power={power} change={changeActive} />
      <Control
        title={curMusic.id}
        power={power}
        setPower={setPower}
        bank={bank}
        setBank={setBank}
        changeVolume={setVolume}
        curVolume={curVolume}
      />
    </div>
  )
}

export default App