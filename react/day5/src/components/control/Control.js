import React, { useEffect, useLayoutEffect, useRef, useState } from 'react'
import style from './control.module.scss'
import classNames from 'classnames'

const Control = (props) => {
  const volumeRef = useRef(null)
  const isDown = useRef(false) // 是否按下音量
  const [left, setLeft] = useState(0)
  const mousedown = e => {
    isDown.current = true
    console.log('mousedown')
  }
  const mousemove = e => {
    if (isDown.current) {
      const { left, width } = volumeRef.current.getBoundingClientRect()
      let l = e.clientX - left - 4
      if (l < 0) l = 0
      if (l > width - 8) l = width - 8
      setLeft(l)
      props.changeVolume(l / (width - 8))
      console.log('当前音量', l / (width - 8))
    }
  }
  const mouseup = e => {
    isDown.current = false
  }
  useEffect(() => {
    document.addEventListener('mousemove', mousemove)
    document.addEventListener('mouseup', mouseup)
    return () => {
      document.removeEventListener('mousemove', mousemove)
      document.removeEventListener('mouseup', mouseup)
    }
  }, [])

  useLayoutEffect(() => {
    const l = (volumeRef.current.clientWidth - 8) * props.curVolume
    setLeft(l)
  }, [])

  return (
    <div className={style.control}>
      <div className={style.switch}>
        <b>power</b>
        <p onClick={() => props.setPower(!props.power)}>
          <span className={classNames({ [style.on]: props.power })}></span>
        </p>
      </div>
      <div className={style.title}>{props.title}</div>
      <div className={style.volume} ref={volumeRef}>
        <i style={{ left }} onMouseDown={mousedown}></i>
      </div>
      <div className={style.switch}>
        <b>bank</b>
        <p onClick={() => props.setBank(!props.bank)}>
          <span className={classNames({ [style.on]: props.bank })}></span>
        </p>
      </div>
    </div>
  )
}

export default Control