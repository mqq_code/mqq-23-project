import React, { useEffect } from 'react'
import style from './keyboard.module.scss'
import classNames from 'classnames'

const Keyboard = (props) => {

  useEffect(() => {
    // 绑定键盘按下和抬起事件
    const keydown = e => {
      const item = props.list.find(v => v.keyCode === e.keyCode)
      console.log('keydown')
      if (item) {
        props.change(item, true)
      }
    }
    const keyup = e => {
      const item = props.list.find(v => v.keyCode === e.keyCode)
      if (item) {
        props.change(item, false)
      }
    }
    document.addEventListener('keydown', keydown)
    document.addEventListener('keyup', keyup)
    return () => {
      document.removeEventListener('keydown', keydown)
      document.removeEventListener('keyup', keyup)
    }
  }, [props.list, props.change])

  return (
    <div className={style.keyboard}>
      <ul>
        {props.list.map((item, index) =>
          <li
            key={index}
            className={classNames({ [style.active]: props.power && item.active, [style.isDown]: item.active })}
            onMouseDown={() => props.change(item, true)}
            onMouseUp={() => props.change(item, false)}
          >{item.keyTrigger}</li>
        )}
      </ul>
    </div>
  )
}

export default Keyboard





