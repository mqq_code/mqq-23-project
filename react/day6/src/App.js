import React from 'react'
import './App.scss'
import Child1 from './components/Child1'
import Child2 from './components/Child2'



const App = () => {

  return (
    <div className="app box">
      <Child1 />
      <Child2 />
    </div>
  )
}

export default App