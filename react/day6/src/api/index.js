import axios from "axios"


export const getBanner = () => {
  return axios.get('https://zyxcl.xyz/banner')
}

export const getToplist = (params) => {
  return axios.get('https://zyxcl.xyz/toplist', { params })
}