import React, { useEffect, useState } from 'react'
import usePos from '../hooks/usePos'
import useRequest from '../hooks/useRequest'
import { getToplist } from '../api'


const Child1 = () => {
  const { x } = usePos()

  const { data, loading, run } = useRequest(getToplist, { a: 1 }, false)

  return (
    <div className="box">
      <h2>Child1</h2>
      <button onClick={() => {
        run({ a: Math.random() })
      }}>刷新</button>
      <div>x: {x}</div>
      <ul>
        {data.list && data.list.map(item => <li key={item.id}>{item.name}</li>)}
      </ul>
      {loading && <div className="loading">加载中。。。</div>}
    </div>
  )
}

export default Child1