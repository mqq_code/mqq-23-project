import React, { useEffect, useState } from 'react'
import usePos from '../hooks/usePos'
import { getBanner } from '../api'
import useRequest from '../hooks/useRequest'

const Child2 = () => {
  const pos = usePos()

  const { data, loading, run } = useRequest(getBanner)

  return (
    <div className="box">
      <h2>Child2</h2>
      <p>y: {pos.y}</p>
      <ul>
        {data.banners && data.banners.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
      {loading && <div className="loading">加载中。。。</div>}
    </div>
  )
}

export default Child2