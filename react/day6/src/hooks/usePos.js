import { useEffect, useState } from 'react'

// 自定义hook：复用逻辑
// 名字必须以 use 开头的函数

const usePos = () => {
  const [pos, setPos] = useState({ x: 0, y: 0 })

  useEffect(() => {
    const mousemove = e => {
      setPos({
        x: e.clientX,
        y: e.clientY
      })
    }
    document.addEventListener('mousemove', mousemove)
    return () => {
      document.removeEventListener('mousemove', mousemove)
    }
  }, [])

  return pos
}


export default usePos