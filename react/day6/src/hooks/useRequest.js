import { useState, useEffect, useCallback } from 'react'

const useRequest = (api, params = {}, immdiate = true) => {


  const [data, setData] = useState({})
  const [loading, setLoading] = useState(false)

  const run = (options) => {
    setLoading(true)
    api(options || params).then(res => {
      setData(res.data)
      setLoading(false)
    })
  }

  useEffect(() => {
    if (immdiate) {
      run()
    }
  }, [])

  return {
    data,
    loading,
    run
  }

}

export default useRequest