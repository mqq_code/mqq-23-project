import React from 'react'
import './App.scss'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import Notfound from './pages/404'
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      {/* exact: 精准匹配 */}
      <Route path="/home" component={Home} />
      <Route exact path="/detail/:id/:a" component={Detail} />
      <Route exact path="/login" render={() => <Login />} />
      <Route exact path="/404" component={Notfound} />
      <Redirect exact from="/" to="/home" />
      <Redirect from="*" to="/404" />
      {/* <Route path="*" component={Notfound} /> */}
    </Switch>
  )
}

export default App