import React from 'react'
import { withRouter, useHistory, useLocation } from 'react-router-dom'

const Child = (props) => {

  // 使用路由提供的 hook 获取路由信息
  const history = useHistory()
  const location = useLocation()

  const goLogin = () => {
    console.log(history)
    console.log(location)
  }
  return (
    <div>
      <h3>我是子组件</h3>
      <button onClick={goLogin}>跳转到登陆</button>
    </div>
  )
}

// withRouter: 高阶组件，给当前组件的props中添加路由信息
export default Child // withRouter(Child)