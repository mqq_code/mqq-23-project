import React from 'react'
import { Link, Switch, Route, Redirect, NavLink } from 'react-router-dom'
import Movie from '../movie/Movie'
import Cinema from '../cinema/Cinema'
import Mine from '../mine/Mine'


const Home = () => {
  return (
    <div className="home">
      <main>
        <Switch>
          <Route exact path="/home/movie" component={Movie} />
          <Route exact path="/home/cinema" render={routeInfo => {
            console.log('routeInfo, 当前路由信息', routeInfo)
            return <Cinema {...routeInfo}></Cinema>
          }} />
          <Route exact path="/home/mine" component={Mine} />
          <Redirect exact from="/home" to="/home/movie" />
        </Switch>
      </main>
      <footer>
        <NavLink activeStyle={{ fontSize: '30px' }} activeClassName="cur" to="/home/movie">电影</NavLink>
        <NavLink activeClassName="cur" to="/home/cinema">影院</NavLink>
        <NavLink activeClassName="cur" to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home