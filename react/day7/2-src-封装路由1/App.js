import React from 'react'
import './App.scss'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Login from './pages/login/Login'
import Notfound from './pages/404'
import Movie from './pages/movie/Movie'
import Cinema from './pages/cinema/Cinema'
import Mine from './pages/mine/Mine'
import Hot from './pages/hot/Hot'
import Coming from './pages/coming/Coming'
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      <Route path="/home" render={routeInfo => {
        return (
          <Home {...routeInfo}>
            <Switch>
              <Route path="/home/movie" render={routeInfo => {
                return (
                  <Movie {...routeInfo}>
                    <Switch>
                      <Route path="/home/movie/hot" component={Hot} />
                      <Route path="/home/movie/coming" component={Coming} />
                      <Redirect from="/home/movie" to="/home/movie/hot" />
                    </Switch>
                  </Movie>
                )
              }} />
              <Route exact path="/home/cinema" component={Cinema} />
              <Route exact path="/home/mine" component={Mine} />
              <Redirect exact from="/home" to="/home/movie" />
            </Switch>
          </Home>
        )
      }} />
      <Route exact path="/detail/:id/:a" component={Detail} />
      <Route exact path="/login" render={() => <Login />} />
      <Route exact path="/404" component={Notfound} />
      <Redirect exact from="/" to="/home" />
      <Redirect from="*" to="/404" />
    </Switch>
  )
}

export default App