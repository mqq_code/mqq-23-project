import React from 'react'
import { useHistory, useLocation, useParams } from 'react-router-dom'

const query = str => {
  const res = {}
  const arr = str.slice(1).split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    res[key] = val
  })
  return res
}

const Detail = (props) => {
  // 获取 ? 后的参数
  // console.log(props.location.search)
  // const searchQuery = query(props.location.search)
  // console.log(searchQuery)

  // 获取state参数
  // console.log(props.location.state)

  // 获取params参数
  console.log(props.match.params)
  const params = useParams()
  console.log(params)

  return (
    <div>详情</div>
  )
}

export default Detail