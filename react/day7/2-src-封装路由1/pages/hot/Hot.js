import React, { useEffect, useState } from 'react'
import axios from 'axios'


const qs = (obj) => {
  let str = ''
  Object.keys(obj).forEach(key => {
    str += `${key}=${obj[key]}&`
  })
  return str.slice(0, str.length - 1)
}

const Hot = (props) => {
    // props: 当组件传给 Route 的 component 属性，会自动把当前路由信息传给 props

    const [list, setList] = useState([])
    useEffect(() => {
      axios.get('https://m.maizuo.com/gateway?cityId=440300&pageNum=1&pageSize=10&type=1&k=687143', {
        headers: {
          'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"16659764383978050249162753"}',
          'X-Host': 'mall.film-ticket.film.list'
        }
      }).then(res => {
        setList(res.data.data.films)
      })
    }, [])
  
    const goDetail = id => {
      console.log(id)
      // search 传参数，把参数拼接到地址栏？后
      // props.history.push('/detail?a=100&b=200&c=300')
      // props.history.push({
      //   pathname: '/detail',
      //   search: `a=100&b=200&c=30&id=${id}`
      // })
      // props.history.push({
      //   pathname: '/detail',
      //   search: qs({ a: 200, id })
      // })
  
      // state 传参数
      // props.history.push({
      //   pathname: '/detail',
      //   state: { id, a: 'AAAA', b: 'BBBB' }
      // })
  
      // params 传参数
      props.history.push(`/detail/${id}/AAA0`)
    }
  return (
    <div className="hot">
      <h1>正在热映</h1>
      <ul>
        {list.map(item =>
          <li key={item.filmId} onClick={() => goDetail(item.filmId)}>
            <h2>{item.name}</h2>
            <img src={item.poster} width={200} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Hot