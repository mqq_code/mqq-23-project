import React from 'react'
import Child from './Child'

const Cinema = (props) => {

  console.log('cinema', props)

  return (
    <div>
      <h1>Cinema</h1>
      <Child />
    </div>
  )
}

export default Cinema