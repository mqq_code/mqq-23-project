import React from 'react'

const menus = [
  {
    label: '菜单1',
    value: '1',
    children: [
      {
        label: '菜单1-1',
        value: '1-1',
        children: [
          {
            label: '菜单1-1-1',
            value: '1-1-1'
          },
          {
            label: '菜单1-1-2',
            value: '1-1-2'
          },
          {
            label: '菜单1-1-3',
            value: '1-1-3'
          }
        ]
      },
      {
        label: '菜单1-2',
        value: '1-2'
      },
      {
        label: '菜单1-3',
        value: '1-3'
      }
    ]
  },
  {
    label: '菜单2',
    value: '2'
  },
  {
    label: '菜单3',
    value: '3'
  },
  {
    label: '菜单4',
    value: '4'
  }
]

const Menu = (props) => {
  return (
    <ul>
      {props.menus.map(item =>
        <li key={item.value}>
          <h2>{item.label}</h2>
          {item.children && <Menu menus={item.children} />}
        </li>
      )}
    </ul>
  )
}

const Mine = () => {

  return (
    <div className='mine'>
      <Menu menus={menus} />
    </div>
  )
}

export default Mine