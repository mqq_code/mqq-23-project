import React from 'react'
import './App.scss'
import RouterView from './router/RouterView'
import routes from './router/routerConfig'
import { Switch, Route, Redirect } from 'react-router-dom'

const App = (props) => {
  return (
    <RouterView routes={routes} />
    // <Switch>
    //   <Route path="/home" render={() => {
    //     console.log('渲染home')
    //     const token = localStorage.getItem('token')
    //     if (!token) {
    //       return <Redirect to="/login" />
    //     }
    //     return <div>home</div>
    //   }} />
    //   <Route path="/list" render={() => {
    //     console.log('渲染list')
    //     return <div>list</div>
    //   }} />
    //   <Route path="/login" render={() => {
    //     console.log('渲染login')
    //     return <div>login</div>
    //   }} />
    // </Switch>
  )
}

export default App
