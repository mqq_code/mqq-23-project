import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App'
import {
  BrowserRouter, // 路由根组件，跟路由相关的所有内容必须包含在根组件内
  HashRouter
} from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Suspense fallback={<div className="loading">loading...</div>}>
      <App />
    </Suspense>
  </BrowserRouter>
);


