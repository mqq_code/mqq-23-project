import React, { lazy, Suspense } from 'react'
// import Child from './Child'

// 组件懒加载
const Child = lazy(() => import(/* webpackChunkName: 'child' */'./Child'))


const Cinema = (props) => {

  // console.log('cinema', props)

  return (
    // 内部懒加载的组件加载成功之前先渲染 fallback 中的内容
    <div>
      <h1>Cinema</h1>
      <Suspense fallback={<div>loading。。。</div>}>
        <Child />
      </Suspense>
    </div>
  )
}

export default Cinema