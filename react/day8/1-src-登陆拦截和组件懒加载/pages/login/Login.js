import React, { useState } from 'react'
import { useHistory, useLocation } from 'react-router-dom'

const query = str => {
  const res = {}
  const arr = str.slice(1).split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    res[key] = decodeURIComponent(val)
  })
  return res
}


const Login = () => {

  const [form, setForm] = useState({ name: '', pwd: '' })
  const locaiton = useLocation()
  const history = useHistory()

  const submit = () => {
    const token = form.name + form.pwd
    localStorage.setItem('token', token)
    const qs = query(locaiton.search)
    history.push(qs.redirectTo || '/home')
  }

  return (
    <div>
      <p>姓名：<input type="text" placeholder="请输入用户名" value={form.name} onChange={e => setForm({ ...form, name: e.target.value })} /></p>
      <p>密码: <input type="password" placeholder="请输入密码" value={form.pwd} onChange={e => setForm({ ...form, pwd: e.target.value })} /></p>
      <button onClick={submit}>登陆</button>
    </div>
  )
}

export default Login