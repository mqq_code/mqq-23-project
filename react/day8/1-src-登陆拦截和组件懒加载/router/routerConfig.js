import React from 'react'
import Home from '../pages/home/Home'
// import Detail from '../pages/detail/Detail'
import Login from '../pages/login/Login'
import Notfound from '../pages/404'
import Movie from '../pages/movie/Movie'
// import Cinema from '../pages/cinema/Cinema'
// import Mine from '../pages/mine/Mine'
import Hot from '../pages/hot/Hot'
// import Coming from '../pages/coming/Coming'
import withAuth from '../hoc/withAuth'

// 组件懒加载
const Detail = React.lazy(() => import(/* webpackChunkName: 'detail' */'../pages/detail/Detail'))
const Cinema = React.lazy(() => import(/* webpackChunkName: 'cinema' */'../pages/cinema/Cinema'))
const Mine = React.lazy(() => import(/* webpackChunkName: 'mine' */'../pages/mine/Mine'))
const Coming = React.lazy(() => import(/* webpackChunkName: 'coming' */'../pages/coming/Coming'))

const routes = [
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/home/movie',
        component: Movie,
        children: [
          {
            path: '/home/movie/hot',
            component: Hot
          },
          {
            path: '/home/movie/coming',
            component: Coming
          },
          {
            path: '/home/movie',
            to: '/home/movie/hot'
          }
        ]
      },
      {
        path: '/home/cinema',
        component: Cinema,
      },
      {
        path: '/home/mine',
        component: withAuth(Mine) // ==> Auth
      },
      {
        path: '/home',
        to: '/home/movie'
      }
    ]
  },
  {
    path: '/detail/:id/:a',
    component: withAuth(Detail),
    exact: true
  },
  {
    path: '/login',
    component: Login,
    exact: true
  },
  {
    path: '/404',
    component: Notfound
  },
  {
    path: '/',
    to: '/home',
    exact: true
  },
  {
    path: '*',
    to: '/404'
  }
]

export default routes