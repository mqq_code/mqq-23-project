import React from 'react'
import { Redirect, useLocation } from 'react-router-dom'

const withAuth = (Com) => {
  
  const Auth = (props) => {
    const location = useLocation()
    const token = localStorage.getItem('token')
    if (!token) {
      const fullPath = location.pathname + location.search
      const pushParams = {
        pathname: '/login',
        search: `?redirectTo=${encodeURIComponent(fullPath)}`
      }
      return <Redirect to={pushParams} />
    }
    return <Com {...props} />
  }

  return Auth
}

export default withAuth