import Mock from 'mockjs'
import zh from './zh.json'
import xl from './xl.json'
import sx from './sx.json'

Mock.mock('/api/zh', () => {
  return zh
})

Mock.mock('/api/xl', () => {
  return xl
})

Mock.mock('/api/sx', () => {
  return sx
})

const allList = [...zh.items, ...xl.items, ...sx.items]
Mock.mock('/api/detail', 'post', (options) => {
  const body = JSON.parse(options.body)
  return allList.find(item => item.item_id === body.id * 1) || {}
})

Mock.mock('/api/search', 'post', (options) => {
  const body = JSON.parse(options.body)
  const arr = []
  allList.forEach(item => {
    const index = arr.findIndex(v => v.item_id === item.item_id)
    if (index === -1 && item.title.includes(body.keywords)) {
      arr.push(item)
    }
  })
  return arr
})