import React, { useEffect, useState } from 'react'
import axios from 'axios'
const Detail = (props) => {

  const [info, setInfo] = useState({})

  useEffect(() => {
    axios.post('/api/detail', { id: props.match.params.id }).then(res => {
      setInfo(res.data)
    })
  }, [])

  return (
    <div>
      <h2>{info.title}</h2>
      <img src={info.img} alt="" />
    </div>
  )
}

export default Detail