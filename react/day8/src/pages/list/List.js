import React, { useEffect, useState, useContext, useMemo } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import axios from 'axios'
import style from './list.module.scss'
import classNames from 'classnames'
import ctx from '../../context/ctx'

const api = {
  zh: '/api/zh',
  xl: '/api/xl',
  sx: '/api/sx'
}

const List = () => {
  const history = useHistory()
  const params = useParams()
  const [list, setList] = useState([])
  const { isGrid, sortType } = useContext(ctx)

  useEffect(() => {
    const url = api[params.id]
    if (url) {
      axios.get(url).then(res => {
        setList(res.data.items)
      })
    } else {
      history.push('/404')
    }
  }, [params])

  const sortList = useMemo(() => {
    const newlist = [...list]
    if (sortType === 0) return newlist
    return newlist.sort((a, b) => {
      if (sortType === 1) {
        return a.price - b.price
      } else {
        return b.price - a.price
      }
    })
  }, [sortType, list])

  const goDetail = id => {
    console.log(id)
    history.push(`/detail/${id}`)
  }

  return (
    <div className={classNames('page', style.list, { [style.grid]: isGrid })}>
      {sortList.map(item =>
        <dl key={item.item_id} onClick={() => goDetail(item.item_id)}>
          <dt><img src={item.img} alt="" /></dt>
          <dd>
            <h3>{item.title}</h3>
            <p>月销{item.sold}笔</p>
            <p className={style.price}>¥{item.price}</p>
          </dd>
        </dl>
      )}
    </div>
  )
}

export default List