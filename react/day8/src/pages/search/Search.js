import React, { useState } from 'react'
import style from './search.module.scss'
import classNames from 'classnames'
import axios from 'axios'
import { debounce } from 'lodash'

const Search = (props) => {

  const [list, setList] = useState([])

  // 添加防抖
  const startSearch = debounce(async (e) => {
    const res = await axios.post('/api/search', { keywords: e.target.value })
    console.log('接口数据', res.data)
    setList(res.data)
  }, 300)

  const goDetail = id => {
    props.history.push(`/detail/${id}`)
  }


  return (
    <div className={classNames('page', style.search)}>
      <header>
        <input type="text" placeholder="请输入搜索的商品名称" onChange={startSearch} />
      </header>
      <div className={style.list}>
        <ul>
          {list.map(item =>
            <li key={item.item_id} onClick={() => goDetail(item.item_id)}>{item.title}</li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Search