import Notfound from '../pages/404'
import Home from '../pages/home/Home'
import Detail from '../pages/detail/Detail'
import Search from '../pages/search/Search'
import List from '../pages/list/List'

const routes = [
  {
    path: '/home',
    component: Home,
    children: [
      {
        exact: true,
        path: '/home/:id',
        component: List
      },
      {
        exact: true,
        path: '/home',
        to: '/home/zh'
      },
      {
        path: '*',
        to: '/404'
      }
    ]
  },
  {
    exact: true,
    path: '/detail/:id',
    component: Detail
  },
  {
    exact: true,
    path: '/search',
    component: Search
  },
  {
    exact: true,
    path: '/404',
    component: Notfound
  },
  {
    exact: true,
    path: '/',
    to: '/home'
  },
  {
    path: '*',
    to: '/404'
  }
]

export default routes