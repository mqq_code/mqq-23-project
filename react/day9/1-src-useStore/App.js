import React from 'react'
import './App.scss'
import RouterView from './router/RouterView'
import routes from './router/routerConfig'

const App = () => {
  return (
    <RouterView routes={routes} />
  )
}

export default App
