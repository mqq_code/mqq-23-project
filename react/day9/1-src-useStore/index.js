import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App'
import {
  // 路由根组件，跟路由相关的所有内容必须包含在根组件内
  BrowserRouter, // history模式，地址栏没#
  HashRouter // hash模式，地址栏中有#
} from 'react-router-dom'
import { Provider } from './store'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <HashRouter>
    <Provider a="10">
      <App />
    </Provider>
  </HashRouter>
);
