import React from 'react'
import { useStore } from '../../store'

const Home = (props) => {
  const { state, dispatch } = useStore()

  console.log(state)

  const remove = id => {
    dispatch({
      type: 'remove_list_item',
      payload: id
    })
  }

  return (
    <div>
      <h1>{state.title}</h1>
      <button onClick={() => props.history.push('/search')}>新增</button>
      <ul>
        {state.list.map(item =>
          <li key={item.id}>{item.name} <button onClick={() => remove(item.id)}>删除</button></li>
        )}
      </ul>
      {state.list.length === 0 && <div>暂时没有数据</div>}
    </div>
  )
}

export default Home