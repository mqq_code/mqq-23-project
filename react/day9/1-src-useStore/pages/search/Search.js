import React, { useState } from 'react'
import { useStore } from '../../store'

const Search = (props) => {
  // 从 store 中获取数据
  const { state, dispatch } = useStore()
  const [value, setVal] = useState('')

  const submit = () => {
    // 通知store更新数据
    dispatch({
      type: 'create_list_item',
      payload: value
    })
    props.history.goBack()
  }
  return (
    <div>
      <input type="text" value={value} onChange={e => setVal(e.target.value)} />
      <button onClick={submit}>添加</button>
      <div>
        <input type="text" value={state.title} onChange={e => {
          dispatch({
            type: 'change_title',
            payload: e.target.value
          })
        }} />
      </div>
      <div>{JSON.stringify(state)}</div>
    </div>
  )
}

export default Search