import React from 'react'
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom'

const RouterView = (props) => {
  const routes = props.routes.filter(item => item.component)
  const redirects = props.routes.filter(item => !item.component)
  return (
    <Switch>
      {routes.map(item => {
        return <Route
          key={item.path}
          exact={item.exact}
          path={item.path}
          render={routeInfo => {
            const Com = item.component
            return <Com {...routeInfo}>
              {item.children && <RouterView routes={item.children} />}
            </Com>
          }}
        />
      })}
      {redirects.map(item => (
        <Redirect key={item.path} exact={item.exact} from={item.path} to={item.to} />
      ))}
    </Switch>
  )
}

export default RouterView