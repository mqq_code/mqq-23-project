import React, { createContext, useReducer, useContext } from 'react'

// 默认值
const initState = {
  list: [],
  title: '默认标题'
}

// 修改数据的函数
const reducer = (state, action) => {
  // 调用dispatch会执行此函数
  if (action.type === 'create_list_item') {
    return {
      ...state,
      list: [
        ...state.list,
        { id: Date.now(), name: action.payload }
      ]
    }
  } else if (action.type === 'remove_list_item') {
    return { ...state, list: state.list.filter(v => v.id !== action.payload) }
  } else if (action.type === 'change_title') {
    return { ...state, title: action.payload }
  }
  return state
}

// 创建context
export const storeCtx = createContext()

export const Provider = (props) => {
  const [state, dispatch] = useReducer(reducer, initState)
  return (
    // 把数据传给所有的后代组件
    <storeCtx.Provider value={{state, dispatch}}>
      {props.children}
    </storeCtx.Provider>
  )
}

// 给组件提供获取仓库数据的hook
export const useStore = () => {
  return useContext(storeCtx)
}







