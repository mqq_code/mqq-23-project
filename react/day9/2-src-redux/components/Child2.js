import React, { Component } from 'react'
import store from '../store'

class Child2 extends Component {

  state = {
    num: store.getState().num
  }

  add = n => {
    store.dispatch({
      type: 'ADD_NUM',
      payload: n
    })
  }

  componentDidMount() {
    this.unsubscribe =  store.subscribe(() => {
      console.log('child2 监听到store改变了', store.getState())
      this.setState({
        num: store.getState().num
      })
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  render() {
    return (
      <div className="box">
        <h2>Child2</h2>
        <p>
          <button onClick={() => this.add(-1)}>-1</button>
          <b style={{ margin: '0 20px' }}>{this.state.num}</b>
          <button onClick={() => this.add(2)}>+2</button>
        </p>
      </div>
    )
  }
}

export default Child2
