import React, { Component } from 'react'
import store from '../store'

class Create extends Component {

  state = {
    num: store.getState().num
  }

  add = n => {
    store.dispatch({
      type: 'ADD_NUM',
      payload: n
    })
  }

  componentDidMount() {
    this.unsubscribe =  store.subscribe(() => {
      console.log('child2 监听到store改变了', store.getState())
      this.setState({
        num: store.getState().num
      })
    })
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  render() {
    return (
      <div>
        <h1>新增页面</h1>
        <p>
          <button onClick={() => this.add(-1)}>-1</button>
          <b style={{ margin: '0 20px' }}>{this.state.num}</b>
          <button onClick={() => this.add(2)}>+2</button>
        </p>
      </div>
    )
  }
}

export default Create
