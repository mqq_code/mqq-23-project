import React, { useEffect, useState } from 'react'
import Child1 from '../components/Child1'
import store from '../store'

const Home = () => {

  const [title, setTitle] = useState(store.getState().title)

  useEffect(() => {
    const unsubscribe = store.subscribe(() => {
      // 监听store中的数据变化，执行此函数
      console.log('home监听到store数据改变了', store.getState())
      // 更新页面
      setTitle(store.getState().title)
    })
    return () => {
      // 组件销毁取消监听
      unsubscribe()
    }
  }, [])

  console.log('react Home组件', title)

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => {
        // 调用 dispatch 发送 action 执行reducer函数
        // action：描述本次修改内容的对象 { type: '' }
        store.dispatch({
          type: 'SET_TITLE',
          payload: e.target.value
        })
      }} />
      <Child1 />
    </div>
  )
}

export default Home