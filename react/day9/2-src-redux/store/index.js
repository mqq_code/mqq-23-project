import { legacy_createStore } from 'redux'

// 初始值
const initState = {
  title: 'redux 标题',
  num: 0,
  list: []
}

// 返回state
const reducer = (state = initState, action) => {
  // console.log('reducer的state', state)
  // console.log('reducer的action', action) // dispatch 传过来的对象
  if (action.type === 'SET_TITLE') {
    return { ...state, title: action.payload }
  } else if (action.type === 'ADD_NUM') {
    return { ...state, num: state.num + action.payload }
  }
  return state
}



// 创建store
const store = legacy_createStore(reducer)

export default store

