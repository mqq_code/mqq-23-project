import React, { Component } from 'react'
import { connect } from 'react-redux'

class Child2 extends Component {


  add = n => {
    this.props.dispatch({
      type: 'ADD_NUM',
      payload: n
    })
  }

  render() {
    return (
      <div className="box">
        <h2>Child2</h2>
        {this.props.title}
        <p>
          <button onClick={() => this.add(-1)}>-1</button>
          <b style={{ margin: '0 20px' }}>{this.props.num}</b>
          <button onClick={() => this.add(2)}>+2</button>
        </p>
      </div>
    )
  }
}
const mapState = state => {
  return {
    num: state.num,
    title: state.title
  }
}
export default connect(mapState)(Child2)
