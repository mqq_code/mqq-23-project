import React, { Component } from 'react'
import { connect } from 'react-redux'

class Create extends Component {

  state = {
  }

  add = n => {
    this.props.dispatch({
      type: 'ADD_NUM',
      payload: n
    })
  }

  render() {
    console.log(this.props)
    return (
      <div>
        <h1>新增页面</h1>
        <p>
          <button onClick={() => this.add(-1)}>-1</button>
          <b style={{ margin: '0 20px' }}>{this.props.num}</b>
          <button onClick={() => this.add(2)}>+2</button>
        </p>
      </div>
    )
  }
}

// connect: 高阶组件，连接组件和store，把store中的数据通过props传给组件
const mapState = state => {
  // console.log(state) // store中的所有数据
  // 把return的数据合并到组件的props中
  return {
    num: state.num,
    a: 100
  }
}
export default connect(mapState)(Create)

