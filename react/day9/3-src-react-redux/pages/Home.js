import React, { useEffect, useState } from 'react'
import Child1 from '../components/Child1'
import { useDispatch, useSelector, useStore } from 'react-redux'

const Home = () => {
  // const store = useStore() // 获取store
  const dispatch = useDispatch() // 获取dispatch函数
  const title = useSelector(state => state.title) // 获取store中的数据
  const num = useSelector(state => state.num) // 获取store中的数据
  
  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => {
        dispatch({
          type: 'SET_TITLE',
          payload: e.target.value
        })
      }} />
      {num}
      <Child1 />
    </div>
  )
}

export default Home