"use strict";
{
    // 枚举: 使用方便理解的单词来描述特定的数字或者字符串，提高代码可读性，方便理解和后期维护
    let Sex;
    (function (Sex) {
        Sex[Sex["women"] = 1] = "women";
        Sex[Sex["men"] = 3] = "men";
        Sex[Sex["other"] = 2] = "other";
    })(Sex || (Sex = {}));
    // 例如后端返回sex字段 0 1 2
    const res = {
        sex: 0
    };
    if (res.sex === Sex.men) {
        console.log('男');
    }
    else if (res.sex === Sex.women) {
        console.log('女');
    }
    let Direction;
    (function (Direction) {
        Direction[Direction["left"] = 65] = "left";
        Direction[Direction["up"] = 87] = "up";
        Direction[Direction["right"] = 68] = "right";
        Direction[Direction["down"] = 83] = "down";
    })(Direction || (Direction = {}));
    const box = document.querySelector('.box');
    document.addEventListener('keydown', e => {
        if (e.keyCode == Direction.left) {
            box.style.left = box.offsetLeft - 10 + 'px';
        }
        else if (e.keyCode == Direction.up) {
            box.style.top = box.offsetTop - 10 + 'px';
        }
        else if (e.keyCode == Direction.right) {
            box.style.left = box.offsetLeft + 10 + 'px';
        }
        else if (e.keyCode == Direction.down) {
            box.style.top = box.offsetTop + 10 + 'px';
        }
    });
    /*
  
    type IArr = number | IArr[]
  
    let arr: IArr[] = [[1,[2,[3,4],5],6],7,8,9]
  
  
    const flat = (array: IArr[], n: number): IArr[] => {
      // let res: number[]  = []
      // array.forEach(item => {
      //   if (Array.isArray(item)) {
      //     res = res.concat(flat(item))
      //   } else {
      //     res.push(item)
      //   }
      // })
      // return res
      if (n < 1) return array
  
      return array.reduce((prev: IArr[], item) => {
        return Array.isArray(item) ? prev.concat(flat(item, n - 1)) : prev.concat([item])
      }, [] as IArr[])
    }
  
  
    console.log(flat(arr, 2))
  
    */
}
