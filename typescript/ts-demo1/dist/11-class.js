"use strict";
{
    // class
    class Person {
        constructor(name) {
            this.sex = '男'; // 私有属性：只能在定义的class内访问
            // 构造函数：实例化时立即执行
            this.name = name;
        }
        say() {
            console.log(`我的名字叫${this.name}，性别:${this.sex}`);
        }
    }
    let xm = new Person('小明');
    // 继承
    class Doctor extends Person {
        constructor(name, age) {
            super(name); // 父类的构造函数
            this.age = age;
        }
        getSex() {
            // console.log(this.sex)
            console.log(this.name);
        }
    }
    const xg = new Doctor('小刚', 30);
    // console.log(xg.name)
    // 存取器
    class Employee {
        get fullName() {
            console.log('有人读取来 fullName');
            return 'abc';
        }
        set fullName(newName) {
            console.log('有人想修改 fullName', newName);
        }
    }
    // const a = new Employee()
    // console.log(a.fullName)
    // setTimeout(() => {
    //   a.fullName = '123'
    // }, 2000)
    class Animal {
        constructor(name) {
            // this指向实例化对象
            this.name = name;
        }
        static getAbc() {
            // 静态方法中的this指向class本身
            console.log(this.abc);
        }
    }
    // 静态方法和静态属性
    Animal.abc = 'ABC';
    const dog = new Animal('老狗');
    // 抽象类：不可以实例化，用来约束子类的实现
    class Base {
        constructor() {
            this.age = 20;
        }
        getName() {
            console.log(this.name);
        }
    }
    // class 使用接口通过 implements 关键字
    class Child extends Base {
        constructor(name) {
            super();
            this.name = name;
        }
        say(text) {
            console.log(this.name);
        }
    }
    let c = new Child('cccc');
    console.log('c', c);
    c.getName();
}
