"use strict";
{
    // 泛型：定义类型时不确定具体类型，使用时才能确定具体类型，可以使用泛型
    // 定义泛型函数
    function createArr(...rest) {
        return rest.concat(rest);
    }
    const arr0 = createArr(true, false);
    const arr = createArr('a', 'b', 'c');
    createArr(1, 2, 3, 4, 5).forEach(item => {
        item.toFixed;
    });
    const arr2 = [['a', ['b']], 'c'];
    const arr3 = [[1, 2, 3, 4], 5, 6, 7];
    const arr4 = [1, 2, 3, 4];
    function flat(array) {
        return array.reduce((prev, item) => {
            return Array.isArray(item) ? prev.concat(flat(item)) : prev.concat([item]);
        }, []);
    }
    console.log(flat(arr2));
    const xm = {
        name: '小明',
        age: 20,
        sex: '1'
    };
    const xm1 = {
        name: '小明',
        age: '30',
        sex: '女'
    };
    // 泛型类
    class List {
        constructor() {
            this.list = [];
        }
        push(val) {
            this.list.push(val);
        }
        pop() {
            this.list.pop();
        }
        unshift(val) {
            this.list.unshift(val);
        }
    }
    const list1 = new List();
    list1.push('a');
    list1.push('100');
    const list2 = new List();
    list2.push(1);
    const list3 = new List();
    list3.push({ name: 'aa', age: 30 });
    console.log(list3);
    // T 必须满足 extends 后的类型条件
    function getLen(option) {
        return option;
    }
    console.log(getLen({ name: 'aaa', a: 10, b: '', length: 10 }));
    console.log(getLen('abcdefg'));
    console.log(getLen([1, 2, 3, 4, 5]));
    function fn1(a) {
    }
    fn1(10);
}
