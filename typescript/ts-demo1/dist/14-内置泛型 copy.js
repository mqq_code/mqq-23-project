"use strict";
{
    function fn(a) {
        if (typeof a === 'string') {
            return a.length;
        }
        else {
            return a.toFixed(2);
        }
    }
    let a = fn(100);
    function sum(...rest) {
        return rest.reduce((prev, val) => prev + val, 0);
    }
    const s1 = Symbol('abc');
    const s2 = Symbol('abc');
    console.log(s1 === s2);
}
