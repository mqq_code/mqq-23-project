"use strict";
{
    // 类型断言、可选链、非空断言
    // 类型断言：当开发者比ts更确定此变量类型时可以使用类型断言
    // const title = document.querySelector('h2') as HTMLHeadingElement
    // const title = <HTMLHeadingElement>document.querySelector('h2')
    // const title = document.querySelector('h2')
    // // 判断元素存在再往后执行
    // title && (title.style.color = 'red')
    // // 可选链
    // title?.addEventListener('click', () => {
    //   console.log('点击h2')
    // })
    // 非空断言
    // const title = document.querySelector('h2')!
    // title.addEventListener('click', () => {
    //   console.log('点击h2')
    // })
    const btn = document.querySelector('.btn');
    const inp = document.querySelector('.inp');
    const title = document.querySelector('.title');
    btn === null || btn === void 0 ? void 0 : btn.addEventListener('click', (e) => {
        title.innerHTML = inp.value;
    });
    inp.addEventListener('keydown', (e) => {
        if (e.keyCode === 13) {
            title.innerHTML = inp.value;
        }
        // console.log((e.target as HTMLInputElement).value)
    });
}
