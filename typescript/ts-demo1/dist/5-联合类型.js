"use strict";
{
    // 联合类型、文字类型
    // 联合类型
    function fn(option) {
        // 联合类型默认只能访问该类型中共有的属性和方法
        if (typeof option === 'string') {
            // 类型保护: 确定此判断条件中 option 是字符串类型
            console.log(option.toUpperCase());
        }
        else {
            console.log(option);
        }
    }
    console.log(fn('abc'));
    // 文字类型
    function tip(type = 'default') {
        if (type === 'success') {
            alert('绿色');
        }
        else if (type === 'error') {
            alert('红色');
        }
        else {
            alert('默认');
        }
    }
    tip('error');
}
