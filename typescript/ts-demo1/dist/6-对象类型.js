"use strict";
{
    let obj = {
        name: '小明',
        age: 20,
        sex: 1
    };
    let obj1 = {
        name: '小刚',
        age: 30,
        sex: 1,
        hobby: ['唱歌']
    };
    const arr = [];
    function push(option) {
        arr.push(option);
    }
    push(obj);
    push(obj1);
    function getInfo(user) {
        return `我的姓${user.firstName}, 我叫${user.lastName}`;
    }
    let res = getInfo({ firstName: '朱', lastName: '葛亮' });
    console.log(res);
    let list = [
        {
            path: '/home',
            name: 'home',
            children: [
                {
                    path: '/home/movie',
                    name: 'movie',
                    children: [
                        {
                            path: '/home/movie/hot',
                            name: 'hot'
                        }
                    ]
                },
                {
                    path: '/home/cinema',
                    name: 'cinema'
                },
                {
                    path: '/home/mine',
                    name: 'mine'
                }
            ]
        },
        {
            path: '/detail',
            name: 'detail',
            children: [
                {
                    path: '/detail/1',
                    name: 'detail1'
                }
            ]
        },
        {
            path: '/login',
            name: 'login'
        }
    ];
}
