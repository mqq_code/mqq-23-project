"use strict";
{
    // typeof、keyof
    const a = 'aaaa';
    // let b: typeof a = 'aaaa'
    // let b: IA = 'aaaa'
    const obj = {
        name: '小明',
        age: 20
    };
    let obj1 = {
        name: '',
        age: 10
    };
    let c = 'name';
    function getVal(obj, k) {
        return obj[k];
    }
    getVal({ a: 1, b: 2 }, 'a');
    let res = getVal({ c: [], d: true }, 'd');
    let obj3 = {
        a: true,
        b: 'aaaaaa',
        c: 10
    };
    const aa = 'c';
}
