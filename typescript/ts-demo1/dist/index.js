"use strict";
// import abc, { a, b, c, d } from './utils/index'
// console.log(abc) // 100
// import * as API from './utils/index'
// API ==> { default: 1, a: 100, b: 200, c: 300, d: 400}
{
    const s1 = Symbol('name');
    const s2 = Symbol('name');
    const obj = {
        [s1]: '小明',
        [s2]: '小刚'
    };
    console.log(obj);
}
