/*
  ts 类型:
    string
    number
    boolean
    null
    undefined
    symbol
    void
    any
    数组
    元组
    enum
    unknown
    never
    object
*/



{




// 定义变量时添加类型注解
let str: string = 'aaaaa'
let num: number = 10
let bool: boolean = false
let und: undefined = undefined
let nu: null = null
let sy: symbol = Symbol('a')

// object: 表示非基础类型
let o: object = {}
// Object: 值不可以是 null 和 undefined
let o1: Object = ''

// 数组
let arr: number[] = [1, 2, 3, 4, 5]
let arr1: string[] = ['a', 'b', 'd']
// 泛型方式定义数组
let arr2: Array<number> = [1, 2, 3, 4]

// 元组: 已知元素数量和类型的数组
let arr3: [number, string] = [100, '歌词']


// any: 任意类型，相当于放弃了类型校验
let aa: any = 10
aa = 'aaa'
aa = false

// void: 没有值，一般当函数没有返回值时使用
function log(text: string): void  {
  console.log(text)
}


// unknown 暂时不确定类型，但是又不希望放弃校验
let una: unknown
setTimeout(() => {
  if (Math.random() > 0.5) {
    una = 'aaaaaa'
    // 使用类型断言给 unknown 确定类型
    console.log((una as string).length)
  } else {
    una = 1000
    console.log((una as number).toFixed(2))
  }
}, 1000)


// never 永不存在的值的类型
// 一个变量不可能既是 string 又是 number，所以 a 的类型就是 never
let a: string & number
// 返回never的函数必须存在无法达到的终点
function error(message: string): never {
  throw new Error(message);
}

// 推断的返回值类型为never
function fail() {
  return error("Something failed");
}

// 返回never的函数必须存在无法达到的终点
function infiniteLoop(): never {
  while (true) {
  }
}














}


