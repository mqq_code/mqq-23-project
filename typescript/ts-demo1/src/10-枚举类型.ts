{

  // 枚举: 使用方便理解的单词来描述特定的数字或者字符串，提高代码可读性，方便理解和后期维护
  enum Sex {
    women = 1,
    men = 3,
    other = 2
  }


  // 例如后端返回sex字段 0 1 2
  const res = {
    sex: 0
  }


  if (res.sex === Sex.men) {
    console.log('男')

  } else if (res.sex === Sex.women) {
    console.log('女')
  } 



  enum Direction {
    left = 65,
    up = 87,
    right = 68,
    down = 83
  }

  const box = document.querySelector('.box') as HTMLDivElement

  document.addEventListener('keydown', e => {
    if (e.keyCode == Direction.left) {
      box.style.left = box.offsetLeft - 10 + 'px'
    } else if (e.keyCode == Direction.up) {
      box.style.top = box.offsetTop - 10 + 'px'
    } else if (e.keyCode == Direction.right) {
      box.style.left = box.offsetLeft + 10 + 'px'
    } else if (e.keyCode == Direction.down) {
      box.style.top = box.offsetTop + 10 + 'px'
    }
  })













  /*

  type IArr = number | IArr[]

  let arr: IArr[] = [[1,[2,[3,4],5],6],7,8,9]


  const flat = (array: IArr[], n: number): IArr[] => {
    // let res: number[]  = []
    // array.forEach(item => {
    //   if (Array.isArray(item)) {
    //     res = res.concat(flat(item))
    //   } else {
    //     res.push(item)
    //   }
    // })
    // return res
    if (n < 1) return array

    return array.reduce((prev: IArr[], item) => {
      return Array.isArray(item) ? prev.concat(flat(item, n - 1)) : prev.concat([item])
    }, [] as IArr[])
  }


  console.log(flat(arr, 2))

  */



}

