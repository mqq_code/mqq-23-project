{
// class
class Person {
  protected name: string // 受保护的属性: 只能在 class 和其子类中访问
  private sex: string = '男' // 私有属性：只能在定义的class内访问
  constructor(name: string) {
    // 构造函数：实例化时立即执行
    this.name = name
  }
  say() {
    console.log(`我的名字叫${this.name}，性别:${this.sex}`)
  }
}

let xm = new Person('小明')


// 继承
class Doctor extends Person {
  public readonly age: number // 公共属性：class内、子类、实例化对象都可以访问
  constructor(name: string, age: number) {
    super(name) // 父类的构造函数
    this.age = age
  }
  getSex() {
    // console.log(this.sex)
    console.log(this.name)
  }
}
const xg = new Doctor('小刚', 30)
// console.log(xg.name)



// 存取器
class Employee {

  get fullName(): string {
    console.log('有人读取来 fullName')
    return 'abc'
  }

  set fullName(newName: string) {
    console.log('有人想修改 fullName', newName)
  }
}

// const a = new Employee()
// console.log(a.fullName)

// setTimeout(() => {
//   a.fullName = '123'
// }, 2000)



class Animal {
  name: string // 实例属性
  constructor(name: string) {
    // this指向实例化对象
    this.name = name
  }

  // 静态方法和静态属性
  static abc: string = 'ABC'
  static getAbc() {
    // 静态方法中的this指向class本身
    console.log(this.abc)
  }
}

const dog = new Animal('老狗')
// console.log(dog.name) // 调用实例属性

// console.log(Animal.abc) // 调用静态属性
// Animal.getAbc() // 调用静态方法


interface IChild {
  name: string;
  age: number;
}


// 抽象类：不可以实例化，用来约束子类的实现
abstract class Base {
  abstract name: string // 定义抽象属性
  abstract say(text: string): void // 定义抽象方法
  age: number = 20
  getName() { // 可以定义具体方法
    console.log(this.name)
  }
}

// class 使用接口通过 implements 关键字
class Child extends Base implements IChild {
  name: string
  constructor(name: string) {
    super()
    this.name = name
  }
  say(text: string) {
    console.log(this.name)
  }
}
let c = new Child('cccc')
console.log('c', c)
c.getName()





}

