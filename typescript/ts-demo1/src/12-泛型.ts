{

  // 泛型：定义类型时不确定具体类型，使用时才能确定具体类型，可以使用泛型
  // 泛型函数、泛型别名、泛型接口、泛型类、泛型约束


  // 定义泛型函数
  function createArr<T>(...rest: T[]): T[] {
    return rest.concat(rest)
  }

  const arr0 = createArr(true, false)


  const arr = createArr<string>('a', 'b', 'c')
  createArr<number>(1,2,3,4,5).forEach(item => {
    item.toFixed
  })

  // 定义泛型别名
  type deepArr<T> = T | deepArr<T>[]

  const arr2: deepArr<string>[] = [['a', ['b']], 'c']
  const arr3: deepArr<number>[] = [[1, 2, 3, 4], 5, 6, 7]
  const arr4: Array<number> = [1, 2, 3, 4]

  function flat<T>(array: deepArr<T>[]): T[] {
    return array.reduce((prev: T[], item) => {
      return Array.isArray(item) ? prev.concat(flat(item)) : prev.concat([item]) 
    }, [] as T[])
  }

  console.log(flat<string>(arr2))


  // 泛型接口
  interface IObj<T, U> {
    name: string;
    age: T;
    sex: U;
  }

  const xm: IObj<number, '0' | '1'> = {
    name: '小明',
    age: 20,
    sex: '1'
  }

  const xm1: IObj<string, '男' | '女'> = {
    name: '小明',
    age: '30',
    sex: '女'
  }


  // 泛型类
  class List<T> {
    list: T[] = []
    push (val: T) {
      this.list.push(val)
    }
    pop () {
      this.list.pop()
    }
    unshift (val: T) {
      this.list.unshift(val)
    }
  }

  const list1 = new List<string>()
  list1.push('a')
  list1.push('100')


  const list2 = new List<number>()
  list2.push(1)


  const list3 = new List<{ name: string; age: number }>()
  list3.push({ name: 'aa', age: 30 })
  console.log(list3)



  // 泛型约束
  type Len = { length: number }
  // T 必须满足 extends 后的类型条件
  function getLen<T extends Len>(option: T) {
    return option
  }
  
  console.log(getLen({ name: 'aaa', a: 10, b: '', length: 10 }))
  console.log(getLen('abcdefg'))
  console.log(getLen([1,2,3,4,5]))

  function fn1<T extends string | number>(a: T) {
  }
  fn1(10)

  


}

