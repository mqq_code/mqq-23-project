{

  // typeof、keyof

  const a = 'aaaa'

  // 类型中的typeof，获取变量的类型
  type IA = typeof a
  // let b: typeof a = 'aaaa'
  // let b: IA = 'aaaa'

  const obj = {
    name: '小明',
    age: 20
  }
  type IObj = typeof obj
  let obj1: IObj = {
    name: '',
    age: 10
  }

  type Iname = typeof obj.name

  // keyof
  interface IPerson {
    name: string;
    age: number;
    sex: '男' | '女';
    hobby: string[];
  }
  // keyof: 返回该类型的所有的 key 组成的文字联合类型
  type PersonKeys = keyof IPerson // 'name' | 'age' | 'sex' | 'hobby'

  let c: PersonKeys = 'name'


  function getVal<T, K extends keyof T>(obj: T, k: K): T[K] {
    return obj[k]
  }

  getVal({ a: 1, b: 2 }, 'a')

  let res = getVal({ c: [], d: true }, 'd')




  let obj3 = {
    a: true,
    b: 'aaaaaa',
    c: 10
  }

  type IObj3 = keyof typeof obj3

  const aa: IObj3 = 'c'






}

