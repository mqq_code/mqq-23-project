{

  // 条件类型
  type ITest<T extends string | number> = T extends string ? number : string

  function fn<T extends string | number>(a: T): ITest<T> {
    if (typeof a === 'string') {
      return a.length as ITest<T> 
    } else {
      return a.toFixed(2) as ITest<T>
    }
  }

  let a = fn<number>(100)


  // ts 中常用的内置泛型
  // Partial
  // Required
  // Readonly
  // Pick
  // Omit
  // Record
  // Exclude
  // Extract
  // ReturnType

  interface IObj {
    name: string;
    age: number;
    sex: '男' | '女';
    hobby?: string[];
  }

  type PartialObj = Partial<IObj> // 把参数的所有属性改成可选属性
  type RequiredObj = Required<IObj> // 把参数的所有属性改成必填属性
  type ReadonlyObj = Readonly<IObj> // 把参数的所有属性改成只读属性
  type PickObj = Pick<IObj, 'name' | 'age'> // 获取对象类型中的指定属性
  type OmitObj = Omit<IObj, 'name' | 'age'> // 排除对象类型中的指定属性
  // type RecordObj = Record<'name' | 'age', string> // 创建对象类型，第一个参数是所有的key，第二个参数是所有key的类型
  type RecordObj = Record<keyof IObj, string>
  type ExcludeKey = Exclude<'a' | 'b' | 'c', 'c' | 'd'> // Exclude<T, U> -- 从T中剔除可以赋值给U的类型。
  type ExtractKey = Extract<'a' | 'b' | 'c', 'c' | 'd'> // Extract<T, U> 提取T中可以赋值给U的类型。
  type NonNullableKey = NonNullable<string | number | null> // 去除 null 和 undefined


  function sum(...rest: number[]) {
    return rest.reduce((prev, val) => prev + val, 0)
  }
  // 获取函数返回值类型
  type IRes = ReturnType<typeof sum>

  type IFn = () => string
  type IRes1 = ReturnType<IFn>
  


}

