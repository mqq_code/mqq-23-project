
{

// 定义函数
function random(n: number): number {
  return Math.floor(Math.random() * n)
}

// 函数完整类型
const addZero: (n: number) => string = function (n: number): string {
  return n > 10 ? n + '' : '0' + n
}
// 箭头函数定义类型
const addZero1: (n: number) => string = (n: number): string => n > 10 ? n + '' : '0' + n

// 对象中函数类型
let obj = {
  say(text: string): void {
    console.log(text);
  }
}


// 剩余参数
const sum = (...rest: number[]): number => {
  return rest.reduce((prev, val) => prev + val)
}
console.log(sum(1, 2, 3, 4, 5, 6, 7))

// 可选参数
function fn1(a: number, b?: number) {
  // b: number | undefined
  console.log(a, b)
}
fn1(1)

// 默认参数
function fn2(a: number, b: number = 10) {
  console.log(a, b)
}
fn2(1)



// 函数重载: 定义函数的多个类型，根据参数自动匹配相对应的类型
function fn3 (a: string): number
function fn3 (a: number, n: number): string
function fn3 (a: any, n?: any) {
  if (typeof a === 'number') {
    return a.toFixed(n)
  } else {
    return a.length
  }
}

let a = fn3(100, 2)
let b = fn3('abcdefg')


// function fn4 (a: number | string, n?: number): number | string {
//   if (typeof a === 'number') {
//     return a.toFixed(n)
//   } else {
//     return a.length
//   }
// }
// let c = fn4(100, 2)





}