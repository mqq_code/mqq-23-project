{

// 类型推论: ts 会根据结果和已有的类型推论出变量的类型
let num: number = 100
let num1 = 100

function sum (a: number, b: number): number {
  return a + b
}

let a = sum(1, 2)

function fn3 (a: string): number
function fn3 (a: number, n: number): string
function fn3 (a: any, n?: any) {
  if (typeof a === 'number') {
    return a.toFixed(n)
  } else {
    return a.length
  }
}

// 根据函数的参数类型推论出对应的结果类型
let b1 = fn3(100, 2)
let b2 = fn3('abcdefg')









}