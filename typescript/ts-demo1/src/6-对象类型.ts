{

// 定义类型别名
type Iobj = {
  name: string;
  readonly age: number; // 只读属性
  sex: 1 | 0; // 联合类型、文字类型
  hobby?: string[]; // 可选属性
}
type Ia = string | number | boolean


let obj: Iobj = {
  name: '小明',
  age: 20,
  sex: 1
}


let obj1: Iobj = {
  name: '小刚',
  age: 30,
  sex: 1,
  hobby: ['唱歌']
}

const arr: Iobj[] = []
function push(option: Iobj) {
  arr.push(option)
}
push(obj)
push(obj1)



function getInfo (user: { firstName: string; lastName: string }) {
  return `我的姓${user.firstName}, 我叫${user.lastName}`
}

let res = getInfo({ firstName: '朱', lastName: '葛亮' })
console.log(res)


// 定义树形结构
type IRoute = {
  path: string;
  name: string;
  children?: IRoute[]
}

let list: IRoute[] = [
  {
    path: '/home',
    name: 'home',
    children: [
      {
        path: '/home/movie',
        name: 'movie',
        children: [
          {
            path: '/home/movie/hot',
            name: 'hot'
          }
        ]
      },
      {
        path: '/home/cinema',
        name: 'cinema'
      },
      {
        path: '/home/mine',
        name: 'mine'
      }
    ]
  },
  {
    path: '/detail',
    name: 'detail',
    children: [
      {
        path: '/detail/1',
        name: 'detail1'
      }
    ]
  },
  {
    path: '/login',
    name: 'login'
  }
]














}