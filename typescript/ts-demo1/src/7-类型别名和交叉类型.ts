{

// 类型别名、交叉类型

// 类型别名
type Iobj = {
  name: string;
  readonly age: number; // 只读属性
  sex: 1 | 0; // 联合类型、文字类型
  hobby?: string[]; // 可选属性
}
type Ia = string | number | boolean

const obj: Iobj = {
  name: 'zz',
  age: 100,
  sex: 1,
  hobby: ['rap']
}

// 交叉类型
type Iobj1 = {
  name: string;
  age: number;
}

type IDoctor = {
  job: string;
  age: number;
}
// 交叉类型: 把多个类型进行合并
type IDoctorUser = IDoctor & Iobj1 & { sex: '男' | '女' }

let xm: Iobj1 = {
  name: '小明',
  age: 20
}
let doc: IDoctor = {
  job: '普通外科',
  age: 30
}

let xg: IDoctorUser = {
  name: '小刚',
  age: 40,
  job: '护士',
  sex: '男'
}










}