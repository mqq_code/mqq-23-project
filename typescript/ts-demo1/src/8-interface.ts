{

  // interface 定义接口
  interface Iobj {
    readonly name: string; // 只读属性
    age?: number; // 可选属性
    // say(text: number): void; // 函数类型
    say: (text: number) => void; // 函数类型
    [k: string]: any; // 其他参数类型
  }

  const xm: Iobj = {
    name: '小明',
    age: 20,
    say(text: number): void {
      console.log(this.name, text)
    }
  }


  // 接口定义函数类型过
  interface ISum {
    (a: number, b: number): number
  }
  type ISum1 = (a: number, b: number) => number

  let sum: ISum1  = (a: number, b: number): number => {
    return a + b
  }


  // 定义可索引类型
  interface StringArray {
    [i: number]: string
  }

  let arr: StringArray = ['a', 'b', 'c']



  // interface 继承
  interface Base {
    readonly name: string; // 只读属性
    age?: number; // 可选属性
  }

  // 使用 extends 继承接口
  interface IPerson extends Base {
    sex: '男' | '女'
  }

  // 接口重名会进行合并
  interface IPerson {
    hobby: string[];
  }

  let xm1: IPerson = {
    name: '小明',
    sex: '女',
    hobby: ['aa']
  }




}