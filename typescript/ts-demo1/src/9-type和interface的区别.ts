{

  /*

    interface 和 type 的区别
    1. interface 是定义类型，type 给已有的类型定义别名
    2. interface 使用 extends 继承进行类型扩展，type 使用交叉类型扩展
    3. type 不可以重复定义，interface 重名会合并类型
    4. interface 可以扩展全局类型
  
  */

    
    // type 定义的别名不可以重名
    // type ITest = {
    //   name: string;
    // }
    // type ITest = {
    //   age: number
    // 



}


interface Window {
  a: number;
}

window.a = 100
  