## typescript
1. typescript 是 javascript 的超集
2. 包含 js 的所有语法，在 js 的基础扩展部分新语法
3. 最终编译成 js 在浏览器中运行

### ts 优势
1. TypeScript 是拥有类型语法的 JavaScript
2. 提供强静态类型，编译时的强类型语言，在编译时检查因类型发生的错误，减少运行时错误
3. 更多的语法提示，在一定程度上可以充当文档

### 安装 typscript
1. 全局安装 `npm i -g typescript`
2. 检查安装ts版本 `tsc -v`
3. 初始化ts项目 `tsc --init`
4. tsconfig.json ts编译配置
5. 编译单个 ts 文件 `tsc 文件路径`
6. 监听整个文件夹的 ts 修改，自动编译成 js `tsc --watch`