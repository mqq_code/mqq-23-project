import { createApp } from 'vue'
import { createPinia } from 'pinia'
// 1. 引入你需要的组件
import { Loading } from 'vant'
// 2. 引入组件样式
import 'vant/lib/index.css'
import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(Loading)

app.mount('#app')
