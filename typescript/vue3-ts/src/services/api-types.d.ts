import { QrStatus } from './constant'
// 热搜接口返回值
export interface HotsItem {
  first: string;
  iconType: number;
  second: number;
}
export interface SearchHotRes {
  code: number;
  result: {
    hots: HotsItem[]
  };
}


// 搜索建议口返回值
export interface SuggestItem {
  keyword: string;
}
export interface SearchSuggestRes {
  code: number;
  result: {
    allMatch: SuggestItem[]
  };
}

// 搜索结果返回值
export interface SongsItem {
  name: string;
  id: number;
  ar: { name: string }[];
  al: {
    id: number;
    name: string;
    picUrl: string;
  }
}
export interface SearchResultRes {
  code: number;
  result: {
    songCount: number;
    songs: SongsItem[]
  };
}

// 登陆二维码key
export interface QrKeyRes {
  code: number;
  data: {
    code: number;
    unikey: string;
  };
}
// 生成登陆二维码
export interface CreateQrRes {
  code: number;
  data: {
    qrurl: string;
    qrimg: string;
  };
}

export interface CheckQrRes {
  code: QrStatus;
  cookie: string;
  message: string;
  nickname?: string;
  avatarUrl?: avatarUrl;
}

// 用户信息接口返回值
export interface UserAccountRes {
  code: number;
  account: {
    id: number;
    createTime: number;
  }
  profile: {
    avatarUrl: string;
    backgroundUrl: string;
    nickname: string;
    userId: number;
  }
}