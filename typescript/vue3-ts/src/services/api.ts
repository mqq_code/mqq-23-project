import type { SearchHotRes, SearchSuggestRes, SearchResultRes, QrKeyRes, CreateQrRes, CheckQrRes, UserAccountRes } from './api-types'
import request from './request'
import loadingRequest from './loadingRequest'

// 热搜列表
export const getSearchHot = () => {
  // loadingRequest.get<接口返回值类型>
  return loadingRequest.get<SearchHotRes>('/search/hot')
}

// 搜索建议
export const getSearchSuggest = (keywords: string) => {
  return loadingRequest.get<SearchSuggestRes>('/search/suggest', {
    params: {
      keywords,
      type: 'mobile'
    }
  })
}
// 搜索结果
export const getSearchResult = (keywords: string) => {
  return loadingRequest.get<SearchResultRes>('/cloudsearch', {
    params: {
      keywords
    }
  })
}
// 生成登陆二维码的key
export const getQrKey = () => {
  // url 拼接时间戳防止缓存
  return request.get<QrKeyRes>(`/login/qr/key?now=${Date.now()}`)
}
// 生成登陆二维码
export const getQrCreate = (key: string) => {
  return request.get<CreateQrRes>('/login/qr/create', {
    params: {
      now: Date.now(),
      key,
      qrimg: 'qrimg'
    }
  })
}
// 检测二维码状态
export const getQrCheck = (key: string) => {
  // url 拼接时间戳防止缓存
  return request.post<CheckQrRes>(`/login/qr/check?now=${Date.now()}`, {
    key,
    noCookie: true
  })
}

// 获取用户信息
export const getUserAccount = () => {
  return loadingRequest.get<UserAccountRes>(`/user/account?now=${Date.now()}`, {
    // params: {
    //   // 需要登陆信息的接口必须传入此参数把登陆接口返回的cookie信息传给后端
    //   cookie: encodeURIComponent(localStorage.getItem('cookie') || '')
    // }
  })
}

