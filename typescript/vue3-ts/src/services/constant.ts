// 检测登陆二维码状态
export enum QrStatus{
  error = 800,
  wait = 801,
  confirm = 802,
  success = 803
}