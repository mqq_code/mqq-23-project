import axios from 'axios'
import { showLoadingToast, showToast,  } from 'vant'


let toast: any = null
const showLoading = () => {
  toast = showLoadingToast({
    duration: 0,
    forbidClick: true,
    message: '加载中',
  });
}
const hidLoading = () => {
  toast && toast.close()
}


// 创建axios实例对象
const request = axios.create({
  baseURL: 'http://localhost:3000/' //'https://zyxcl.xyz'
})

// 添加拦截器，统一处理公共参数、错误信息和loading

// 添加请求拦截器
request.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  showLoading()

  // 添加公共参数
  if (config.params) {
    config.params.cookie = encodeURIComponent(localStorage.getItem('cookie') || '')
  } else {
    config.params = {
      cookie: encodeURIComponent(localStorage.getItem('cookie') || '')
    }
  }
  console.log(config)
  return config;
}, function (error) {
  // 对请求错误做些什么
  hidLoading()
  return Promise.reject(error);
});

// 添加响应拦截器
request.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  hidLoading()
  return response;
}, function (error) {
  // 对响应错误做点什么
  hidLoading()
  const { response } = error
  if (response.status === 500) {
    showToast('服务器内部错误，请稍后重试')
  } else if (response.status === 404) {
    showToast({
      message: '网络错误，请稍后重试'
    })
  } else if (response.status === 401) {
    // 登陆信息失效
    showToast({
      message: '登陆信息失效，请重新登陆'
    })
  } else if (response.status === 403) {
    showToast({
      message: '没有访问权限'
    })
  }
  return Promise.reject(error);
});

export default request
