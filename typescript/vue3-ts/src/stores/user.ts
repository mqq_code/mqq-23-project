import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { UserAccountRes } from '@/services/api-types'


const useUserStore = defineStore('user', () => {
  const account = ref<UserAccountRes['account'] | null>(null)
  const profile = ref<UserAccountRes['profile'] | null>(null)

  return {
    account,
    profile
  }
})

export default useUserStore
