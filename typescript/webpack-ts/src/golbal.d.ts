// .ts 可以写类型和逻辑
// .d.ts 只能定义类型

// .d.ts 类型声明文件
// 默认读取项目中所有的 .d.ts 文件
// 类型声明文件没有使用 import、export 就是全局文件，如果使用了就是局部类型需要引入才能使用该文件中定义的类型

// 定义全局类型
interface IPerson {
  name: string;
  age: number;
  sex: '男' | '女';
}


// 定义全局变量类型
declare var _version_: string;
declare const _test_: 'test';
declare let _bb_: string;
declare function tip(text: string): void;
declare interface Window {
  a: string;
}


// 定义引入的模块类型
declare module '*.png'
declare module '*.svg'

// 给第三方包定义类型声明文件
// declare module 'mockjs' {
//   declare function mock(): void;
//   declare const aa: string;
// }