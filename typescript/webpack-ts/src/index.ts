import './scss/style.scss'
import logo from './img/logo.svg'
import img1 from './img/img1.png'
import axios from 'axios' // 自带类型声明文件
import Mock from 'mockjs' // 第三方包没有类型声明文件, 尝试 `npm i --save-dev @types/包名` 下载对应的声明文件，如果没有就需要自己定义类型声明文件
import testObj, { sum, ITest } from './utils/test'
import { Iaa, Ibb } from './typings/a'

// 使用其他文件中抛出的类型和变量
console.log(testObj)
const c: ITest = {
  aa: 'aaaaaa',
  bb: 200
}

// 使用局部类型声明文件中定义的类型
const aa: Iaa = {
  abc: '100'
}

// 使用全局类型声明文件中定义的类型
const xm: IPerson = {
  name: '小明',
  age: 20,
  sex: '男'
}


// ts 中使用全局变量
console.log('window.a', window.a)
console.log('_version_', _version_)
console.log('_test_', _test_)
console.log('_bb_', _bb_)
// tip('100')

const cc: Icc = {
  cc: 'aa'
}
const dd: Idd = '0'


const title = document.querySelector('h1')
title?.addEventListener('click', () => {
  title.style.color = 'red'
})

interface Iobj {
  name: string;
  age: number;
}

const obj: Iobj = {
  name: '小明',
  age: 20
}


/*
  1. .ts可以写类型和逻辑，.d.ts是类型声明文件只能定义类型
  2. 默认读取项目中所有的 .d.ts 文件
  3. 类型声明文件没有使用 import、export 就是全局文件，如果使用了就是局部类型需要引入才能使用该文件中定义的类型
  4. 下载第三方包
    - 自带类型声明文件
    - 第三方包没有类型声明文件, 尝试 `npm i --save-dev @types/包名` 下载对应的声明文件
    - @types/包名 不存在，需要通过`declare mocule '包名'`自己定义该模块的声明文件

*/




