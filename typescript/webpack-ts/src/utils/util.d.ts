// 针对 util.js 文件定义类型
export declare function sort(arr: number[]): number[]
export declare function merge<T>(arr1: T[], arr2: T[]): T[]


// 在局部声明文件中定义全局类型
declare global {

  interface Icc {
    cc: string;
  }
  type Idd = '0' | '1'
  
}

