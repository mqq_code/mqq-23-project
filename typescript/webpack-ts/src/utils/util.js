export function sort(arr) {
  return [...arr].sort((a, b) => a - b)
}

export function merge(arr1, arr2) {
  return [...arr1, ...arr2]
}