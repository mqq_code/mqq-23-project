const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|svg|gif)$/i,
        type: 'asset'
      },
      {
        test: /\.(js|ts|tsx)$/i,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  resolve: {
    // 可以忽略的文件后缀名
    extensions: ['.ts', '.tsx', '.js']
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      filename: 'index.html'
    })
  ],
  devServer: {
    open: true
  }
}