## Vue 
1. 数据驱动视图改变
2. 组件化开发

### 指令
> 带有前缀 v- 的特殊属性，提供底层 dom 操作
1. v-text 类似 js 中的 innerText
2. v-html 类似 js 中的 innerHTML
3. v-if、v-else-if、v-else 条件渲染，会添加删除dom元素
4. v-show 条件渲染，会添加删除元素的 display: none
5. v-for 渲染列表
6. v-on 绑定事件 简写 @
7. v-bind: 让标签的属性可以使用变量, style 和 class 可以传入对象和数组
8. v-model 表单双向绑定
- 三个修饰符 .trim .lazy .number