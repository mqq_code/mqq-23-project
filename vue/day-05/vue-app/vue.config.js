const { defineConfig } = require('@vue/cli-service')
const data = require('./data.json')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {
      devServer.app.get('/api/list', (req, res) => {
        res.send(data)
      })
      return middlewares
    }
  }
})
