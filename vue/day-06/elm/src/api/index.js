import axios from 'axios'

export const getSellerInfo = () => {
  return axios.get('/mqq/sell/api/seller')
}

export const getGoods = () => {
  return axios.get('/mqq/sell/api/goods')
}

export const getRatings = () => {
  return axios.get('/mqq/sell/api/ratings')
}
