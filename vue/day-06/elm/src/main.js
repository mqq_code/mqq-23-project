import Vue from 'vue'
import App from './App.vue'
import 'animate.css'
import VConsole from 'vconsole'

Vue.config.productionTip = false
const v = new VConsole()
console.log(v)

new Vue({
  render: h => h(App)
}).$mount('#app')
