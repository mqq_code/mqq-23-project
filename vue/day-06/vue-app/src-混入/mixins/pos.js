// 定义混入抽离公共逻辑，组件中的属性都可以在此文件中使用
const posMixin = {
  data () {
    return {
      pos: { x: '', y: '' },
      title: '测试一下',
      num: 100
    }
  },
  methods: {
    add () {
      console.log(this.num)
      this.num++
    }
  },
  mounted () {
    this.mouseMove = e => {
      this.pos = { x: e.pageX, y: e.pageY }
    }
    document.addEventListener('mousemove', this.mouseMove)
  },
  beforeDestroy () {
    document.removeEventListener('mousemove', this.mouseMove)
  }
}

export default posMixin
