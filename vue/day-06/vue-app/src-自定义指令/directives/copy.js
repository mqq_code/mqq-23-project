const copy = {
  bind (el, binding) { // 指令绑定到元素时执行，无法获取父元素
    console.log('bind', el, binding.value)
    el.$copyValue = binding.value
    el.$clickFunction = () => {
      const textarea = document.createElement('textarea')
      textarea.value = el.$copyValue
      document.body.appendChild(textarea)
      textarea.select()
      document.execCommand('copy')
      textarea.remove()
      alert('复制成功' + el.$copyValue)
    }
    el.addEventListener('click', el.$clickFunction)
  },
  inserted (el, binding) { // 元素插入到父节点时执行，可以获取父元素，但是不保证父元素在页面
    console.log('inserted', el, binding.value)
  },
  update (el, { value, oldValue }) { // 组件更新时执行
    // console.log('update', el, binding)
    if (value !== oldValue) {
      el.$copyValue = value // 把最新的值存到 el
      console.log('数据更新了', value, oldValue)
    }
  },
  componentUpdated () { // 组件更新完成执行，可以获取到最新的dom
  },
  unbind (el, binding) { // 只调用一次，指令与元素解绑时调用。
    console.log('unbind', el)
    el.removeEventListener('click', el.$clickFunction)
  }
}

export default copy
