import copy from './copy'
import loading from './loading'

const directives = {
  copy,
  loading
}

export default {
  install (Vue) {
    // 注册自定义指令
    Object.keys(directives).forEach(key => {
      Vue.directive(key, directives[key])
    })
  }
}
