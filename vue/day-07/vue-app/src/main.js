import Vue from 'vue'
import App from './App.vue'
import 'animate.css'

Vue.config.productionTip = false
// eventBus \ 发布订阅模式 实现跨组件传值
Vue.prototype.$bus = new Vue()

new Vue({
  render: h => h(App)
}).$mount('#app')
