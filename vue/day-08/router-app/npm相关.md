## 包管理器
1. npm: node自带
2. cnpm: 需要全局安装 `npm install -g cnpm`
3. yarn: 需要全局安装 `npm install -g yarn`

### npm下载源
1. 设置淘宝镜像：`npm config set registry https://registry.npmmirror.com/`
2. 查看：`npm config get registry`

### 检查版本
1. `npm -v`
2. `cnpm -v`
3. `yarn -v`

> 注意：一个项目中下包只能用一种包管理器

### 我的版本
1. node: 16.14.0
2. npm: 6.14.18
> 修改 npm 版本: `npm install -g npm@版本号`
3. yarn: 1.22.18

### 切换 node 版本
1. windows: nvm
2. mac: nvm、n
> mac 安装 n: `npm i -g n`
> mac 安装 nvm: `https://juejin.cn/post/7083026831263137800`
> windows 安装 nvm: `https://juejin.cn/post/6844904164397416462`