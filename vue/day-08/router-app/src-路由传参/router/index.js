import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home'
import Movie from '../pages/Movie'
import Hot from '../pages/Hot'
import Coming from '../pages/Coming'
import Cinema from '../pages/Cinema'
import Mine from '../pages/Mine'
import Detail from '../pages/Detail'
import City from '../pages/City'
import Login from '../pages/Login'
import Notfound from '../pages/404'

Vue.use(VueRouter)

const router = new VueRouter({
  // 路由模式： history地址栏没有 #，hash地址栏有#
  mode: 'history',
  // 配置路由表
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/movie',
      children: [
        {
          path: '/movie',
          component: Movie,
          name: 'movie',
          redirect: '/movie/hot',
          children: [
            {
              path: '/movie/hot',
              name: 'hot',
              component: Hot
            },
            {
              path: '/movie/coming',
              name: 'coming',
              component: Coming
            }
          ]
        },
        {
          path: '/cinema',
          name: 'cineme',
          component: Cinema
        },
        {
          path: '/mine',
          name: 'mine',
          component: Mine
        }
      ]
    },
    {
      // 动态路由
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/city',
      name: 'city',
      component: City
    },
    {
      path: '/404',
      name: '404',
      component: Notfound
    },
    {
      path: '*',
      redirect: '/404' // 重定向
    }
  ]
})

export default router
