import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home'
import Detail from '../pages/Detail'
import City from '../pages/City'
import Login from '../pages/Login'

Vue.use(VueRouter)

const router = new VueRouter({
  // 路由模式： history地址栏没有 #，hash地址栏有#
  mode: 'history',
  // 配置路由
  routes: [
    {
      path: '/home',
      component: Home
    },
    {
      path: '/detail',
      component: Detail
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/city',
      component: City
    }
  ]
})

export default router
