import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/pages/home/Home'
import Movie from '@/pages/home/movie/Movie'
import Hot from '@/pages/home/movie/hot/Hot'
import Coming from '@/pages/home/movie/coming/Coming'
import Cinema from '@/pages/home/cinema/Cinema'
// import Mine from '@/pages/home/mine/Mine'
// import Detail from '@/pages/detail/Detail'
// import City from '@/pages/city/City'
// import Login from '@/pages/login/Login'
import Notfound from '@/pages/404'

Vue.use(VueRouter)

const router = new VueRouter({
  // 路由模式： history地址栏没有 #，hash地址栏有#
  mode: 'hash',
  // 配置路由表
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/movie',
      meta: {
        requiredAuth: true
      },
      children: [
        {
          path: '/movie',
          component: Movie,
          name: 'movie',
          redirect: '/movie/hot',
          children: [
            {
              path: '/movie/hot',
              name: 'hot',
              component: Hot,
              meta: {
                title: '热映'
              }
            },
            {
              path: '/movie/coming',
              name: 'coming',
              component: Coming,
              meta: {
                title: '即将上映'
              }
            }
          ]
        },
        {
          path: '/cinema',
          name: 'cineme',
          component: Cinema,
          meta: {
            title: '影院'
          }
          // 路由独享守卫，只有跳转此路由之前会执行此函数
          // beforeEnter: (to, from, next) => {
          //   console.log('cinema独享守卫')
          //   next()
          // }
        },
        {
          path: '/mine',
          name: 'mine',
          // 组件懒加载，异步加载
          component: () => import('../pages/home/mine/Mine'),
          meta: {
            title: '个人中心'
          }
        }
      ]
    },
    {
      // 动态路由
      path: '/detail/:id',
      name: 'detail',
      component: () => import('../pages/detail/Detail'),
      meta: {
        // 存信息
        requiredAuth: true,
        title: '详情'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../pages/login/Login'),
      meta: {
        title: '用户登陆'
      }
    },
    {
      path: '/city',
      name: 'city',
      component: () => import('../pages/city/City'),
      meta: {
        requiredAuth: true,
        title: '选择城市'
      }
    },
    {
      path: '/404',
      name: '404',
      component: Notfound,
      title: '404'
    },
    {
      path: '*',
      redirect: '/404' // 重定向
    }
  ]
})

// 全局前置守卫(所有页面跳转之前执行)
router.beforeEach((to, from, next) => {
  // console.log('from', from)
  console.log('to', to)
  document.title = to.meta.title
  if (to.matched.some(v => v.meta.requiredAuth)) { // 判断此页面是否需要登陆
    console.log('此页面需要登陆', to)
    const token = localStorage.getItem('token')
    if (token) { // 判断此用户是否已登陆
      next()
    } else {
      // 此页面需要登陆，并且当前本地没有用户的登录信息，就跳转到登录页面
      next({
        path: '/login',
        query: {
          redirectUrl: to.fullPath
        }
      })
    }
  } else {
    next()
  }
})

// 全局后置守卫
router.afterEach((to, from) => {
  // 可以再次函数中做统计数据相关的功能
})

// 处理重定向报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) { return originalPush.call(this, location, onResolve, onReject) }
  return originalPush.call(this, location).catch((err) => err)
}

export default router
