const { defineConfig } = require('@vue/cli-service')
const express = require('express')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.use(express.json())
      devserver.app.post('/api/login', (req, res) => {
        const { name, password } = req.body
        if (name === 'aaa' && password === '123') {
          res.send({
            code: 0,
            msg: '登陆成功',
            token: 'successssssssssssssssss'
          })
        } else {
          res.send({
            code: -1,
            msg: '用户名或密码错误'
          })
        }
      })
      return middlewares
    },
    proxy: {
      '/mqq': {
        target: 'https://api.map.baidu.com',
        pathRewrite: { '^/mqq': '' }
      }
    }
  }
})
