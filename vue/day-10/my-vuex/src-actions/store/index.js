import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: { // 存数据
    list: []
  },
  mutations: { // 修改数据的唯一方法，必须是同步函数
    setList (state, payload) {
      state.list = payload
    }
  },
  actions: { // 处理异步
    getList (context, payload) {
      // context: 类似组件中的 this.$store
      // console.log('context', context)
      axios.get('https://m.maizuo.com/gateway?cityId=310100&ticketFlag=1&k=1485526', {
        headers: {
          'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"16659764383978050249162753","bc":"310100"}',
          'X-Host': 'mall.film-ticket.cinema.list'
        }
      }).then(res => {
        // console.log(res.data.data.cinemas)
        // 调用 mutations 中的函数修改数据
        context.commit('setList', res.data.data.cinemas)
      })
    }
  },
  getters: { // 功能类似组件中的计算属性
  }
})

export default store
