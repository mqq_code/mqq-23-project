import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: { // 存数据
    num: 10,
    a: 'aa',
    b: 'bbb',
    c: 'cccc',
    d: 'dddd',
    e: 'eeee',
    obj: {
      name: '小米'
    }
  },
  mutations: { // 修改数据的唯一方法，必须是同步函数
    addNum (state, payload) {
      // state: 仓库数据
      // payload: 参数
      state.num += payload
    },
    decNum (state, payload) {
      state.num -= payload
    },
    setAge (state, payload) {
      // 给对象添加新属性
      Vue.set(state.obj, 'age', payload)
    }
  },
  getters: { // 功能类似组件中的计算属性
    word (state) {
      return state.a + state.b + state.c + state.d + state.e + state.num
    }
  }
})

export default store
