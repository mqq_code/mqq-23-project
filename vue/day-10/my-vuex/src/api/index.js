import axios from 'axios'

export const getCity = () => {
  return axios.get('https://m.maizuo.com/gateway?k=6885639', {
    headers: {
      'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"16659764383978050249162753","bc":"110100"}',
      'X-Host': 'mall.film-ticket.city.list'
    }
  })
}
