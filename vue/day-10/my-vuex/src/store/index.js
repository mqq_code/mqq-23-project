import Vue from 'vue'
import Vuex from 'vuex'
import { getCity } from '../api'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: { // 存数据
    cities: [], // 城市列表
    curCity: JSON.parse(localStorage.getItem('curCity')) || {} // 当前选择的城市
  },
  mutations: { // 修改数据的唯一方法，必须是同步函数
    setCities (state, payload) {
      state.cities = payload
    },
    setCurCity (state, payload) {
      state.curCity = payload
      localStorage.setItem('curCity', JSON.stringify(payload))
    }
  },
  actions: { // 处理异步
    async getCites (context) {
      const res = await getCity()
      context.commit('setCities', res.data.data.cities)
    }
  },
  getters: { // 功能类似组件中的计算属性
  }
})

export default store
