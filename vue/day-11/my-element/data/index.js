const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')

// 生成随机数据
const data = Mock.mock({
  'list|1000': [{
    id: '@id',
    name: '@cname',
    'age|20-30': 20,
    'no|+1': 1,
    'sex|0-1': 0,
    email: '@email',
    address: '@county(true)',
    'married|0-1': 0,
    createTime: '@date(T)',
    avatar: '@image(100x100, @color, @color, png, @word)'
  }]
})

console.log(data.list)

fs.writeFileSync(path.join(__dirname, 'user.json'), JSON.stringify(data.list))
