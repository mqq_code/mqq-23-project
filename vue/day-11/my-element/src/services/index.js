import axios from 'axios'

export const getlist = params => {
  return axios.get('/api/list', { params })
}

export const removeUser = id => {
  return axios.post('/api/list/del', { id })
}

export const updateUser = params => {
  return axios.post('/api/list/update', params)
}

export const addUser = params => {
  return axios.post('/api/list/add', params)
}
