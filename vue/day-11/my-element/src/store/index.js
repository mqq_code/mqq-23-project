import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: { // 存数据
  },
  mutations: { // 修改数据的唯一方法，必须是同步函数
  },
  actions: { // 处理异步
  },
  getters: { // 功能类似组件中的计算属性
  }
})

export default store
