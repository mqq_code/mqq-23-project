const { defineConfig } = require('@vue/cli-service')
const fs = require('fs')
const path = require('path')
const express = require('express')

const readUserlist = () => {
  return new Promise((resolve, reject) => {
    const userPath = path.join(__dirname, './data/user.json')
    fs.readFile(userPath, 'utf-8', (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(JSON.parse(data))
      }
    })
  })
}
const writeUserlist = (file) => {
  return new Promise((resolve, reject) => {
    const userPath = path.join(__dirname, './data/user.json')
    fs.writeFile(userPath, file, (err) => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.use(express.json())
      const numKey = ['sex', 'age', 'married']

      devserver.app.get('/api/list', async (req, res) => {
        const { page, pagesize, ...other } = req.query
        const userlist = await readUserlist()
        if (isNaN(page) || isNaN(pagesize)) {
          res.send({
            code: 1000,
            msg: '参数错误',
            data: {}
          })
        } else {
          let newlist = userlist
          Object.keys(other).forEach(key => {
            if (numKey.includes(key)) {
              newlist = newlist.filter(v => v[key] === other[key] * 1)
            } else {
              newlist = newlist.filter(v => v[key].includes(other[key]))
            }
          })
          res.send({
            code: 200,
            msg: '成功',
            data: {
              list: newlist.slice((page - 1) * pagesize, page * pagesize),
              total: newlist.length,
              page: page * 1,
              pagesize: pagesize * 1
            }
          })
        }
      })

      devserver.app.post('/api/list/del', async (req, res) => {
        const { id } = req.body
        const userlist = await readUserlist()
        const index = userlist.findIndex(item => item.id === id)
        if (index > -1) {
          userlist.splice(index, 1)
          await writeUserlist(JSON.stringify(userlist))
          res.send({
            code: 200,
            msg: '成功',
            data: {}
          })
        } else {
          res.send({
            code: 1002,
            msg: '参数错误，没有此用户',
            data: {}
          })
        }
      })

      const paramsKey = ['name', 'sex', 'age', 'married', 'address', 'email']
      devserver.app.post('/api/list/add', async (req, res) => {
        const flag = paramsKey.every(key => {
          return req.body[key] || typeof req.body[key] === 'number'
        })
        if (flag) {
          const userlist = await readUserlist()
          userlist.push({
            ...req.body,
            id: Date.now() + '',
            createTime: Date.now(),
            no: userlist[userlist.length - 1].no + 1
          })
          await writeUserlist(JSON.stringify(userlist))
          res.send({
            code: 200,
            msg: '成功',
            data: {}
          })
        } else {
          res.send({
            code: 1003,
            msg: '缺少参数',
            data: {}
          })
        }
      })

      devserver.app.post('/api/list/update', async (req, res) => {
        const { id } = req.body
        const userlist = await readUserlist()
        const index = userlist.findIndex(item => item.id === id)
        if (index > -1) {
          userlist.splice(index, 1, req.body)
          await writeUserlist(JSON.stringify(userlist))
          res.send({
            code: 200,
            msg: '成功',
            data: {}
          })
        } else {
          res.send({
            code: 1002,
            msg: '参数错误，没有此用户',
            data: {}
          })
        }
      })
      return middlewares
    }
  }
})
