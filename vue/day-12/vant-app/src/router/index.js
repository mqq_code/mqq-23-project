import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/home/Home'
import Product from '@/views/home/product/Product'
import Cart from '@/views/home/cart/Cart'
import Mine from '@/views/home/mine/Mine'
import Detail from '@/views/detail/Detail'
import Address from '@/views/address/Address'
import Create from '@/views/create/Create'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/product',
    children: [
      {
        path: '/product',
        name: 'product',
        component: Product
      },
      {
        path: '/cart',
        name: 'cart',
        component: Cart
      },
      {
        path: '/mine',
        name: 'mine',
        component: Mine
      }
    ]
  },
  {
    path: '/detail',
    name: 'detail',
    component: Detail
  },
  {
    path: '/address',
    name: 'address',
    component: Address
  },
  {
    path: '/create',
    name: 'create',
    component: Create
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
