import Vue from 'vue'
import Vuex from 'vuex'
import test1 from './test1'
import test2 from './test2'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    curAddress: {}, // 当前选中的地址
    addressList: [ // 所有地址列表
      // {
      //   id: '1',
      //   name: '张三',
      //   tel: '13000000000',
      //   address: '浙江省杭州市西湖区文三路 138 号东方通信大厦 7 楼 501 室'
      // }
    ],
    wareList: [], // 所有商品列表
    cartList: [] // 购物车商品列表
  },
  mutations: {
    createAddress (state, payload) {
      state.addressList.push({
        ...payload,
        id: Date.now(),
        address: payload.addressDetail
      })
    },
    setCurAddress (state, payload) {
      state.curAddress = payload
    },
    updateAddress (state, payload) {
      const index = state.addressList.findIndex(v => v.id === payload.id)
      if (index > -1) {
        state.addressList.splice(index, 1, { ...payload, address: payload.addressDetail })
      }
    },
    setWareList (state, payload) {
      state.wareList = payload
    },
    changeCartList (state, { goods, type }) {
      const index = state.cartList.findIndex(v => v.wareId === goods.wareId)
      if (index > -1) {
        state.cartList[index].wareCount += type
        if (state.cartList[index].wareCount <= 0) {
          state.cartList.splice(index, 1)
        }
      } else {
        state.cartList.push({
          ...goods,
          wareCount: 1,
          checked: true
        })
      }
    },
    changeCartChecked (state, payload) {
      payload.checked = !payload.checked
    },
    changeAll (state, payload) {
      state.cartList.forEach(item => {
        item.checked = payload
      })
    }
  },
  getters: {
    total (state) {
      return state.cartList.reduce((prev, val) => {
        return {
          count: prev.count + (val.checked ? val.wareCount : 0),
          price: prev.price + (val.checked ? val.wareCount * val.warePrice : 0)
        }
      }, { count: 0, price: 0 })
    }
  },
  actions: {
  },
  modules: {
    test1,
    test2
  }
})
