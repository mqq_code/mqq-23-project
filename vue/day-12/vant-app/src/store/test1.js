
export default {
  namespaced: true, // 命名空间，让 mutations 和 actions 的函数指针对当前模块
  state: () => {
    return {
      title: '测试仓库1',
      name: '111111'
    }
  },
  mutations: {
    setTitle (state, payload) {
      console.log('修改test1')
      state.title = payload
    }
  }
}
