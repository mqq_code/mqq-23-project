const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/mqq': {
        target: 'https://api.map.baidu.com',
        pathRewrite: { '^/mqq': '' }
      }
    }
  }
})
