import { defineStore } from 'pinia'
import { ref, computed, watch } from 'vue'
import useTitleStore from './title'


// 组合式
const useCounterStore = defineStore('counter', () => {
  // 使用其他仓库
  const titleStore = useTitleStore()

  let timer = null
  const count = ref(0)

  watch(count, () => {
    console.log('count 改变了', count.value)
  })

  const start = () => {
    timer = setInterval(() => {
      count.value++
      if (count.value === 10) {
        titleStore.setTitle('count 大于等于 10')
      }
    }, 1000)
  }

  const stop = () => {
    clearInterval(timer)
  }


  return {
    count,
    start,
    stop
  }

})

export default useCounterStore

