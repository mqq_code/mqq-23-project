import { defineStore } from 'pinia'
import { ref, computed } from 'vue'

// 组合式
const useTitleStore = defineStore('title', () => {

  const title = ref('默认标题')

  const titleLen = computed(() => title.value.length)

  const setTitle = text => {
    title.value = text
  }


  return {
    title,
    titleLen,
    setTitle
  }

})

export default useTitleStore




// 选项式
// const useTitleStore = defineStore('counter', {
//   state: () => {
//     return {
//       title: '默认标题'
//     }
//   },
//   getters: {
//     titleLen(state) {
//       return state.title.length
//     }
//   },
//   actions: {
//     setTitle(text) {
//       this.title = text
//     }
//   }
// })
// export default useTitleStore

