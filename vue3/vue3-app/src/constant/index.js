export const tablistConfig = [
  {
    title: '资金管理',
    list: [
      {
        title: '导航1',
        path: '/child1'
      },
      {
        title: '导航2',
        path: '/child2'
      },
      {
        title: '导航3',
        path: '/child3'
      }
    ]
  },
  {
    title: '系统管理',
    list: [
      {
        title: '导航4',
        path: '/child4'
      },
      {
        title: '导航5',
        path: '/child5'
      }
    ]
  }
]