import Mock from 'mockjs'
import MD5 from 'md5'
// 模拟接口
Mock.mock('/api/login', 'post', (options) => {
  const { username, password } = JSON.parse(options.body)
  if (username === '小明' && password === '123') {
    return {
      code: 0,
      msg: '成功',
      token: MD5(username + password + Date.now())
    }
  } else {
    return {
      code: -1,
      msg: '用户名或密码错误'
    }
  }
})
