import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import express from 'express'
import MD5 from 'md5'
import { createServer as createViteServer } from 'vite'

export default defineConfig({
  plugins: [
    vue(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})

// middlewareMode() {
    //   // 以中间件模式创建 Vite 服务器
    //   const vite = await createViteServer({
    //     server: { middlewareMode: true },
    //     appType: 'custom', // 不引入 Vite 默认的 HTML 处理中间件
    //   })
    //   const app = express()
    //   // 将 vite 的 connect 实例作中间件使用
    //   app.use(vite.middlewares)
    //   app.use(express.json())
    //   app.post('/login', (req, res) => {
    //     const { username, password } = req.body
    //     if (username === '小明' && password === '123') {
    //       res.send({
    //         code: 0,
    //         msg: '成功',
    //         token: MD5(username + password + Date.now())
    //       })
    //     } else {
    //       res.send({
    //         code: -1,
    //         msg: '用户名或密码错误'
    //       })
    //     }
    //   })
    // }
