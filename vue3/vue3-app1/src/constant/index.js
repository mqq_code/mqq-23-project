export const tablistConfig = [
  {
    title: '资金管理',
    list: [
      {
        title: '导航1',
        path: '/child1'
      },
      {
        title: '导航2',
        path: '/child2'
      },
      {
        title: '导航3',
        path: '/child3'
      }
    ]
  },
  {
    title: '系统管理',
    list: [
      {
        title: '导航4',
        path: '/child4'
      },
      {
        title: '导航5',
        path: '/child5'
      }
    ]
  }
]

export const menulist = [
  {
    title: '董事会',
    children: [
      {
        title: '人事部',
        children: [
          {name: '人事第一人', pinyin: 'renshidiyiren'},
          {name: '人事小白', pinyin: 'renshixiaobai'},
          {name: '人事大牛', pinyin: 'renshidaniu'},
          {name: '小牛也是人事', pinyin: 'xiaoniu'}
        ]
      },
      {
        title: '研发部',
        children: [
          {name: '王大牛', pinyin: 'wangdaniu'},
          {name: '石金宝', pinyin: 'shijinbao'},
          {name: '李大炮', pinyin: 'lidapao'},
          {name: '刘铁锤', pinyin: 'liutiechui'},
          {name: '蔡完蛋', pinyin: 'caiwandan'},
          {name: '牛铁柱', pinyin: 'niutiezhu'}
        ]
      },
      {
        title: '产品部',
        children: [
          {name: '华莱事', pinyin: 'hualaishi'},
          {name: '卖当牢', pinyin: 'maidanglao'},
          {name: '啃得鸡', pinyin: 'kendeji'},
          {name: '威露是', pinyin: 'weilushi'}
        ]
      }
    ]
  },
  {
    title: '安保会',
    children: [
      {
        title: 'A队',
        children: [
          {name: '王敏', pinyin: 'wangmin'},
          {name: '王大翠', pinyin: 'wangdacui'},
          {name: '李铁柱', pinyin: 'litiezhu'},
          {name: '秦天柱', pinyin: 'qintianzhu'},
          {name: '魏震天', pinyin: 'weizhentian'}
        ]
      },
      {
        title: 'B队',
        children: [
          {name: '立刻', pinyin: 'like'},
          {name: '留底', pinyin: 'liudi'},
          {name: '李克', pinyin: 'like'},
          {name: '戴维', pinyin: 'daiwei'},
          {name: '龙王', pinyin: 'longwang'}
        ]
      }
    ]
  }
]
