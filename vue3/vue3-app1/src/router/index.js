import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Child1 from '../views/Child1.vue'
import Child2 from '../views/Child2.vue'
import Child3 from '../views/Child3.vue'
import Child4 from '../views/Child4.vue'
import Child5 from '../views/Child5.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      redirect: '/child1',
      children: [
        {
          path: '/child1',
          name: 'child1',
          component: Child1
        },
        {
          path: '/child2',
          name: 'child2',
          component: Child2
        },
        {
          path: '/child3',
          name: 'child3',
          component: Child3
        },
        {
          path: '/child4',
          name: 'child4',
          component: Child4
        },
        {
          path: '/child5',
          name: 'child5',
          component: Child5
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    }
  ]
})

router.beforeEach((to, from) => {
  if (to.name !== 'login') {
    const token = localStorage.getItem('token')
    if (!token) {
      return { path: '/login' }
    }
  }
})

export default router
