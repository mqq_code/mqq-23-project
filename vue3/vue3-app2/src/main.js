
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'


// 标准屏幕 375px， 1rem === 100px

// 计算当前屏幕和标准屏幕的比例 320 / 375
// const scale = 320 / 375

// 1rem === 100 * scale
// html.fontSize = 100 * scale

function getRem() {
  // 当前屏幕和标准屏幕的比例
  const scale = document.body.clientWidth / 375
  // 根据当前屏幕和标准屏幕的比例修改 html 的 font-size
  document.documentElement.style.fontSize = 100 * scale + 'px'
}
getRem()


const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(ElementPlus)

app.mount('#app')
