import { ref, watch } from 'vue'

// 使用组合式函数抽离公共逻辑，名称一般已 use 开头，函数内可以使用 vue 的 api
const useCountDown = (defaultCount = 10) => {

  const count = ref(defaultCount)
  let timer = null
  const start = () => {
    timer = setInterval(() => {
      count.value--
      if (count.value === 0) {
        clearInterval(timer)
      }
    }, 1000)
  }
  const stop = () => {
    clearInterval(timer)
  }

  watch(count, () => {
    console.log('count 改变了')
  })

  return {
    count,
    start,
    stop
  }
}


export default useCountDown
