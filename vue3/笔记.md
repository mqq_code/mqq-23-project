## vue3

### 响应式语法
1. reactive
- 定义数组和对象形式的响应式变量
- 不可以重新赋值
2. ref
- 定义任何类型的响应式变量
- 在 js 中使用时需要通过 value 属性获取和修改
- 获取 dom 元素
- 获取子组件的数据和方法

### 计算属性
- 通过 computed 函数定义计算属性返回一个 ref 对象，特性和 vue2 一致
```js
import { computed } from 'vue'

const total = computed(() => {
  return arr.value.reduce((prev, val) => prev + val)
})
```

### watch
1. 监听单个响应式变量, 默认深度监听
```js
watch(响应式变量, (newVal, oldVal) => {
  // 变量改变执行函数
  // newVal: 改变后的值，oldVal：修改前的数据
})
```
2. 监听多个响应式变量
```js
watch([变量1, 变量2, ...], ([newVal1, newVal2, ...], [oldVal1, oldVal2, ...]) => {
  // 变量改变执行函数
})
```
3. 监听对象的属性变化
```js
const obj = reactive({ name: '', age: 20 })
watch(() => obj.age, (newVal, oldVal) => {
  // 变量改变执行函数
  // newVal: 改变后的值，oldVal：修改前的数据
})
```
4. 停止监听
```js
const stop = watch(source, callback)

// 当已不再需要该侦听器时：
stop()
```
5. 第三个参数
```js
watch(变量, () => {
  // todo
}, {
  deep: true, // 深度监听
  immediate: true, // 在侦听器创建时立即触发回调
  flush: 'post' // 在页面更新后执行 watch 的回调函数
})
```
6. watchEffect 自动监听函数内使用的变量
```js
const num = ref(0)
watchEffect(() => {
  console.log(num.value) // num 修改时自动执行此函数
})
```
7. watchPostEffect 自动监听函数内使用的变量，并且在页面更新后执行


### 选项式生命周期
1. setup
2. beforeCreate
3. created
4. beforeMount
5. mounted
6. beforeUpdate
7. updated
8. beforeUnmount
9. unmounted

### 组合式生命周期 (没有创建阶段)
1. onBeforeMount
2. onMounted
3. onBeforeUpdate
4. onUpdated
5. onBeforeUnmount
6. onUnmounted

### 组件通讯
1. 父 -> 子 props， 子组件中需要通过 defineProps 接收参数
2. 子 -> 父 emit 调用自定义事件，子组件中需要通过 defineEmits 接收自定义事件名
3. provide\inject 依赖注入，父级组件跨级传参给所有后代组件

### 组件上的 v-model （把 vue2 中的 v-model 和 .sync 修饰符合并）
```jsx
// 语法躺
<Child v-model="num" v-model:visible="flag" />
// 完整写法
<Child
  :modelValue="num"
  @update:modelValue="e => num = e"
  :visible="flag"
  @update:visible="e => visible = e"
/>
```

### Teleport: 让元素渲染到指定位置
```jsx
<Teleport to="body">
  {/* 把此元素渲染到 body 中 */}
  <div>弹窗</div>
</Teleport>
```

### 路由
```js
// vue2
// this.$route // 当前路由信息
// this.$router // 路由实例

// vue3
import { useRoute, useRouter } from 'vue-router'
const route = useRoute() // 当前路由信息
const router = useRouter() // 路由实例
```

### 状态管理 pinia
```js
import { defineStore } from 'pinia'

// 定义 store
const useCounterStore = defineStore('counter', () => {
  // 定义变量和方法
  const count = ref(0)
  function increment() {
    count.value++
  }
  return { count, increment }
})
export default useCounterStore


// 组件中使用 store
import useCounterStore from './CounterStore.js'

const CounterStore = useCounterStore()
// 可以在组件中直接修改 store 中的数据
CounterStore.count++
CounterStore.increment()

```




