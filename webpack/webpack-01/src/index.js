// import Person from './utils/person'
// import sum from './utils/sum'
// import './scss/style.scss'
// import '@babel/polyfill' // 兼容低版本浏览器，补全低版本浏览器语法


// import img1 from './imgs/img1.gif'
// import img2 from './imgs/img2.png'

// let imgbox = new Image()
// imgbox.src = img1
// imgbox.width = 100
// document.body.appendChild(imgbox)

// let img2box = new Image()
// img2box.src = img2
// img2box.width = 100
// document.body.appendChild(img2box)


// let xm = new Person('小明', 20)
// const a = sum(xm.age, 20)

// const h1 = document.querySelector('h1')
// h1.style.color = '#ffffff'


const arr = [
  {
      height: 180,
      bgColor: 'red',
      animateTime: '1s'
  },
  {
      height: 280,
      bgColor: 'blue',
      animateTime: '0.5s'
  },
  {
      height: 30,
      bgColor: 'orange',
      animateTime: '0.7s'
  },
  {
      height: 180,
      bgColor: 'pink',
      animateTime: '0.3s'
  },
  {
      height: 180,
      bgColor: 'purple',
      animateTime: '1s'
  }
]

const clickBoxArr = [] // 存点击的元素
let firstFinish = false

function createBox({ height, bgColor, animateTime }) {
  return new Promise((resolve, reject) => {
    const box = document.createElement('div')
    box.style.cssText = `
      float: left;
      width: 100px;
      background: ${bgColor};
      transition: height ${animateTime} linear;
      height: 0px;
    `
    document.body.appendChild(box)
    setTimeout(() => {
      box.style.height = height + 'px'
    })
    // 过渡结束事件
    const transitionend = () => {
      resolve()
      box.removeEventListener('transitionend', transitionend)
    }
    box.addEventListener('transitionend', transitionend)
    // 绑定点击事件
    box.addEventListener('click', () => {
      if (firstFinish && clickBoxArr.length === 0) {
        clickBoxArr.push(box)
        addHeight()
      } else {
        clickBoxArr.push(box)
      }
    })
  })
}
function addHeight() {
  const el = clickBoxArr[0]
  el.style.height = el.offsetHeight + 20 + 'px'
  const transitionend = () => {
    clickBoxArr.shift()
    if (clickBoxArr.length > 0) {
      addHeight()
    }
    el.removeEventListener('transitionend', transitionend)
  }
  el.addEventListener('transitionend', transitionend)
}

async function run() {
  for (let i = 0; i < arr.length; i ++) {
    await createBox(arr[i])
  }
  firstFinish = true
  if (clickBoxArr.length > 0) {
    addHeight()
  }
}

run()
