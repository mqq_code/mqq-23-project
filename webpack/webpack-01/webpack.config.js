const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// webpack 配置项
module.exports = {
  mode: 'development', // 打包模式 'development' or 'production'
  entry: './src/index.js', // webpack 打包的入口文件
  output: { // 配置输出目录
    path: path.join(__dirname, 'build'), // 绝对路径
    filename: 'js/index.js', // 输出文件名
    clean: true, // 每次打包清空dist目录
    assetModuleFilename: 'imgs/[name][ext]' // 输出图片路径
  },
  // 插件
  plugins: [
    new HtmlWebpackPlugin({ // 打包时生成html文件，并自动引入打包后的js
      template: './src/index.html',
      filename: 'index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'css/style.css'
    }) // 把css抽离成单独的文件
  ],
  module: {
    // 配置 loader （加载器，让 js 可以解析其他类型的文件）
    rules: [
      {
        test: /\.(css|sass|scss)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.html$/i,
        use: 'html-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/, // 排除的目录
        use: 'babel-loader'
      }
    ]
  },
  // 起服务
  devServer: {
    port: 3000,
    open: true // 自动打开浏览器
  }
}