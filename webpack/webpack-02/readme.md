## webpack 自动化构建工具（打包工具）
> 压缩混淆代码、转译scss、less、es6+ 语法、起服务....

1. npm i webpack webpack-cli -D
> 在 package.json 的 scripts 选项中配置 "build": "webpack", 在终端使用 npm run build 执行
2. npm i html-webpack-plugin -D  // 插件：打包时生成html文件，并自动引入打包后的js
3. npm i webpack-dev-server -D // 起服务
> 在 package.json 的 scripts 选项中配置 "dev": "webpack serve", 在终端使用 npm run dev 执行
