const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// webpack 配置项
module.exports = {
  mode: 'development', // 打包模式 'development' or 'production'
  devtool: 'source-map', // 将编译后的代码映射回原始源代码
  // entry: ['./src/a.js', './src/index.js'], // 多个入口文件，单个出口文件
  entry: { // 配置多个入口文件，多个出口文件
    index: './src/index/index.js',
    detail: './src/detail/index.js'
  },
  output: { // 配置输出目录
    path: path.join(__dirname, 'build'), // 绝对路径
    filename: 'js/[name].[hash:8].js', // 输出文件名
    clean: true, // 每次打包清空dist目录
    assetModuleFilename: 'imgs/[name].[hash:8][ext]' // 输出图片路径
  },
  // 插件
  plugins: [
    new HtmlWebpackPlugin({ // 打包时生成html文件，并自动引入打包后的js
      template: './src/index.html',
      filename: 'index.html',
      chunks: ['index']
    }),
    new HtmlWebpackPlugin({
      template: './src/detail.html',
      filename: 'detail.html',
      chunks: ['detail']
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash:8].css'
    }) // 把css抽离成单独的文件
  ],
  module: {
    // 配置 loader （加载器，让 js 可以解析其他类型的文件）
    rules: [
      {
        test: /\.(css|sass|scss)$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.html$/i,
        use: 'html-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/, // 排除的目录
        use: 'babel-loader'
      }
    ]
  },
  resolve: {
    // 配置别名
    alias: {
      '@': path.resolve(__dirname, 'src'),
      Util: path.resolve(__dirname, 'src/utils/'),
    },
  },
  // 起服务
  devServer: {
    port: 3000,
    open: true, // 自动打开浏览器
    proxy: {
      // 配置代理地址
      // /mqq/a/api/list => http://localhost:3001/mqq/a/api/list
      // '/mqq': 'http://localhost:3001',

      // /mqq/a/api/list => http://localhost:3001/a/api/list
      '/mqq': {
        target: 'http://localhost:3001', // 代理的地址
        pathRewrite: { '^/mqq': '' }, // 重写路径
        changeOrigin: true, // 覆盖主机头信息
      },
    },
    
    setupMiddlewares: (middlewares, devServer) => {
      // 模拟后端接口
      devServer.app.get('/test/list', (req, res) => {
        res.send([1, 2, 3, 4, 5, 6, 7])
      })
      return middlewares
    },
  }
}