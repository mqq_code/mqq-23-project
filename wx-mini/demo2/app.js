// app.js
App({
  onLaunch() {
    console.log('%c 小程序初始化成功', 'color: red; font-size: 20px')
  },
  onShow() {
    console.log('%c 小程序切到前台', 'color: yellow; font-size: 20px')
    // setTimeout(() => {
    //   a++
    // }, 2000)
  },
  onHide() {
    console.log('%c 小程序切到后台', 'color: green; font-size: 20px')
  },
  onError(err) {
    console.log('%c 捕获错误信息', 'color: green; font-size: 20px', err)
  },
  // 存全局数据
  globalData: {
    rootTitle: '默认标题'
  }
})
