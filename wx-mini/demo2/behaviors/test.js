export default Behavior({

  data: {
    title: '我是混入的数据'
  },

  methods: {
    changeTitle(e) {
      console.log('我是混入的函数', e.target.dataset)
      this.setData({
        title: e.target.dataset.title
      })
    }
  }
})