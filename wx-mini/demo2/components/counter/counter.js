// components/counter/counter.js
import test from '../../behaviors/test'

Component({
  // 注册混入
  behaviors: [test],

  /**
   * 组件的属性列表
   */
  properties: {

  },

  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },

  /**
   * 组件的初始数据
   */
  data: {
    num: 0
  },
  // 监听变量改变
  observers: {
    num() {
      console.log('num改变了', this.data.num)
    }
  },
  // 生命周期
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      console.log('组件加载成功')
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
      console.log('组件销毁')
    },
  },
  /**
   * 组件的方法列表
   */
  methods: {
    changeNum(e) {
      this.setData({
        num: this.data.num + e.target.dataset.n
      })
    }
  }
})
