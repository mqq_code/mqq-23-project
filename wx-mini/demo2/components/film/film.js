// components/film/film.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    poster: String,
    name: String,
    synopsis: String,
    filmId: Number
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    clickFn() {
      console.log('点击电影', this.data.filmId)
      // 调用父组件传的自定义事件, 并且给父组件传参数
      this.triggerEvent('handleClick', {
        filmId: this.data.filmId,
        aa: 'AAAAA'
      })
    }
  }
})
