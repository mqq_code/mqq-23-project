// components/tab/tab.js
Component({
  relations: {
    '../tabpane/tabpane': {
      type: 'child', // 关联的目标节点应为子节点
      linked: function(target) {
        // 每次有custom-li被插入时执行，target是该节点实例对象，触发在该节点attached生命周期之后
      },
      linkChanged: function(target) {
        // 每次有custom-li被移动后执行，target是该节点实例对象，触发在该节点moved生命周期之后
      },
      unlinked: function(target) {
        // 每次有custom-li被移除时执行，target是该节点实例对象，触发在该节点detached生命周期之后
      }
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    tablist: [],
    curIndex: 0
  },
  nodes: [],
  /**
   * 组件的方法列表
   */
  methods: {
    getAllTabpane: function(){
      // 使用getRelationNodes可以获得nodes数组，包含所有已关联的custom-li，且是有序的
      this.nodes = this.getRelationNodes('../tabpane/tabpane')
      this.nodes[this.data.curIndex].setData({
        show: true
      })
      this.setData({
        list: this.nodes.map(item => item.data.title)
      })
    },
    changeTab (e) {
      const { index } = e.target.dataset
      this.nodes[this.data.curIndex].setData({
        show: false
      })
      this.nodes[index].setData({
        show: true
      })
      this.setData({
        curIndex: index
      })
    }
  },
  ready() {
    this.getAllTabpane()
  }
})
