// index.js
const app = getApp() // 获取全局实例对象

// console.log(app.globalData) // 获取全局数据

Page({
  data: {
    title: app.globalData.rootTitle, // 获取全局数据
    list: [], // 电影列表
    pageNum: 1, // 当前第几页
    total: null // 总数
  },

  goDetail(e) {
    console.log('跳转详情页面', e.detail) // 接收子组件传过来的数据
    wx.navigateTo({
      url: `/pages/detail/detail?filmId=${e.detail.filmId}`,
    })
  },

  getlist (isRefresh) {
    wx.showLoading({
      title: '加载中',
      mask: true
    })
    wx.request({
      url: 'https://m.maizuo.com/gateway',
      method: 'GET',
      data: {
        cityId: 440300,
        pageNum: this.data.pageNum,
        pageSize: 10,
        type:1,
        k: 46382
      },
      header: {
        'X-Client-Info': '{"a":"3000","ch":"1002","v":"5.2.1","e":"16659764383978050249162753"}',
        'X-Host': 'mall.film-ticket.film.list'
      },
      success: res => {
        this.setData({
          list: this.data.list.concat(res.data.data.films),
          total: res.data.data.total
        })
        wx.hideLoading()
        console.log(this.data.list)
        if (isRefresh) {
          // 关闭下拉刷新
          wx.stopPullDownRefresh()
        }
      }
    })
  },
  onLoad() {
    this.getlist()
  },
  onShow() {
    console.log('index.js onShow', app.globalData)
    // 页面展示更新全局数据
    this.setData({
      title: app.globalData.rootTitle
    })
  },
  // 上拉加载
  onReachBottom () {
    console.log('页面到最底部了')
    if (this.data.list.length < this.data.total) {
      this.setData({
        pageNum: this.data.pageNum + 1
      })
      this.getlist()
    }
  },
  // 下拉刷新
  onPullDownRefresh() {
    this.setData({
      pageNum: 1,
      list: []
    })
    this.getlist(true)
  },
  onShareAppMessage({ from }) {
    return {
      title: '测试一下小程序',
      path: '/pages/list/list',
      imageUrl: 'https://pic.maizuo.com/usr/movie/825cd3106925b127e6027b300fb01026.jpg'
    }
  }
})
