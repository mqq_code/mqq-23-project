// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: []
  },

  golist() {

    wx.navigateTo({
      url: '/pages/list/list',
    })
  },

  goDetail(e) {
    console.log(e.currentTarget.dataset)
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/detail/detail?id=${id}`,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
  
    wx.request({
      url: 'https://zyxcl.xyz/toplist', //仅为示例，并非真实的接口地址
      method: 'post',
      success: res => {
        this.setData({
          list: res.data.list
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})