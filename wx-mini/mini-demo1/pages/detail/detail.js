// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    playList: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // options: 当前页面参数
    console.log('详情页面加载成功', options.id)
    wx.request({
      url: 'https://zyxcl.xyz/playlist/detail',
      method: 'GET',
      data: {
        id: options.id
      },
      success: res => {
        console.log(res.data)
        this.setData({
          playList: res.data.playlist
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})