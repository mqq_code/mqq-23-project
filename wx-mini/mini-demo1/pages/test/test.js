// pages/test/test.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '标题',
    num: 0,
    arr: ['a', 'b', 'c', 'd'],
    list: [
      {
        title: '王小明',
        hobby: ['吃饭', '喝可乐', '烫头']
      },
      {
        title: '李四',
        hobby: ['吃烧烤', '吃火锅', '吃牛排']
      }
    ],
    obj: {
      user: '王大名',
      age: 22
    }
  },

  changeTitle(e) {
    //  获取input的value
    console.log('修改input', e.detail.value)
    this.setData({
      title: e.detail.value
    })
  },

  add (e) {
    // 获取标签上的参数 e.target.dataset
    console.log('add', e.target.dataset)
    const { num } =  e.target.dataset
    // 修改数据，更新页面
    this.setData({
      num: this.data.num + num
    })
  },

  tapBox(e) {
    // console.log(e.currentTarget.dataset) // 绑定事件的元素
    // console.log(e.target) // 触发事件的元素
    console.log(e)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})