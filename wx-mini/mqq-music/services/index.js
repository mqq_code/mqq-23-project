
// 获取轮播图数据
export const getBanner = () => {
  return uni.request({
    url: 'https://zyxcl.xyz/banner'
  })
}

// 推荐歌单
export const getPersonalized = () => {
  return uni.request({
    url: 'https://zyxcl.xyz/personalized'
  })
}


// 所有榜单
export const getToplist = () => {
  return uni.request({
    url: 'https://zyxcl.xyz/toplist/detail'
  })
}

// 歌单详情
export const getPlaylistDetail = (id) => {
  return uni.request({
    url: 'https://zyxcl.xyz/playlist/detail',
    data: {
      id
    }
  })
}
// 歌曲详情
export const getSongDetail = (ids) => {
  return uni.request({
    url: 'https://zyxcl.xyz/song/detail',
    data: {
      ids
    }
  })
}

// 音乐url
export const getSongUrl = (id) => {
  return uni.request({
    url: 'https://zyxcl.xyz/song/url/v1',
    data: {
      id,
      level: 'standard'
    }
  })
}

// 获取歌词
export const getLyric = (id) => {
  return uni.request({
    url: 'https://zyxcl.xyz/lyric',
    data: {
      id
    }
  })
}

// 获取歌曲评论
export const getCommentMusic = (id) => {
  return uni.request({
    url: 'https://zyxcl.xyz/comment/music',
    data: {
      id
    }
  })
}
// 获取歌单评论
export const getCommentPlaylist = (id) => {
  return uni.request({
    url: 'https://zyxcl.xyz/comment/playlist',
    data: {
      id
    }
  })
}

// 推荐mv
export const getPersonalizedMv = () => {
  return uni.request({
    url: 'https://zyxcl.xyz/personalized/mv'
  })
}
// mv 详情
export const getMvDetail = mvid => {
  return uni.request({
    url: 'https://zyxcl.xyz/mv/detail',
    data: {
      mvid
    }
  })
}
// mv 地址
export const getMvUrl = id => {
  return uni.request({
    url: 'https://zyxcl.xyz/mv/url',
    data: {
      id
    }
  })
}
// mv 评论
export const getCommentMv = id => {
  return uni.request({
    url: 'https://zyxcl.xyz/comment/mv',
    data: {
      id
    }
  })
}




