import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { getSongUrl, getSongDetail } from '@/services/index.js'

const app = getApp()

const usePlaylistStore = defineStore('playlist', () => {
  const playlist = ref([]) // 播放列表
  const curSongIndex = ref(0) // 当前播放歌曲的下标
  const curSongDuration = ref(0) // 当前歌曲总时长
  const randomPlay = ref(false) // 是否随机播放
  // 当前播放歌曲的详情
  const songInfo = computed(() => {
    return playlist.value[curSongIndex.value]
  })
  
   // 获取当前歌曲的 url
  const getSongUrlAction = async (id) => {
    const res = await getSongUrl(id)
    app.globalData.innerAudioContext.src = res.data.data[0].url
    app.globalData.innerAudioContext.autoplay = true
  }
  
  // 播发列表添加歌曲
  const addSong = async id => {
    // 要添加的歌曲和当前正在播放的歌曲不一致，重新获取音乐url
    if (songInfo.value?.id !== id) {
      getSongUrlAction(id)
    }
    
    const index = playlist.value.findIndex(v => v.id === id)
    if (index > -1) {
      curSongIndex.value = index
    } else {
      // 获取歌曲详情添加到播放列表
      const res = await getSongDetail(id)
      const songInfo = res.data.songs[0]
      playlist.value.unshift(songInfo) // 播放列表添加歌曲
      curSongIndex.value = 0
    }
  }
  
  // 播放歌单所有歌曲
  const setPlaylist = list => {
    playlist.value = list
    curSongIndex.value = 0
    getSongUrlAction(list[0].id)
  }
  
  // 切换歌曲
  const changeSong = index => {
    if (randomPlay.value) {
      curSongIndex.value = Math.floor(Math.random() * playlist.value.length)
    } else {
      if (index < 0) {
        curSongIndex.value = playlist.value.length - 1
      } else if (index > playlist.value.length - 1) {
        curSongIndex.value = 0
      } else {
        curSongIndex.value = index
      }
    }
    getSongUrlAction(playlist.value[curSongIndex.value].id)
  }
  
  app.globalData.innerAudioContext.onCanplay(() => {
    console.log('总时长', app.globalData.innerAudioContext.duration)
    curSongDuration.value = app.globalData.innerAudioContext.duration
  })
  
  app.globalData.innerAudioContext.onEnded(() => {
    console.log('播放结束')
    changeSong(curSongIndex.value + 1)
  })
  
  return {
    curSongIndex,
    playlist,
    songInfo,
    addSong,
    setPlaylist,
    changeSong,
    curSongDuration,
    randomPlay
  }
});

export default usePlaylistStore