"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/mine/mine.js";
  "./pages/audior/audior.js";
  "./pages/search/search.js";
  "./pages/rank/rank.js";
  "./pages/listDetail/listDetail.js";
  "./pages/player/player.js";
  "./pages/mv/mv.js";
  "./pages/playMv/playMv.js";
}
const _sfc_main = {
  globalData: {
    innerAudioContext: common_vendor.index.createInnerAudioContext()
    // 音频对象
  },
  onLaunch: function() {
  },
  onShow: function() {
  },
  onHide: function() {
  }
};
const App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/App.vue"]]);
function createApp() {
  const app = common_vendor.createSSRApp(App);
  app.use(common_vendor.createPinia());
  return {
    app,
    Pinia: common_vendor.Pinia
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
