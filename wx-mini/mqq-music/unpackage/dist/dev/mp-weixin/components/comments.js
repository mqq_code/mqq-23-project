"use strict";
const common_vendor = require("../common/vendor.js");
if (!Array) {
  const _easycom_uni_section2 = common_vendor.resolveComponent("uni-section");
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  (_easycom_uni_section2 + _easycom_uni_popup2)();
}
const _easycom_uni_section = () => "../uni_modules/uni-section/components/uni-section/uni-section.js";
const _easycom_uni_popup = () => "../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  (_easycom_uni_section + _easycom_uni_popup)();
}
const _sfc_main = {
  __name: "comments",
  props: ["visible", "hotComments", "comments"],
  emits: ["update:visible"],
  setup(__props, { emit: emits }) {
    const props = __props;
    const popup = common_vendor.ref(null);
    const change = (e) => {
      emits("update:visible", e.show);
    };
    common_vendor.watch(() => props.visible, () => {
      if (props.visible) {
        popup.value.open();
      } else {
        popup.value.close();
      }
    });
    return (_ctx, _cache) => {
      return common_vendor.e({
        a: __props.hotComments && __props.hotComments.length > 0
      }, __props.hotComments && __props.hotComments.length > 0 ? {
        b: common_vendor.f(__props.hotComments, (item, k0, i0) => {
          return {
            a: item.user.avatarUrl,
            b: common_vendor.t(item.user.nickname),
            c: common_vendor.t(item.timeStr),
            d: common_vendor.t(item.likedCount),
            e: common_vendor.t(item.content),
            f: item.commentId
          };
        }),
        c: common_vendor.p({
          title: "热门评论",
          type: "line"
        })
      } : {}, {
        d: common_vendor.f(__props.comments, (item, k0, i0) => {
          return {
            a: item.user.avatarUrl,
            b: common_vendor.t(item.user.nickname),
            c: common_vendor.t(item.timeStr),
            d: common_vendor.t(item.likedCount),
            e: common_vendor.t(item.content),
            f: item.commentId
          };
        }),
        e: common_vendor.p({
          title: "最新评论",
          type: "line"
        }),
        f: common_vendor.sr(popup, "5e90f4b2-0", {
          "k": "popup"
        }),
        g: common_vendor.o(change),
        h: common_vendor.p({
          type: "bottom",
          ["background-color"]: "#fff"
        })
      });
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/components/comments.vue"]]);
wx.createComponent(Component);
