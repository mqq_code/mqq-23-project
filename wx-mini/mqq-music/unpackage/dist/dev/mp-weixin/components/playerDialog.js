"use strict";
const common_vendor = require("../common/vendor.js");
const stores_playlist = require("../stores/playlist.js");
require("../services/index.js");
if (!Array) {
  const _easycom_uni_card2 = common_vendor.resolveComponent("uni-card");
  _easycom_uni_card2();
}
const _easycom_uni_card = () => "../uni_modules/uni-card/components/uni-card/uni-card.js";
if (!Math) {
  _easycom_uni_card();
}
const _sfc_main = {
  __name: "playerDialog",
  props: ["isTabBar"],
  setup(__props) {
    const playlistStore = stores_playlist.usePlaylistStore();
    const ar = common_vendor.computed(() => {
      var _a;
      return ((_a = playlistStore.songInfo) == null ? void 0 : _a.ar.map((v) => v.name).join("/")) || "";
    });
    const goPlayer = () => {
      common_vendor.index.navigateTo({
        url: `/pages/player/player?id=${playlistStore.songInfo.id}`
      });
    };
    return (_ctx, _cache) => {
      var _a, _b;
      return common_vendor.e({
        a: common_vendor.unref(playlistStore).songInfo
      }, common_vendor.unref(playlistStore).songInfo ? {
        b: common_vendor.p({
          title: (_a = common_vendor.unref(playlistStore).songInfo) == null ? void 0 : _a.name,
          ["sub-title"]: common_vendor.unref(ar),
          thumbnail: (_b = common_vendor.unref(playlistStore).songInfo) == null ? void 0 : _b.al.picUrl,
          isFull: true,
          border: false
        }),
        c: common_vendor.n({
          tabBar: __props.isTabBar
        }),
        d: common_vendor.o(goPlayer)
      } : {});
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/components/playerDialog.vue"]]);
wx.createComponent(Component);
