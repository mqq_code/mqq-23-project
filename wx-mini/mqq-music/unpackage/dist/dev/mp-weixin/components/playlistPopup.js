"use strict";
const common_vendor = require("../common/vendor.js");
const stores_playlist = require("../stores/playlist.js");
require("../services/index.js");
if (!Array) {
  const _easycom_uni_popup2 = common_vendor.resolveComponent("uni-popup");
  _easycom_uni_popup2();
}
const _easycom_uni_popup = () => "../uni_modules/uni-popup/components/uni-popup/uni-popup.js";
if (!Math) {
  _easycom_uni_popup();
}
const _sfc_main = {
  __name: "playlistPopup",
  props: ["visible"],
  emits: ["update:visible"],
  setup(__props, { emit: emits }) {
    const props = __props;
    const playlistStore = stores_playlist.usePlaylistStore();
    const playlistPopup = common_vendor.ref(null);
    common_vendor.watch(() => props.visible, () => {
      if (props.visible) {
        playlistPopup.value.open();
      } else {
        playlistPopup.value.close();
      }
    });
    const change = (e) => {
      emits("update:visible", e.show);
    };
    const ar = (list) => {
      return list.map((v) => v.name).join("/");
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(common_vendor.unref(playlistStore).playlist.length),
        b: common_vendor.f(common_vendor.unref(playlistStore).playlist, (item, index, i0) => {
          return {
            a: common_vendor.t(index + 1),
            b: common_vendor.t(item.name),
            c: common_vendor.t(ar(item.ar)),
            d: item.id,
            e: common_vendor.n({
              active: common_vendor.unref(playlistStore).curSongIndex === index
            }),
            f: common_vendor.o(($event) => common_vendor.unref(playlistStore).changeSong(index), item.id)
          };
        }),
        c: common_vendor.unref(playlistStore).curSongIndex * 40,
        d: common_vendor.sr(playlistPopup, "4f2ce12c-0", {
          "k": "playlistPopup"
        }),
        e: common_vendor.o(change),
        f: common_vendor.p({
          type: "bottom",
          ["background-color"]: "#fff"
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/components/playlistPopup.vue"]]);
wx.createComponent(Component);
