"use strict";
const common_vendor = require("../../common/vendor.js");
const services_index = require("../../services/index.js");
if (!Array) {
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_search_bar2 = common_vendor.resolveComponent("uni-search-bar");
  const _easycom_uni_section2 = common_vendor.resolveComponent("uni-section");
  const _easycom_uni_drawer2 = common_vendor.resolveComponent("uni-drawer");
  (_easycom_uni_icons2 + _easycom_uni_search_bar2 + _easycom_uni_section2 + _easycom_uni_drawer2)();
}
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_search_bar = () => "../../uni_modules/uni-search-bar/components/uni-search-bar/uni-search-bar.js";
const _easycom_uni_section = () => "../../uni_modules/uni-section/components/uni-section/uni-section.js";
const _easycom_uni_drawer = () => "../../uni_modules/uni-drawer/components/uni-drawer/uni-drawer.js";
if (!Math) {
  (_easycom_uni_icons + _easycom_uni_search_bar + _easycom_uni_section + _easycom_uni_drawer + PlayerDialog)();
}
const PlayerDialog = () => "../../components/playerDialog.js";
const _sfc_main = {
  __name: "index",
  setup(__props) {
    const banners = common_vendor.ref([]);
    const personalizedList = common_vendor.ref([]);
    const navlist = common_vendor.reactive([
      {
        "id": 1,
        "name": "每日推荐",
        "iconUrl": "http://p1.music.126.net/4DpSgAVpJny4Ewf-Xw_WQQ==/109951163986641971.jpg",
        "url": "/pages/mv/mv"
      },
      {
        "id": 2,
        "name": "私人FM",
        "iconUrl": "http://p1.music.126.net/Shi7cRT1bDhwpVDM7AOFXg==/109951165265330616.jpg",
        "url": ""
      },
      {
        "id": 3,
        "name": "歌单",
        "iconUrl": "http://p1.music.126.net/uG5p6CnwAHrLqOkaSeRlnA==/109951163986649164.jpg",
        "url": ""
      },
      {
        "id": 4,
        "name": "排行榜",
        "iconUrl": "http://p1.music.126.net/SDFC6A3X2wzUCavYyeGIOg==/109951163986649670.jpg",
        "url": "/pages/rank/rank"
      },
      {
        "id": 5,
        "name": "有声书",
        "iconUrl": "http://p1.music.126.net/Kb4oK0m_ocs3FR3lo-r9yg==/109951167319110429.jpg",
        "url": ""
      },
      {
        "id": 6,
        "name": "数字专辑",
        "iconUrl": "http://p1.music.126.net/nRWhsf3P7r7eqHz-v61VBg==/109951166989045593.jpg",
        "url": ""
      },
      {
        "id": 7,
        "name": "直播",
        "iconUrl": "http://p1.music.126.net/2JvVZZjcjyCE72fGMsC1hg==/109951166989043011.jpg",
        "url": ""
      },
      {
        "id": 8,
        "name": "关注新歌",
        "iconUrl": "http://p1.music.126.net/BAOWsqZmmxL8JIH-wejMmQ==/109951167294312390.jpg",
        "url": ""
      },
      {
        "id": 9,
        "name": "收藏家",
        "iconUrl": "http://p1.music.126.net/gk99UFRasebERf38t6A1kA==/109951168168319514.jpg",
        "url": ""
      },
      {
        "id": 10,
        "name": "游戏专区",
        "iconUrl": "http://p1.music.126.net/qzKViKyOodgMYIEpp5t1NQ==/109951166989041366.jpg",
        "url": ""
      }
    ]);
    services_index.getBanner().then((res) => {
      banners.value = res.data.banners;
    });
    services_index.getPersonalized().then((res) => {
      console.log(res.data.result);
      personalizedList.value = res.data.result;
    });
    const showLeft = common_vendor.ref(null);
    const showDrawer = () => {
      showLeft.value.open();
    };
    const hrefFn = (url) => {
      common_vendor.index.navigateTo({
        url
      });
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          color: "#5e6d82",
          type: "settings-filled",
          size: "30"
        }),
        b: common_vendor.o(showDrawer),
        c: common_vendor.p({
          placeholder: "搜索",
          bgColor: "#EEEEEE",
          readonly: true
        }),
        d: common_vendor.f(banners.value, (item, k0, i0) => {
          return {
            a: item.imageUrl,
            b: item.targetId
          };
        }),
        e: common_vendor.f(navlist, (item, k0, i0) => {
          return {
            a: item.iconUrl,
            b: common_vendor.t(item.name),
            c: item.id,
            d: common_vendor.o(($event) => hrefFn(item.url), item.id)
          };
        }),
        f: common_vendor.p({
          title: "推荐歌单",
          type: "line"
        }),
        g: common_vendor.f(personalizedList.value, (item, k0, i0) => {
          return {
            a: item.picUrl,
            b: common_vendor.t(item.name),
            c: item.id,
            d: common_vendor.o(($event) => hrefFn(`/pages/listDetail/listDetail?id=${item.id}`), item.id)
          };
        }),
        h: common_vendor.sr(showLeft, "0d4f802f-3", {
          "k": "showLeft"
        }),
        i: common_vendor.p({
          mode: "left",
          width: 320
        }),
        j: common_vendor.p({
          isTabBar: true
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
