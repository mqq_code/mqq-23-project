"use strict";
const common_vendor = require("../../common/vendor.js");
const services_index = require("../../services/index.js");
const stores_playlist = require("../../stores/playlist.js");
if (!Math) {
  (Comments + PlayerDialog)();
}
const Comments = () => "../../components/comments.js";
const PlayerDialog = () => "../../components/playerDialog.js";
const _sfc_main = {
  __name: "listDetail",
  setup(__props) {
    const playlistStore = stores_playlist.usePlaylistStore();
    const playlist = common_vendor.ref({});
    const visible = common_vendor.ref(false);
    const hotComments = common_vendor.ref([]);
    const comments = common_vendor.ref([]);
    const commentsTotal = common_vendor.ref(0);
    const getPlaylistDetailApi = async (id) => {
      const res = await services_index.getPlaylistDetail(id);
      if (res.data.code === 200) {
        console.log("歌单详情", res.data);
        playlist.value = res.data.playlist;
        common_vendor.index.setNavigationBarTitle({
          title: res.data.playlist.name
        });
      } else {
        common_vendor.index.showToast({
          title: res.data.message,
          icon: "error"
        });
      }
    };
    const getCommentPlaylistApi = async (id) => {
      const res = await services_index.getCommentPlaylist(id);
      hotComments.value = res.data.hotComments;
      comments.value = res.data.comments;
      commentsTotal.value = res.data.total;
    };
    common_vendor.onLoad((options) => {
      getPlaylistDetailApi(options.id);
      getCommentPlaylistApi(options.id);
    });
    const ar = (list) => {
      return list.map((v) => v.name).join("/");
    };
    const format = (time) => {
      return common_vendor.hooks(time).format("YYYY-MM-DD");
    };
    const goPlayer = (id) => {
      playlistStore.addSong(id);
      common_vendor.index.navigateTo({
        url: `/pages/player/player?id=${id}`
      });
    };
    const playAll = () => {
      console.log(playlist.value.tracks);
      playlistStore.setPlaylist(playlist.value.tracks);
    };
    return (_ctx, _cache) => {
      var _a, _b;
      return {
        a: `url(${playlist.value.coverImgUrl})`,
        b: playlist.value.coverImgUrl,
        c: common_vendor.t(playlist.value.name),
        d: (_a = playlist.value.creator) == null ? void 0 : _a.avatarUrl,
        e: common_vendor.t((_b = playlist.value.creator) == null ? void 0 : _b.nickname),
        f: common_vendor.t(format(playlist.value.updateTime)),
        g: common_vendor.t(commentsTotal.value),
        h: common_vendor.o(($event) => visible.value = true),
        i: common_vendor.o(playAll),
        j: common_vendor.f(playlist.value.tracks, (item, index, i0) => {
          return {
            a: common_vendor.t(index + 1),
            b: common_vendor.t(item.name),
            c: common_vendor.t(ar(item.ar)),
            d: item.id,
            e: common_vendor.o(($event) => goPlayer(item.id), item.id)
          };
        }),
        k: common_vendor.o(($event) => visible.value = $event),
        l: common_vendor.p({
          hotComments: hotComments.value,
          comments: comments.value,
          visible: visible.value
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/pages/listDetail/listDetail.vue"]]);
wx.createPage(MiniProgramPage);
