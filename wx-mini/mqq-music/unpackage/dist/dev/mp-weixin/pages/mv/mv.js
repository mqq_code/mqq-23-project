"use strict";
const common_vendor = require("../../common/vendor.js");
const services_index = require("../../services/index.js");
const _sfc_main = {
  __name: "mv",
  setup(__props) {
    const list = common_vendor.ref([]);
    services_index.getPersonalizedMv().then((res) => {
      list.value = res.data.result;
    });
    const goPlayMv = (id) => {
      common_vendor.index.navigateTo({
        url: `/pages/playMv/playMv?id=${id}`
      });
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(list.value, (item, k0, i0) => {
          return {
            a: item.picUrl,
            b: common_vendor.t(item.copywriter),
            c: common_vendor.t(item.artistName),
            d: item.id,
            e: common_vendor.o(($event) => goPlayMv(item.id), item.id)
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/pages/mv/mv.vue"]]);
wx.createPage(MiniProgramPage);
