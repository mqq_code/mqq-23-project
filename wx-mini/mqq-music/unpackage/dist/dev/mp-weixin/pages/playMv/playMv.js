"use strict";
const common_vendor = require("../../common/vendor.js");
const services_index = require("../../services/index.js");
if (!Math) {
  Comments();
}
const Comments = () => "../../components/comments.js";
const _sfc_main = {
  __name: "playMv",
  setup(__props) {
    const url = common_vendor.ref("");
    const info = common_vendor.ref({});
    const visible = common_vendor.ref(false);
    const commentsTotal = common_vendor.ref(0);
    const hotComments = common_vendor.ref([]);
    const comments = common_vendor.ref([]);
    const getMvDetailApi = async (id) => {
      const res = await services_index.getMvDetail(id);
      console.log("详情", res.data.data);
      info.value = res.data.data;
    };
    const getMvUrlApi = async (id) => {
      const res = await services_index.getMvUrl(id);
      console.log("播放地址", res.data.data.url);
      url.value = res.data.data.url;
    };
    const getCommentMvApi = async (id) => {
      const res = await services_index.getCommentMv(id);
      console.log("评论", res.data);
      hotComments.value = res.data.hotComments;
      comments.value = res.data.comments;
      commentsTotal.value = res.data.total;
    };
    common_vendor.onLoad((options) => {
      getMvDetailApi(options.id);
      getMvUrlApi(options.id);
      getCommentMvApi(options.id);
    });
    return (_ctx, _cache) => {
      return {
        a: info.value.cover,
        b: url.value,
        c: common_vendor.t(info.value.name),
        d: common_vendor.t(info.value.artistName),
        e: common_vendor.t(info.value.desc),
        f: common_vendor.t(commentsTotal.value),
        g: common_vendor.o(($event) => visible.value = true),
        h: common_vendor.o(($event) => visible.value = $event),
        i: common_vendor.p({
          hotComments: hotComments.value,
          comments: comments.value,
          visible: visible.value
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-34c97f89"], ["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/pages/playMv/playMv.vue"]]);
wx.createPage(MiniProgramPage);
