"use strict";
const common_vendor = require("../../common/vendor.js");
const services_index = require("../../services/index.js");
const stores_playlist = require("../../stores/playlist.js");
if (!Array) {
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  _easycom_uni_icons2();
}
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
if (!Math) {
  (_easycom_uni_icons + Comments + PlaylistPopup)();
}
const Comments = () => "../../components/comments.js";
const PlaylistPopup = () => "../../components/playlistPopup.js";
const _sfc_main = {
  __name: "player",
  setup(__props) {
    const playlistStore = stores_playlist.usePlaylistStore();
    const app = getApp();
    const lyric = common_vendor.ref([]);
    const isPaused = common_vendor.ref(app.globalData.innerAudioContext.paused);
    const curRowIndex = common_vendor.ref(0);
    const curScrollTop = common_vendor.ref(0);
    const hotComments = common_vendor.ref([]);
    const comments = common_vendor.ref([]);
    const visible = common_vendor.ref(false);
    const commentsTotal = common_vendor.ref(0);
    const playlistPopupVisible = common_vendor.ref(false);
    const currentTime = common_vendor.ref(0);
    app.globalData.innerAudioContext.onTimeUpdate(() => {
      currentTime.value = app.globalData.innerAudioContext.currentTime;
      lyric.value.forEach((item, index) => {
        if (app.globalData.innerAudioContext.currentTime >= item[0]) {
          curRowIndex.value = index;
        }
      });
    });
    common_vendor.watch(curRowIndex, () => {
      curScrollTop.value = curRowIndex.value * 20 - 20;
    });
    const getLyricApi = async (id) => {
      const res = await services_index.getLyric(id);
      lyric.value = res.data.lrc.lyric.split("\n").filter(Boolean).map((item) => {
        const [first, second] = item.split("]");
        const [m, s] = first.slice(1).split(":");
        return [m * 60 + s * 1, second];
      });
    };
    const getCommentMusicApi = async (id) => {
      const res = await services_index.getCommentMusic(id);
      hotComments.value = res.data.hotComments;
      comments.value = res.data.comments;
      commentsTotal.value = res.data.total;
    };
    common_vendor.onLoad((options) => {
      const { id } = options;
      getLyricApi(id);
      getCommentMusicApi(id);
      if (!playlistStore.songInfo) {
        playlistStore.addSong(id);
      }
    });
    common_vendor.watch(() => playlistStore.songInfo, () => {
      var _a, _b;
      common_vendor.index.setNavigationBarTitle({
        title: (_a = playlistStore.songInfo) == null ? void 0 : _a.name
      });
      if ((_b = playlistStore.songInfo) == null ? void 0 : _b.id) {
        getLyricApi(playlistStore.songInfo.id);
        getCommentMusicApi(playlistStore.songInfo.id);
      }
    }, {
      immediate: true
    });
    const addZero = (n) => {
      return n < 10 ? "0" + n : n;
    };
    const formatTime = (t) => {
      const m = addZero(Math.floor(t / 60));
      const s = addZero(Math.floor(t % 60));
      return `${m}:${s}`;
    };
    const changeStatus = () => {
      if (app.globalData.innerAudioContext.paused) {
        app.globalData.innerAudioContext.play();
        isPaused.value = false;
      } else {
        app.globalData.innerAudioContext.pause();
        isPaused.value = true;
      }
    };
    const curProggress = common_vendor.computed(() => {
      return Math.floor(currentTime.value / playlistStore.curSongDuration * 100);
    });
    const changeProgress = (e) => {
      console.log(playlistStore.curSongDuration * (e.detail.value / 100));
      app.globalData.innerAudioContext.currentTime = playlistStore.curSongDuration * (e.detail.value / 100);
    };
    return (_ctx, _cache) => {
      var _a;
      return {
        a: (_a = common_vendor.unref(playlistStore).songInfo) == null ? void 0 : _a.al.picUrl,
        b: common_vendor.n({
          stop: isPaused.value
        }),
        c: isPaused.value,
        d: common_vendor.o(changeStatus),
        e: common_vendor.f(lyric.value, (item, index, i0) => {
          return {
            a: common_vendor.t(item[1]),
            b: index,
            c: common_vendor.n({
              active: curRowIndex.value === index
            })
          };
        }),
        f: curScrollTop.value,
        g: common_vendor.p({
          type: "heart",
          color: "#ffffff",
          size: "30"
        }),
        h: common_vendor.p({
          type: "chatbubble",
          color: "#ffffff",
          size: "30"
        }),
        i: common_vendor.t(commentsTotal.value > 999 ? "999+" : commentsTotal.value),
        j: common_vendor.o(($event) => visible.value = true),
        k: common_vendor.t(formatTime(currentTime.value)),
        l: common_vendor.unref(curProggress),
        m: common_vendor.o(changeProgress),
        n: common_vendor.t(formatTime(common_vendor.unref(playlistStore).curSongDuration)),
        o: common_vendor.o(($event) => common_vendor.unref(playlistStore).randomPlay = !common_vendor.unref(playlistStore).randomPlay),
        p: common_vendor.p({
          type: common_vendor.unref(playlistStore).randomPlay ? "paperclip" : "loop",
          color: "#ffffff",
          size: "30"
        }),
        q: common_vendor.o(($event) => common_vendor.unref(playlistStore).changeSong(common_vendor.unref(playlistStore).curSongIndex - 1)),
        r: common_vendor.p({
          type: "left",
          color: "#ffffff",
          size: "30"
        }),
        s: common_vendor.t(isPaused.value ? "播放" : "暂停"),
        t: common_vendor.o(changeStatus),
        v: common_vendor.o(($event) => common_vendor.unref(playlistStore).changeSong(common_vendor.unref(playlistStore).curSongIndex + 1)),
        w: common_vendor.p({
          type: "right",
          color: "#ffffff",
          size: "30"
        }),
        x: common_vendor.p({
          type: "list",
          color: "#ffffff",
          size: "30"
        }),
        y: common_vendor.o(($event) => playlistPopupVisible.value = true),
        z: common_vendor.o(($event) => visible.value = $event),
        A: common_vendor.p({
          hotComments: hotComments.value,
          comments: comments.value,
          visible: visible.value
        }),
        B: common_vendor.o(($event) => playlistPopupVisible.value = $event),
        C: common_vendor.p({
          visible: playlistPopupVisible.value
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/pages/player/player.vue"]]);
wx.createPage(MiniProgramPage);
