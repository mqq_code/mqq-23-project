"use strict";
const common_vendor = require("../../common/vendor.js");
const services_index = require("../../services/index.js");
const _sfc_main = {
  __name: "rank",
  setup(__props) {
    const toplist = common_vendor.ref([]);
    const otherlist = common_vendor.ref([]);
    services_index.getToplist().then((res) => {
      toplist.value = res.data.list.slice(0, 4);
      otherlist.value = res.data.list.slice(4);
    });
    const goDetail = (id) => {
      console.log(id);
      common_vendor.index.navigateTo({
        url: `/pages/listDetail/listDetail?id=${id}`
      });
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(toplist.value, (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.t(item.updateFrequency),
            c: item.coverImgUrl,
            d: common_vendor.f(item.tracks, (val, index, i1) => {
              return {
                a: common_vendor.t(index + 1),
                b: common_vendor.t(val.first),
                c: common_vendor.t(val.second),
                d: index
              };
            }),
            e: item.id,
            f: common_vendor.o(($event) => goDetail(item.id), item.id)
          };
        }),
        b: common_vendor.f(otherlist.value, (item, k0, i0) => {
          return {
            a: item.id,
            b: item.coverImgUrl,
            c: common_vendor.o(($event) => goDetail(item.id), item.id)
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/23-project/mqq-23-project/wx-mini/mqq-music/pages/rank/rank.vue"]]);
wx.createPage(MiniProgramPage);
