"use strict";
const common_vendor = require("../common/vendor.js");
const getBanner = () => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/banner"
  });
};
const getPersonalized = () => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/personalized"
  });
};
const getToplist = () => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/toplist/detail"
  });
};
const getPlaylistDetail = (id) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/playlist/detail",
    data: {
      id
    }
  });
};
const getSongDetail = (ids) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/song/detail",
    data: {
      ids
    }
  });
};
const getSongUrl = (id) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/song/url/v1",
    data: {
      id,
      level: "standard"
    }
  });
};
const getLyric = (id) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/lyric",
    data: {
      id
    }
  });
};
const getCommentMusic = (id) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/comment/music",
    data: {
      id
    }
  });
};
const getCommentPlaylist = (id) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/comment/playlist",
    data: {
      id
    }
  });
};
const getPersonalizedMv = () => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/personalized/mv"
  });
};
const getMvDetail = (mvid) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/mv/detail",
    data: {
      mvid
    }
  });
};
const getMvUrl = (id) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/mv/url",
    data: {
      id
    }
  });
};
const getCommentMv = (id) => {
  return common_vendor.index.request({
    url: "https://zyxcl.xyz/comment/mv",
    data: {
      id
    }
  });
};
exports.getBanner = getBanner;
exports.getCommentMusic = getCommentMusic;
exports.getCommentMv = getCommentMv;
exports.getCommentPlaylist = getCommentPlaylist;
exports.getLyric = getLyric;
exports.getMvDetail = getMvDetail;
exports.getMvUrl = getMvUrl;
exports.getPersonalized = getPersonalized;
exports.getPersonalizedMv = getPersonalizedMv;
exports.getPlaylistDetail = getPlaylistDetail;
exports.getSongDetail = getSongDetail;
exports.getSongUrl = getSongUrl;
exports.getToplist = getToplist;
