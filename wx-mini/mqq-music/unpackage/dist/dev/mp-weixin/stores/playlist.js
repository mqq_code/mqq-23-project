"use strict";
const common_vendor = require("../common/vendor.js");
const services_index = require("../services/index.js");
const app = getApp();
const usePlaylistStore = common_vendor.defineStore("playlist", () => {
  const playlist = common_vendor.ref([]);
  const curSongIndex = common_vendor.ref(0);
  const curSongDuration = common_vendor.ref(0);
  const randomPlay = common_vendor.ref(false);
  const songInfo = common_vendor.computed(() => {
    return playlist.value[curSongIndex.value];
  });
  const getSongUrlAction = async (id) => {
    const res = await services_index.getSongUrl(id);
    app.globalData.innerAudioContext.src = res.data.data[0].url;
    app.globalData.innerAudioContext.autoplay = true;
  };
  const addSong = async (id) => {
    var _a;
    if (((_a = songInfo.value) == null ? void 0 : _a.id) !== id) {
      getSongUrlAction(id);
    }
    const index = playlist.value.findIndex((v) => v.id === id);
    if (index > -1) {
      curSongIndex.value = index;
    } else {
      const res = await services_index.getSongDetail(id);
      const songInfo2 = res.data.songs[0];
      playlist.value.unshift(songInfo2);
      curSongIndex.value = 0;
    }
  };
  const setPlaylist = (list) => {
    playlist.value = list;
    curSongIndex.value = 0;
    getSongUrlAction(list[0].id);
  };
  const changeSong = (index) => {
    if (randomPlay.value) {
      curSongIndex.value = Math.floor(Math.random() * playlist.value.length);
    } else {
      if (index < 0) {
        curSongIndex.value = playlist.value.length - 1;
      } else if (index > playlist.value.length - 1) {
        curSongIndex.value = 0;
      } else {
        curSongIndex.value = index;
      }
    }
    getSongUrlAction(playlist.value[curSongIndex.value].id);
  };
  app.globalData.innerAudioContext.onCanplay(() => {
    console.log("总时长", app.globalData.innerAudioContext.duration);
    curSongDuration.value = app.globalData.innerAudioContext.duration;
  });
  app.globalData.innerAudioContext.onEnded(() => {
    console.log("播放结束");
    changeSong(curSongIndex.value + 1);
  });
  return {
    curSongIndex,
    playlist,
    songInfo,
    addSong,
    setPlaylist,
    changeSong,
    curSongDuration,
    randomPlay
  };
});
exports.usePlaylistStore = usePlaylistStore;
